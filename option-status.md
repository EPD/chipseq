Summary
=======

Suggested changes (major ones; a few minor ones are in the detailed sections):

* let `-f` stand for "feature" everywhere (change `eland2sga.pl`); when `-f`
  means "forward" use `-F` (and `-R` for reverse, if needed); when it means
  "file" either pass the filename as positional argument, or on stdin; or use an
  option char derived from the file's format.
* keep `-c` for "cutoff", introduce `-C` for "centering"
* let `-d` stand for "debug" (change `chr_replace_sga.pl` and `partit2bed.pl`)
* keep `-e` for "end", use `-x` for "extended"
* keep `-n` for "number" and `-N` for "normalized"
* use `-o` for "oriented" and "-O" for "output format"
* use `-k` ("keep") instead of `-r` for "retain mode on" (`filter_counts`)
* use `-a` instead of `-s` for assembly

Besides, make sure the long forms are consistent throughout.

Details
=======

`-A`
----

Used to specify one of the features (feature A) when there are two. Consistent;
no change required IMHO (though some use `-a` instead of `-A`).


`-a`
---

Used for assembly (`sga2wigSmooth_FS.pl`) or feature (`eland2sga.pl`, but
**not** `gff2sga.pl` -- it appears in a comment but not in the code); also min
BED score (`bed2bed_display`).

Feature is usually specified with `-f` (or `-A` and `-B` when there are two), so
we might want to change this. The thing is, `-f` is used by `eland2sga.pl` to
specify the eland file. We might use `-e`, or use a positional parameter.
Minimum might be specified with `-m`.

`-B`
----

Used to specify one of the features (feature B) when there are two. Consistent;
no change required IMHO.

`-b`
----

Used to specify beginnigs, of the chromosome or the 5'-end extension.
Semantically consistent, so no change is required IMHO.

`-c`
----

Mostly for count cutoff, but also for centering (`bed2sga`, `gff2sga`) or as a
normalisation factor (`sga2bed`). Maybe try `-C` for centering and `-n/-N` for
normalisation. Also, long option not always present.

`--color`
---------

Used in only one program, to specify the colour of the annotation track. No
change required.

`-d`
----

Among the C programs (based on `grep`ping the C source (help section)), `-d`
always seems to cause some form of debug behaviour, though in some cases this
involves checking the syntax of the input SGA file (e.g. `chipcor), while in
others it doesn't -- even though it does input SGA (e.g. `chipcenter`).
(**NOTE** Again, this is based on the help, not on the program logic!)

Only four programs (namely, `bed2bed_display`, `bed2sga`, `sga2bed`, and
`sga2wig`) seem to accept a long form (`--debug`).

Among the Perl scripts, `-d` is used for specifying the database
(`chr_replace_sga.pl`) or to specify the description of the BED header
(`partit2bed.pl`). There is no hint of any debugging behaviour, or of a
`--debug` option.

`--db`
------

Used as a long form of `-i` by all C programs except `chipcenter` and
`chippeak`. Used as apparently the sole form of specifying the path to
`chro_idx.nstorage` by `gff2sga.pl`, `partit2bed.pl`, `partit2sga.pl`,
`sga2fps.pl`, `sga2gff.pl`, and `wigVS2sga.pl`.

`-e`
----

Used mostly to specify a chromosome or region end (see also `-b`), but also to
specify extented SGA format or to specify the semantics of SGA fields. Maybe
`-E` (or `-x`, as in `fps2sga.pl`) could stand for extended format. The SGA
"feature" field, now specified to `sga2gff` by `-e`, could be specified by `-f`
since this option is unused.

`-f`
----

Mostly used to specify the feature (when there is only one, otherwise it's `-A`.
and `-B`).  Many of the programs that use `-f` in this sense, but not all,
accept the long form `--feature`. Also frequently used to specify an input file.
Other uses are: length of the 3' (= fwd) extension (`sga2bed`), normalization
factor (`sga2wig`), and fragment size (`sga2wigSmooth_FS.pl`).

I would argue that we should keep `-f` for feature. Mandatory files can be
passed as a positional argument or on stdin; optional ones (or mandatory ones as
well, if we really want) can be passed by an option derived from the format, e.g.
`-G` for GFF, `-S` for SGA, `-E` for Eland, etc.

The normalization factor could be passed by `-n`, the fragment size by `-s`.

`--feature`
----------

See `-f`.

`--format`
----------

Only used by `sga2wig`, to specify format (short form is `-o`, not `-f`).

`-h`, `--help`
--------------

Used consistently to provide help, though not all programs accept the long form.
No other option used to show help.

We may want to ensure that the long form is accepted by all programs.

`-i`
----

Among the C programs, the following accept option `-i` to specify the path to
the database: `bed2sga, `chipcenter, `chippeak, `sga2bed, and `sga2wig. Of
these, `chipcenter` and `chippeak` do not admit the long form `--db`. The
argument placeholder is inconsistently `<path>` or `<dir>`.

Among the Perl scripts, only `eland2sga.pl` accepts `-i`, seemigly for the same
purpose (though the placeholder is `db-dir`).

`--ignore0flag`
---------------

Singleton (`fps2sga.pl`).

`-l`
----

Used to specify lengths (of reads or tags). Use is consistent, nothing to do
IMHO.

`-n`
----

Use is divided roughly equally between normalisation and chromosome number.
Since `-N` is not used, we might want to introduce it.

`-o`
----

Used to select an output format or to specify that a strand is oriented. We
could introduce `-O` for one and keep `-o` for the other.

`--oformat`
-----------

Long form of `-o` for `bed2bed_display` (note that `sga2wig` uses `--format`,
not `--oformat`: we may want to harmonize).

`-p`
----

Transition penalty, used only by `chippart`.

`-q`
----

Report feature B tag counts as `feature_name=<int>`, used only by `chipscore`.

`-r`
----

This is the most polysemic of all options, and no two programs use it with the
same meaning:

Program         Meaning of `-r`
--------------- --------------------------
`bed2sga`		    Generate a 2-line[+/-] SGA file representing BED regions
`chipcenter`		New feature name (for feature replacement)
`chippeak`		  Refine Peak Positions
`chipscore`		  Reverse extraction process
`filter_counts`	Retain Mode on
`sga2bed`		    BED format without annotation track header lines

I'd suggest `-R` for "reverse" (since I've suggested `-F` for "forward") - this
is for `chipscore`. We could use `-k` ("keep") for "retain mode"
(`filter_counts`). I see no connection between "r" and dropping headers in track
annotations, so why not `-H`? This would leave only two uses of `-r` (namely,
"region" (`bed2sga`) and "refine" (`chippeak`). That seems reasonable.

`-s`
----

Mostly assembly ("species"), but also shift, density (?), and span. We might be
able to use `-a` instead for assembly; but `-d` cannot be used for density as it
is used for "debug". Maybe `-p`, which looks like ρ, a frequent symbol for
density?.

`--span`
--------

Long form of `-s` in `sga2wig`, but not recognized by `sga2wigSmooth_FS.pl` (in
which `-s` has the same meaning).

`--species`
-----------

Long form of `-s`, but not recognized by all programs which take `-s` as
"species".

`-t`
---

For threshold or track name (2 each). I'm wondering if "threshold" is different
from "cutoff"; if not we might be able to reuse `-c`.

`-u`
----

Unoriented (referring to the generated SGA file). Consistent, no changes needed.

`-v`
----

Vicinity. Singleton, used by `chippeak`.

`-w`
----

Means "width" in `chipextract` and "window" in `chippeak`. The meanings are
similar and there is no clash with other options, so we needn't change anything
here, IMO.

`-x`
----

Means either "expand" SGA into multiple GFF or BED lines, or produce "extended"
GFF. The meanings are similar and do not conflict with others; I'd say keep as
is. Possibly change `-e` to `-x` when `-e` means "extend".

`-z`
----

Set strand to zero. Only used by `chipcenter`.
