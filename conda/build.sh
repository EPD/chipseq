#!/bin/sh
set -x -e

# programs installed
# bed2bed_display bed2sga chipcenter chipcor chipextract chippart chippeak chipscore compactsga countsga featreplace filter_counts sga2bed sga2wig
# check_bed.pl chr_replace_sga.pl eland2sga.pl fetch_sga.pl fps2sga.pl gff2sga.pl make_chro_idx.nstorage.pl partit2bed.pl partit2gff.pl partit2sga.pl rmsk2bed.pl rmsk2sga.pl sga2fps.pl sga2gff.pl sga2wigSmooth_FS.pl wigVS2sga.pl


sed -i "s@^PREFIX .*@PREFIX = $PREFIX@" src/Makefile Makefile
make clean
make
make test
make install

