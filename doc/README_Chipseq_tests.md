---
title: ChipSeq Test System
author: Thomas Junier
date: \today
classoption: onecolumn
toc: true
autoSectionLabels: true
---

For the Impatient
=================

```bash
$ cd ./src/chipseq/src/tests
$ ./test_main.sh -h
$ ./test_main.sh
```

Aims of this Document
=====================

The present document explains how the `ChipSeq` test system works, how to use
it, and how to add new tests.

Overview
========

The following describes the default behaviour of the system.

The test system checks that programs of the `ChipSeq` suite work as expected by
calling them on fixed inputs with fixed parameters, so that the expected output
is known, and then comparing their _actual_ output with the expected output. A
particular program is usually tested several times, each time with a different
set of inputs and/or parameters.

Each test consists of a snippet of shell code (referred to as _test command_
below) that calls one or more programs of the suite.  Each test also has a
unique _name_ that is used to refer to it in the test report. The test names and
associated commands are stored in a simple TSV file, which makes it trivial to
add test cases to the set.  In order to run a test, the system creates a Bash
_test case script_ that contains (and calls) the test command, and then runs it.
The script's actual outputs are then compared to the test's expected outputs: if
they are identical, the test is a _success_; otherwise it is a _failure_ (if the
test cannot even run properly, it is an _error_) - see @sec:test-results. In
case of success, the test case script and program output are removed, otherwise
they are kept to facilitate troubleshooting. The test script may run additional
commands to set up and tear down the environment (such as linking to data files
the script expects to find in its working directory, and removing them
afterwards).

The system can launch test commands in five^[Or perhaps only four -- Singularity
and Apptainer are almost identical (they differ mostly by the name of the binary
that runs the container).]  different _environments_: native, Docker, Apptainer,
Singularity and Conda. The particulars of launching the test commands in a given
environment are handled in the test case script.

Installation
============

The test system is included in the ChipSeq distribution.^[The actual Git
repository still has to be determined, but it is now available from
`https://gitlab.sib.swiss/tjunier/epd.git`.]

Running Tests {#sec:interface}
=============

Default 
-------

`cd` to `./src/chipseq/src/tests` and do

```bash
$ ./test_main.sh
```

This will run all tests^[Except the meta tests, see @sec:meta-tests.] in the
_native_ environment (that is, no container or Conda environment). It will
produce an ouput like the following:

```
Running test_bed2bed_display_STDIN...   ok
Running test_bed2bed_display_ARG1...    ok
Running test_bed2sga_STDIN...           ok
.
.
.
Running test_countsga_STDIN...          ok
Running test_sga2bed_ARG1...            ok
Running test_sga2bed_STDIN...           ok

All 23 tests OK!
```

**NOTE**: to avoid accidentally interfering with troubleshooting, `test_main.sh`
will **refuse to run** if `./out` is not empty. This can be overridden by
passing it option `-f` ("force", see @sec:opt-f); another possibility is
to clean up leftover files manually or by passing option `-c` ("cleanup", see
{@sec:opt-c}).

Test Results {#sec:test-results}
-------

### Errors {#sec:errors}

If the test command _cannot be executed_, the system reports an _error_. The
test name is followed by the word `error`, possibly along with an error message
from the shell (typically indicating that a file wasn't found or that
permissions were insufficient), like this:

```
Running meta_noexp... error .exp file (./exp/meta_noexp.exp) not found [...]
```

In this case, It is _unknown_ whether the `ChipSeq`-suite
program being tested works correctly or not: given the right inputs, or with the
right permissions, it might work perfectly well; it was just not possible to
verify that. 

To help the developer find out what went wrong, the test case script is retained
in case of error (but see option `-s` in @sec:opt-s).  The script is in the same
directory as `./test_main.sh`; its name has the structure
`<test-name>-<environment>.sh`, in the above example `meta_noexp-native.sh`.
This script can be run independently from `test_main.sh`, its contents can be
easily checked for errors (e.g. in file names), etc. Likewise, any output (even
empty) is retained in the `./out` subdirectory (but see option `-o`, @sec:opt-o).

### Failures

If the test command can be executed, but the actual output _does not match_ the
expected output, the system reports a _failure_: the test name is followed by
the word `failure`, like this:

```bash
Running meta_fail...  failure
```

In that case, the program being tested does not produce the expected output.
This may mean that it does not work as expected, but it can also mean that it
was _called_ improperly (the test command is wrong) or that the expected input
file is wrong.

As in the case of errors, output files and test case scripts are retained in
case of failure to help with troubleshooting, and the main script will refuse to
run if output files are still present, so as to avoid destroying potentially
useful debugging information (see @sec:errors and cross-references therein for
details, including how to override default behaviour).

### Successes

If the test command can be executed, and the actual output _matches_ the
expected output, the system reports a _success_: the test name is followed by
the word `ok`, like this:

```bash
Running test_chippart_STDIN_dash...     ok
```

In this case, there is reason to believe that the program being tested works as
expected - provided the test itself is correct, of course.^[We could of course
test the tests themselves, and we do provide tests that are meant to detect
failure to detect problems (see @sec:opt-M), but this has to stop somewhere lest
we end up in infinite recursion -- _Quis custodiet ipsos custodes?_]

Results Summary
---------------

After the reports for individual tests, `test_main.sh` prints out a summary. If
all tests succeeded, it states this along with the number of tests:

```bash
$ ./test_main.sh  

Running test_bed2bed_display_STDIN...   ok
.
.
.
Running test_sga2bed_STDIN...           ok

All 23 tests OK!
```

In case of error or failure, we are shown a breakdown by case:

```bash
# some problematic tests here
.
.
.

0 successes
1 failures
2 errors
```

Options
=======

The default behaviour described above can be altered by passing options to
`test_main.sh`. The posibilities are decribed below. Options exist in short and
long form, but **note** that they cannot be mixed, even switches in their short
form: `-ofv`, for example, will not work and should be written `-o -f -v`.

Obtaining Help
--------------

To obtain a help message, pass option `-h` (long version: `--help`). The program
will not run any tests and will exit successfully after printing out the help
text (any other options will be ignored).

Verbose Output
--------------

Additional information can be requested by passing option `-v` (long form:
`--verbose`). The additional information depends on the task:

* When running tests (the default task), the program prints the pattern used to
  select tests (see @sec:opt-p), the number of tests matching the pattern,
  as well as the environment; it also possibly prints some container-related
  information, for example:
  ```bash
  ./test_main.sh --verbose --environment docker
  Created docker container 0e5c9d338267668e77e435f4fdb6718061d90c8955d3183b668aad68e7f0159f
  Running tests matching 'test_*' (23 tests).
  Test environment: docker

  Running test_bed2bed_display_STDIN...   ok
  .
  .
  .

  All 23 tests OK!
  Stopping Docker container... done.
  Removing Docker container.
  ```
* When cleaning up (see @sec:opt-c), it will print out the names of possible
  case test scripts that it will remove if found.
  
Specifying Which tests to Run {#sec:opt-p}
-----------------------------

The program selects which tests to run by matching their names (as in the first
column of `test_cmds.tsv`) to a shell glob pattern. By default this pattern is
`test_*`, which matches all tests except the meta tests (see @sec:opt-M). This
pattern can be changed by passing another pattern as an argument to option `-p`
(long form: `--pattern`), e.g. to select all tests that pertain to `chipcor`
(note the quotes around the pattern):

```bash
$ ./test_main.sh -p 'test_chipcor*' 

Running test_chipcor_ARG1...       ok
Running test_chipcor_STDIN_dash... ok
Running test_chipcor_STDIN...      ok

All 3 tests OK!
```


This is useful when work has been done on a certain program and only tests of
that program need to be run. It is also possible to single out a specific test
by passing its name to `-p` (in this case the quotes are not needed):

```bash
 # Will only run test_chipcor_ARG1
$ ./test_main.sh -p test_chipcor_ARG1
```

Running only the Meta Tests {#sec:opt-M}
---------------------------

To run only the meta tests (see @sec:meta-tests), pass `-M` (long:
`--meta-tests`). This equivalent to passing `-p 'meta_*'` (see @sec:opt-p).

Choosing the Environment
------------------------

Tests can be run in any of five environments (_sensu lato_: this includes both
containers and Conda environments).  The environment is specified by the
argument to option `-e`(long form: `--environment`), as follows

Argument to `-e` Corresponding environment
---------------  -------------------------
`apptainer`      Apptainer (formerly Singularity)
`conda`          Conda (see requirements below)
`docker`         Docker
`native`         native (no container or environment)
`singularity`    Singularity (now Apptainer)


### Conditions

Some environment-specific conditions must be met for tests to run, as follows
(see below for details):

Environment  Conditions
-----------  ----------
Apptainer    `chipseq_latest.sif` must be in the test direcory. 
Conda        `chipseq` Conda environment must be actvated.
Docker       The latest `sibswiss/chipseq` must have been pulled. 
native       none
Singularity  `chipseq_latest.sif` must be in the test direcory. 

#### Apptainer and Singularity

Apptainer and Singularity simply re-use the Docker container, which is stored at
the top of the test directory as `chipseq_latest.sif` (that is, in Singularity
format). 

This file can be pulled by doing

```bash
$ apptainer pull docker://sibswiss/chipseq:latest
 # or
$ singularity pull docker://sibswiss/chipseq:latest
```

The same file is used by the two environments. If the file is not found,
`test_main.sh` will emit a warning and abort. It will not, however, pull the
file on its own: the decision to pull a new container rests with the user.

Note that Apptainer and Singularity may emit warnings that have nothing to do
with the test status but can obscure the results. These can be suppressed by
passing `-W` (or `--no-warnings`). This passes option `-s` to
`apptainer`/`singularity`, which requests the suppression of said warnings. This
option is deliberately _not_ passed by default, in case a more serious warning
is emitted - one that should not be ignored.

#### Conda

To run the tests in the Conda environment, do the following:

##### Install the `chipseq` Conda environment

This need be done only once.

```bash
$ conda create --name chipseq
$ conda activate chipseq
$ conda install -c sibswiss -c bioconda chipseq
```

At this point you have installed the `chipseq` binaries, typically in
`~/.conda/envs/chipseq/bin`. Now you need the test data and scripts, which are
in the Gitlab repository `gitlab.sib.swiss/EPD/chipseq.git`

```bash
(chipseq) git clone 'https://gitlab.sib.swiss/EPD/chipseq.git'
(chipseq) cd chipseq/src/tests
(chipseq) ./test_main.sh --environment conda
```

Forcing a Run {#sec:opt-f}
-------------

As noted above (@sec:interface), leftover output files will cause `test_main.sh`
to abort. One way^[The other way is to remove the output files, either manually
or by doing a cleanup run (see @sec:opt-c).] of circumventing this is to pass
option `-f` (long: `--force`), in which case existing output files will be
ignored. They will be removed if the corresponding test succeeds, otherwise they
will be replaced by a new output file (or left untouched if the test isn't run
at all^[Which can be deliberate or the result of an error.]).

Keeping Output Files
--------------------

Ouput files are normally removed if (and only if) the corresponding test
succeeds. It may occasionally be helpful to keep the outputs even in case of
success, for example when debugging the script or adding tests (e.g. when two or
more tests have the same output file or when an outpt file can be manually
derived from another). To prevent deletion of outputs, pass option `-o` (long
form: `--keep-outputs`). 

**NOTE** that the leftover outputs will cause `test_main.sh` to refuse to run
(see @sec:interface). Either remove them nanually, or do a cleanup (see
@sec:opt-c) or pass `-f` (@sec:opt-f).

Keeping Test Case Scripts {#sec:opt-s}
-------------------------

Test case scripts are normally removed if the test succeeds (and only then) -
see @sec:test-results. This can be changed using option `-s` (long forem:
`--keep-scripts`). To unconditionally _keep_ the scripts, pass argument `always`
(that is: `-s always` or `--keep-scripts always`); to unconditionally _remove_
them, pass `never` (these can be shortened to `a` and `n`, respectively).

Keeping Test Outputs {#sec:opt-o}
--------------------

Test outputs are normally removed if the test succeeds (and only then) - see
@sec:test-results. This can be changed using option `-o` (long form:
`--keep-outputs`). The presence of output files will prevent `test_main.sh` from
running; see @sec:opt-c for cleaning up thefiles or @sec:opt-f to force a run.


Cleaning Up {#sec:opt-c}
-----------

Test outputs and test case scripts are normally removed only for successful
tests (unless explicitly requested otherwise, see @sec:opt-o and @sec:opt-s).
The presence of these files prevents the system from running (but see
@sec:opt-f). To remove all these leftover files, pass option `-c` (long form:
`--cleanup`). This will cause `test_main.sh` to remove the files and exit. When
combined with `-v`, this shows the list of files `test_main.sh` tries to
remove.^[The test outputs are removed using a glob (`rm out/*`) because they're
in a directory that contains nothing else, but the test case scripts reside in
the same place as the main test script and commands file, so using a glob here
could inadvertently remove files that should be left alone. To avoid this, the
cleanup function generates a list of all possible test case scripts (for all
environments) and removes any that it finds.]

Colorizing Output
-----------------

Output can be colorized by passing `-C` (long form: `--color`) or by setting the
environment parameter `CHIPSEQ_TESTS_COLOR`. Success reports will then appear in
green, failures in red, and errors in yellow. This only concerns test outputs:
warnings and fatal error messages will still be colorized.

Using Fancy Characters
----------------------

Fancy Unicode characters can be used instead of words by passing `-F` or
`--fancy-chars`. This will signal success by a checkmark^[Which apparently
cannot be rendered in PDF.], failure by a
crossmark, and errors by `?` or `!`.

Return Value
============

`test_main.sh` follows the UNIX convention of returning 0 for success (all tests
succeeded) or nonzero for anything else. This enables calling scripts to check
whether the whole test run suceeded or not.

Meta Tests {#sec:meta-tests}
============================

To help ensure that failures and errors are detected and correctly reported,
the commands file contains a set of tests that deliberately result in failure or
error. Their name starts with `meta_`, and accordingly they are not run by
default (the default patten being `test_*`).

Name          Description
---------     -------
`meta_fail`   Failure (output different from input)
`meta_nocmd`  Error (command not found)
`meta_noexp`  Error (expected output file not found)

To run these tests, either pass `-p 'meta_*'`, or pass option `-M` (long form:
`--meta-tests`), which has the same effect. The run **should report failures and
errors**, as follows (`\`s added to avoid truncating lines):

```bash
$ ./test_main.sh -M
WARNING: -M/--meta-tests will cause leftover scripts and outputs. \
  You may want to do a cleanup run (-c).

Running meta_fail...  failure
Running meta_nocmd... ./meta_nocmd-native.sh: line 16: ../dummy: \ 
  No such file or directory
error
Running meta_noexp... error .exp file (./exp/meta_noexp.exp) not found, \
  not a plain file, or unreadable

0 successes
1 failures
2 errors
```

Adding Test Cases
=================

The test cases are stored in a TSV file named `test_cmds.tsv`. It resides in the
same directory as the main script, `test_main.sh`. It has the following form
(ignoring the leading comment lines):

```bash
<test name>TAB<test command>
```

The `test_name` should be a valid identifier, and the `test_command` can be
almost arbitrary Bash code, including redirection operators (`<`, `>`, etc.) and
pipes (`|`). Note however that when passed to containers (e.g. `docker exec ...
bash -c`) is is surrounded by single quotes to ensure that expansions are done
within the container. Hence the test command __may not contain single quotes__.

Some parameters are available to the test commands (they are defined in the test
scripts or, in the case of Docker, when initializing the container).  These are:

Parameter        Meaning
----------       --------
`$BED2SGA_IN`    Input file for `bed2sga`.
`$BIN_DIR`       Directory where the binaries reside.
`$CHR_SIZE_DIR`  Where to find the chromsome sizes file (see e.g. `chipcenter -i`).
`$COUNTSGA_IN`   Input file for `countsga`.
`$STAT1_MOTIFS`  Input file for `chipcenter`.
`$STAT1_READS`   Input file for various test cases.
`$UT189`         Input file for `chipcor`.

### Example

One line in the file reads:

```bash
test_countsga_STDIN\t"$BIN_DIR"/countsga -c 10 < "$STAT1_READS"
```

This defines a test case named `test_countsga_STDIN`, whose command is
`"$BIN_DIR"/countsga -c 10 < "$STAT1_READS"` To add another test with a
different value of `-c`, for example, one could add the following line to the
file:

```bash
test_countsga_c20_STDIN     "$BIN_DIR"/countsga -c 20 < "$STAT1_READS"
```

**NOTES**

* The test names should be unique. The test run will abort if this is not the
  case.
* By convention, test cases that pass their input data on standard input have
  names ending in `STDIN`, those who pass their input as the first argument have
  names ending in `ARG1`. This allows to distinguish cases that differ only in
  the way the input data is passed. Some programs also allow a `-` to specify
  standard input, the corresponding test cases have names in `STDIN_dash`. 
* In the foreseeable future, the special variables for input files such as
  `$STAT1_READS` may be deprecated, and the pathnames of the files, starting
  from `$DATA_DIR`, used instead.

Checking for Failure
--------------------

In some cases, such as when a file is not found, the program is expected to
fail, that is, to return a nonzero exit status (and usually, to output some
error message). To check that the script does fail, the usual method of
comparing its exit status to 0 will obviously fail. The correct approach is to
test for nonzero after the test command, like this: `<test command>; (($? > 0))`
this will return 0 only if the test command _fails_, e.g.

```
test_bed2sga_amel4_no-i_nochr\t"$BIN_DIR"/bed2sga -s amel4 <<< $'chrLG1\\t0\\t1' 2>&1; (($? > 0))
```

tests what the program does if it cannot find the `chr_NC_gi` file it needs. It
is supposed to fail and output help text on STDERR (hence the `2>&1`).

**Note**: one might be tempted to just add `|| true` afer the test command, but
this will not catch cases in which the program erroneously fails to return a
nonzero status.

Setup and Teardown
------------------

Setup commands can be provided in the form of snippets of code in
the third field of `test_cmds.tsv`, except that setup commands are not mandatory. The test script
will include the command if present, and run it before the test command.

Teardown commands work in the same way, and are found (if present) in the fourth
field `teardown.tsv`. They are executed after the test command, whether it
succeeded or not (although it may fail to be executed if the test command
crashes).

Both setup and teardown must be present or absent - if you define one, you must
define the other. In the (presumably rare) cases when one is needed but not the
other, use `:` as a no-op. For example, the `sga2wigSmooth_FS.pl` test commands
produces an additional output file, `hg19.chrom.sizes`, that is not sent to
STDOUT but written in the current working directory. This needs to be removed,
so in the fourth column of `test_cmds.tsv` we have, for those tests, `rm
hg19.chrom.sizes`. But these tests don't need any setup, so in the third column,
we just have `:`.  Note that we cannot leave this empty, as this would yield a
run of two consecutive TABs, which would count as a _single_ field delimiter.
The line would then have 3 fields - instead of the expected 2 or 4.

Implementation
==============

The test system is built from the following components:

* A Bash script, `test_main.sh`, that resides in `./src/chipseq/src/tests`
  (in the EPD Git repository). Its main task is to run the tests and display the
  results (see @sec:interface).
* A test case file, `test_cmds.tsv`, also found in `./src/chipseq/src/tests`.
  This contains the shell commands that each test consists of, as well as
  possible setup and teardown commands.
* Input data, in `./src/chipseq/data`. These files (mostly SGA) are passed as
  input to programs of the `ChipSeq` suite.
* Expected data, in `./src/chipseq/src/tests/exp`.

Undocumented Features
=====================

Option `-x` switches on `xtrace`, but this is used for debugging and the way it
works (especially how it is passed to the test case scripts) may change.

Bugs
====

No known bugs as of 2023-02-21 (Git commit
`a53a05d5d1a6d023711592db09eebe883402060d`). 
