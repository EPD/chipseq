---
title: Preliminary notes, corrections on the ChIP-Seq_Tools-UsersGuide
date: 9 April 2024
author: Philipp Bucher
---
:

P3 (1.2 Decompressing Data):
============================

make gunzip-data (not working)

_This has been fixed in commit `ac19d07`._

P5 (2.1. Help):
===============

* "Short usage instructions can also be obtained by simply typing the name of
the program without any options or arguments"

Not true for many utilites, which can be run without any options or argument.

_Reflected this, and provided a list of utils who do respond by a help message to
being clled without any arguments._


P5 3. (ChIP-Seq programs: Notes):
=================================

* "FIXME: what about Conda and the containers? These install binaries somewhere
  in the system – they do not compile them in ./src."

I see two possibilities to deal with this issue:

i.  require all users to install the programs before running the examples and
eliminate the prefix "../src" in the user guide,
ii.  provide special instructions for conda, docker and apptainer users (possibly
in an Appendix

_We already have instructions for Conda and the containers (p. 3). But these
could indeed go into an appendix. We could indeed aim for having example
commands that are as similar as possible across the environments (as a matter of
fact, we already do pretty much this in the tests, where the exact same command
line is used everywhere, the environment-related differences being reflected in
variables._

P6 (3.1. chipcor):
==================

* "-C Count cutoff value for all input reads (default: 10)."

Why suddenly upper case -C (seems to be a recent change)

_Fixed (commit 5611fb2)_

P8 (3.2 chipcenter):
====================

* "Directory where the chr_size file is found"

Right after the Table, add something like

(the format and function of the chr_size file are given under #.# Genome
information files)

_Done._

I'm going to write a short chapter "Genome information files". The precise
location in the document still needs to be determined.

* "CTCF.sga.gz"

Should be CTCF.sga

_Done._

P9 (3.3 chipextract):
=====================

* "The strand can also be included as a feature specification in the same way as
  for the reference feature."

This concerne the target feature. However, strand o has no meaning. If specified,
the program produces no output.

Perhaps a sentence should be added to this effect: Proposal:

"The strand can also be included as a feature specification in the same way as
for the reference feature, except that strand o has no meaning in this case and
    leads to empty output)."

_Done._

P10 (3.3 chipextract):
======================

* -A "TSS o”

strange quote ” (discovered here, probably recurrent issue throughout the
document)

_This was due to a backtick before a double quote (`"). Fixed (this instance
as well as all others (n=3) I could find._

P11 (3.4 chippeak):
===================

* "Chippeak works at its best with centered read distributions."

Proposed replacement:

For ChIP-seq data, chippeak works best with centered read positions."

_Done._

P11 (3.4 chippeak):
===================

* "-p Refine peak positions.
-r Synonym for -r ("refine")"

Synonym or deprecated? Actually, I would prefer to keep only -r. Reuse of the -r
option is in fact not problematic, because it has everywhere a different
program- specific meaning. Only those options which control recurrent processes
(-h, -c, -f, -i etc.) should be standardized and should not be used for any
other purposes.

(Debatable of course, perhaps too late to change)

_Reverted to -r (only); all tests ok._

P12: (3.4 chippeak):
====================

* "Where CTCF_centered.sga is the input file containing the list of centered
  CTCF reads."

Add backwards reference to place where it has been generated, e.g. see 3.2.

_I had also spotted the need for a reference and added text to that effect just
before the first example. I kept PB's version_

P12: (3.4 chippeak):
====================

* "With these parameters, the program detects 15’600 peaks."

Why not to give the exact number (15939), or say about 15’600 peaks.  BTW, I
would prefer as output filename CTCF_peaks.sga (instead of CTCF_peaks.sga), to
remember that the output is an SGA file, which potentially could be used as


input to other programs.

_Done - 15,939 (using comma for thousands separator)._

P13: (3.5 chippart):
====================

I. Number of processed sequences, total DNA length, and total number of fragments;
II. Total fragment length, average fragment length, and percentage of DNA length;
III. Percentage of total read counts, average number of counts, and count density.

I propose the following changes:

I. Number of sequences, total length, total counts, total number of fragments;
II. Total fragment length, average fragment length, total length as fraction of total sequence length;
III. Fraction of counts in fragments, average counts per fragment, counts per bp in
fragments.

_Done. For some reason Pandoc gets the indentation of the list wrong._

Notes:
* To be discussed, **please check** whether you understand best. (I feel there is a
  potential for further amendments.)
* I changed percentage to fragments because that's what is displayed (though
  sticking to the truth may be overkill).
* I wouldn't mind propagating these changes to the program code. (The actual
  text in the user guide doesn’t match the text issued by the program).

_As discussed during the TC (2024-04-09 or thereabouts), I think PB's version is
the clearer. I note that there is a difference in the fields in pt. (I): 3
fields as of now, 4 with the proposed changes._

* "The two parameters" -> The two mandatory parameters
* ES.K4_partit.out -> ES.K4_partit.sga? (to be discussed)

_Done. As mentioned in the TC, I'm all in favour of reflecting file format in
the extension_


P14 (3.5 chippart):
===================

* "The output of chippart is an SGA-like format in which each region of interest
  is split into two lines, the beginning and the end, respectively."

Proposal:

The output of chippart conforms to a substandard of the SGA format called
"regions SGA". Each region is defined by two subsequent lines, the first one
(with "+" in the strand field) defines the beginning of the region, the second
(with "-" in the strand field) defines the end of the region.

**Please check**, which sentence you understand this better.

* "It is important to note that all ChIP-Seq programs send their output to
  standard output"

I'm not sure whether this is the best place to mention this. Comes a bit late.
Since the following text includes a command line example involving chippeak, it
could be put under 3.4 chippeak (without the chippart example). In this case,
the pipe version of the example command could be presented under 3.5 chippart,
instead of the current two-step version.

_I tried the following: move the above paragraph to a separate subsection in
sct. 2 ("Basics...", p. 4). The preceding material is now in a new subsection,
"Functions and Formats")._

P14 (3.6 chipscore):
====================

* "The counts corresponding to the target feature are reported in the 6th
  field."

This is not universally true.

Proposal:

The counts corresponding to the target feature are reported in a new field at
the end of each reported reference feature line. This program can thus be used
several times in succession for scoring the same reference feature with
different target feature counts.

> -b <from> Beginning position of the correlation analysis range (relative distance in bp) considered in the output histogram.
> -e <to> End position of the correlation analysis range (relative distance in bp).

The text is misleading, presumably copy-pasted from the chipcor description. I
propose something like:

> -b <from> Beginning position of the vicinity range, in which target features are counted). The position is given as relative distance to the reference feature.

_Done._

Similar changes for the -e <to> option.

_Done._

Note: The help text is also misleading. (to be changed if time permits)

_Couldn't find the same confusing part in the help message - left as is._

P15 (3.6 chipscore):
====================

* "The program extracts reference reads that are enriched (>=) or depleted in
  target reads"
  
I propose:

"The program extracts reference features that are enriched (>=) or depleted (<=)
in target reads"

* "Oriented strand processing. It means reverting the chromosome axis when the
  reference feature is on the – strand."

Proposal: It means reverting the chromosome axis and swapping the strand field
(sign) of the target feature when the reference feature is on the – strand.

Please check whether similar changes should be made to previous explanations of
the -o option in this document)

_Changed it here and in another two places. Could IMHO be simplified to "Revert
the chromosome axis and swap the strand field (sign) of the target feature when
the reference feature is on the – strand."_

* ES_CTCF_peaks.sga.gz, Mm_EPDnew_001_mm9.sga.gz

Remove .gz extension (?)

_Done._

* "TSS_CTCF-enriched.out"

Recurrent issue. Should we use the extension `*sga` for
all output files that conform to the SGA format standards. To be discussed.

_Switched to `.sga`._

* "#FIXME: argument missing in next command"

Next command works! Left-over?

_Yes. Removed._

