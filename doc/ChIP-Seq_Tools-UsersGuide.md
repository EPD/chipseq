---
title: EPD ChIP-Seq Tools User Manual
author:
- Giovanna Ambrosini
- Thomas Junier
- Sébastien Moretti
- Philipp Bucher
date: \today{}
classoption: onecolumn
fontsize: 12pt
geometry:
  - top=30mm
  - left=30mm
  - right=30mm
numbersections: true
toc: true
links-as-notes: true
colorlinks: true
linkcolor: blue
secnumdepth: 2
header-includes:
  - \usepackage{titlesec}
---

\newpage

The EPD ChIP-Seq Tools software suite provides a set of tools performing common
genome-wide ChIP-Seq analysis tasks, including positional correlation analysis,
peak detection, and genome partitioning into signal-rich and signal-poor
regions. 

Conventions {-}
-----------

The term "ChIP-Seq" may refer to either of two concepts:

1. A technique called _Chromatin Immunoprecipitation Sequencing_, and commonly
   abbreviated to "ChIP-Seq" or a variant thereof;
1. The software tools presented in this manual, whose purpose is to analyse
   data produced by the above mentioned technique.

In this document, the term "Chip-Seq" will refer to the technique, while the
phrase "EPD ChIP-Seq Tools" (or sometimes just "ChIP-Seq Tools") will refer to
the software.

The following typographic conventions apply:

Object       Typography    Example
-------      -----------   --------
File name    italics       _CTCF.sga_
Code         monospace     `cd chipseq`

Whole commands are usually prefixed with a `$` that denotes the shell prompt:

```bash
$ conda activate chipseq
```

Installation
============

The ChIP-Seq Tools are known to work (and tested on) MacOS and Linux. This
section describes how to install them on your machine. The examples provided in
this guide make use of data that needs to be downloaded separately (see below).

Obtaining the EPD ChIP-Seq Tools
--------------------------------

The tools are available in the following forms:

* Git repository
* tarball
* container (Docker, Apptainer/Singularity)
* virtual environment (Conda)

We refer to this in the present document as the _environment_. Installs with Git
or a tarball are referred to as _native_. See below for environment-specific
details.

### Git

```bash
$ git clone https://gitlab.sib.swiss/EPD/chipseq.git
$ cd chipseq
$ make && make test && sudo make install
```

### tarball

```bash
$ wget "https://gitlab.sib.swiss/EPD/chipseq/-/archive/v1.6.0/chipseq-v1.6.0.tar.gz"
$ tar xzf chipseq-v1.6.0.tar.gz
$ cd chipseq-v1.6.0
$ make && make test && sudo make install
```

### Docker {#sec:docker-install}

```
$ docker pull sibswiss/chipseq
```

### Apptainer/Singularity {#sec:obt-AS}

These rely on the Docker container, which must therefore have been pulled
beforehand (see @sec:docker-install).

```bash
$ apptainer pull docker://sibswiss/chipseq:latest
# or, as the case may be
$ singularity pull docker://sibswiss/chipseq:latest
```

### Conda {#sec:install-conda}

First install the EPD ChIP-Seq Tools binaries (this only needs be done once):

```bash
$ conda create --name chipseq
$ conda activate chipseq
$ conda install -c sibswiss -c bioconda chipseq
```

The programs are now installed, typically in `~/.conda/envs/chipseq/bin`. 

Obtaining the Example Data
--------------------------

The example data is available from
[https://gitlab.sib.swiss/EPD/chipseq/-/archive/v1.6.0/chipseq-v1.6.0.tar.gz](https://gitlab.sib.swiss/EPD/chipseq/-/archive/v1.6.0/chipseq-v1.6.0.tar.gz).
You can use Curl or Wget to download it:

```bash
$ wget 'https://gitlab.sib.swiss/EPD/chipseq/-/archive/v1.6.0/chipseq-v1.6.0.tar.gz'
$ tar xzf chipseq-v1.6.0.tar.gz
```

### Decompressing the Example Data {#sec:gunzip}

Some of the data files used in the examples below (see @sec:examples) are
distributed in compressed form due to their large size. To run the examples,
they need to be decompressed. To do so, issue the following command in the
top-level directory:

```bash
$ make gunzip-data 
```



Basics about the EPD ChIP-Seq Tools {#sec:basics}
=================================================

Functions and Formats
---------------------

The tools exist as stand-alone C programs and perform the following tasks:

I.  Positional correlation analysis and generation of an aggregation
    plot (AP) (*chipcor*),

II. Extraction of specific genome annotation features around reference
    anchor points (*chipextract*),

III. Read centering or shifting (*chipcenter*),

IV. Narrow peak caller using a fixed width peak size (*chippeak*),

V.  Broad peak caller used for large regions of enrichment (*chippart*),

VI. Feature selection tool based on a read count threshold (*chipscore*).

Because the ChIP-Seq Tools are primarily optimized for speed, they use
their own compact format for ChIP-Seq data representation called SGA
(Simplified Genome Annotation). SGA is a line-oriented, tab-delimited
plain text format with the following five obligatory fields per line:

I.  Sequence ID (char string)

II. Feature (char string)

III. Sequence Position (integer)

IV. Strand (+/- or 0)

V.  Read Counts (integer)

Additional fields may be added, containing application-specific
information used by other programs. In the case of ChIP-Seq data, an SGA
file represents the genome-wide read count distributions from one or
several experiments. However, the format can also represent a large
variety of other types of genomic data, including derived features such
as peaks extracted from ChIP-Seq data, or genome annotations such as
promoters or variants. Any type of genomic feature that can be projected
to a single base on a chromosome can be represented in SGA format.

The *sequence ID* field typically identifies a chromosome. The software does not
impose any naming convention. However, if you merge data from different
experiments, the same names must be used for the same chromosomes.  Most of the
demo data included in this distribution use NCBI/RefSeq accession numbers as
sequence/chromosome identifiers. Chromosome names as used by the UCSC genome
browser constitute another *de facto* standard and are readily converted into
NCBI/RefSeq accession numbers. However, keep in mind that chromosome names are
ambiguous unless the corresponding assembly is indicated. Do not mix chromosome
names from different assemblies, otherwise you will get wrong results.

The *feature* field contains a short code that identifies an experiment.
It often corresponds to the name of the molecular target of a ChIP-Seq
experiment. Its function is to distinguish data lines relating to
different experiments that were merged into a single file. The
*position* field contains the position within the sequence. The *strand*
field indicates the strand to which the feature has been mapped. The SGA
format distinguishes between "oriented" features that occur either on
the plus or on the minus strand of the chromosome sequence, and
"unoriented" features which cannot be assigned to one or the other
strand. Peaks from a ChIP-Seq experiment, for instance, constitute an
example of an unoriented feature. Unoriented features are identified by
a 0 (zero) in field 4. The *counts* field contains the number of reads
that have been mapped to a specific base position on the chromosome. It
can also be used for other purposes as well, such as representing
conservation scores, SNP frequencies or any type of genome annotation
that can be mapped to a single base on a chromosome.

An example of an SGA-formatted file is shown below:

```
NC_000001.9 H3K4me3 4794 + 1
NC_000001.9 H3K4me3 6090 + 1
NC_000001.9 H3K4me3 6099 + 3
NC_000001.9 H3K4me3 6655 + 1
NC_000001.9 H3K4me3 18453 - 1
NC_000001.9 H3K4me3 19285 + 1
NC_000001.9 H3K4me3 44529 + 4
NC_000001.9 H3K4me3 46333 + 1
NC_000001.9 H3K4me3 46349 - 1
```

The EPD ChIP-Seq Tools programs require SGA files to be sorted by sequence name,
position, and strand. In a UNIX environment, the command to properly sort SGA
files is the following:

```bash
$ sort -s -k1,1 -k3,3n -k4,4 <SGA file>
```

Duplicated lines are allowed, even though we provide a program called
*compactsga* to merge repeated lines into a single line adjusting the
count field accordingly.

Pipelines
---------

All ChIP-Seq programs send their output to standard output (`stdout`) and can
therefore be used at the end of a pipeline. Many also accept their data on
`stdin`), that is they are _filters_, and can thus occur at any position in a
pipeline.

Help
----

Documentation for most EPD ChIP-Seq Tools programs is provided via UNIX-style man
pages, type *e.g*.:

```bash
$ man chipcor
...
```

Programs also respond to option `-h` by emitting a help message on the standard
error streams.

```bash
$ chipcor -h
...
```

Some programs also output the same message when called without arguments.^[Those
that do not are *bed2sga*, *compactsga*, *countsga*, *featreplace*,
*filter_counts*, *rmsk2bed.pl*, *rmsk2sga.pl*, and *test_main.sh*.]

EPD ChIP-Seq Tools programs: command-line usage and examples of use {#sec:examples}
=========================================================


**NOTES**

1. In the native and Conda environments, the example commands below are meant to
   be run in the `./doc` subdirectory. For the Docker environment, the examples
   may conceivably be run anywhere since Docker uses absolute paths for its
   mounts.
1. Some files, such as `CTCF.sga`, are large, so they are distributed in
   compressed form (e.g. `CTCF.sga.gz`). They need to be decompressed before the
   examples below can be followed -- see @sec:gunzip. 
1. Some example involve the UNIX `sort` command, which works differently in
   different locales (mostly reflecting the user's language settings). The
   examples in this document assume the "C" locale, which can be set by issuing,
   depending on your shell:

    ```bash
    $ export LANG=C # Bash, Zsh, Ksh, POSIX, & other Bourne-like shells
    ```

    ```csh
    % setenv LANG C # csh, tcsh 
    ```

    ```fish
    > set -gx LANG C # Fish
    ```

    There are other environment variables that control the locale, but they are
    beyond the scope of this guide --- see `LANG` in your shell's manual page.

### Native

The example commands are written for the native environment, so they can be run
without modification.

### Docker

To run the examples, the directory that contains the data files must be mounted,
e.g. with the `--mount` option. Assuming the environment variable
`CHIPSEQ_DATA_DIR` is set to the path to this directory, the general syntax for
running Docker is:

```bash
$ docker run --mount \
  type=bind,source=$CHIPSEQ_DATA_DIR,target=/home/chipseq/data \
  sibswiss/chipseq:latest \
  <sib-chipseq-tools-command> [args]
```

The *chipcor* example ([listing @lst:chipcor-xpl1]) can thus be run as follows:

```bash
$ CHIPSEQ_DATA_DIR=/path/to/data # replace with actual path
$ docker run --mount \
  type=bind,source=$CHIPSEQ_DATA_DIR,target=/home/chipseq/data \
  sibswiss/chipseq:latest \
  chipcor -A "CTCF +" -B "CTCF -" -b -1000 -e 1000 -w 1 -c 1 -n 1 \
  ./data/CTCF.sga > CTCF.out
```

### Apptainer/Singularity

**Note**: the Apptainer/Singularity container is based on the Docker one, and
must be pulled prior to use (see @sec:obt-AS).  Contrary to Docker, there is no
need to mount local disks. The general syntax is thus:

```bash
$ singularity exec chipseq_latest.sif <cmd>
```

Assuming the environment variable `CHIPSEQ_DATA_DIR` is set to the path to this
directory, the general syntax for running a Singularity/Apptainer container is:

```bash
$ singularity exec chipseq_latest.sif /usr/bin/chipcor \
  -A "CTCF +" -B "CTCF -" -b -1000 -e 1000 -w 1 -c 1 -n 1 \
  "$CHIPSEQ_DATA_DIR"/CTCF.sga > CTCF.out

```

### Conda

First, activate the `chipseq` environment (see @sec:install-conda for
installing it):

```bash
$ conda activate chipseq
```

This takes care of setting the `PATH` environment variable so that the SIB
ChIP-Seq Tools programs are found. The examples can then be run without further
modification:

```bash
# Assuming the current dir is ./doc
(chipseq) $ chipcor -A "CTCF +" -B "CTCF -" \
    -b -1000 -e 1000 -w 1 -c 1 -n 1 ../data/CTCF.sga
```

*chipcor*
---------

```bash
$ chipcor [OPTIONS] -A <feature A> -B <feature B>  \
          -b <from> -e <to> -w <window> [<] <SGAfile>
```

The *chipcor* program reads a data file (or from `stdin`) in SGA
format (`<SGAfile>`) and generates a count correlation histogram or
aggregation plot (AP), which is a text file that indicates the frequency
of the target feature (`<feature B>`) as a function of the relative
distance to the reference feature (`<feature A>`). The output of
*chipcor* can be easily imported into any statistical or graphical tool,
such as R or Gnuplot, to generate an AP.

Mandatory parameters are:

| **Parameter**   | **Description**                                                                                                                                            |
| --------------- | -------------------------------------------------------                                                                                                    |
| `-A <feature>`  | Feature field name selecting reference reads in the SGA file.  The strand can also be included as a feature specification (`<feature> = <name> [+\| -]`).  |
| `-B <feature>`  | Feature field name selecting target reads in the SGA file. The strand can also be included as a feature specification (`<feature> = <name> [+\| -]`).      |
| `-b <from>`     | Beginning position of the correlation analysis range (relative distance in bp) considered in the output histogram.                                         |
| `-e <to>`       | End position of the correlation analysis range (relative distance in bp).                                                                                  |
| `-w <window>`   | The window defines the histogram step size or bin.                                                                                                         |

Options are:

| **Option**  | **Description**                                                                                                                                                                                                                                                                                             |
| ----------- | ------------------------------------------------------                                                                                                                                                                                                                                                      |
| `-h`        | Show the program usage.                                                                                                                                                                                                                                                                                     |
| `-d`        | Produce debugging output and check whether the input SGA is properly sorted.                                                                                                                                                                                                                                |
| `-o`        | Revert the chromosome axis and swap the strand field (sign) of the target feature when the reference feature is on the – strand.                                                                                                                                                                            |
| `-C`        | Count cutoff value for all input reads (default: 10).                                                                                                                                                                                                                                                       |
| `-n`        | Histogram normalization. By default is 0, meaning that histogram entries display raw read counts. If `-n 1`, histogram entries display the count density (#counts/bp) of the target feature. If `-n 2`, histogram entries represent target feature abundance as a fold change relative to a genome average. |

The exact position of the window along the range is defined such that
the window is always centered at position 0. As a result, the range
might be slightly reduced.

Here is an example of using *chipcor* to estimate the average fragment size of
CTCF reads:

```{#lst:chipcor-xpl1 .bash caption="*chipcor* example"}
$ ../src/chipcor -A "CTCF +" -B "CTCF -" -b -1000 -e 1000 \
  -w 1 -c 1 -n 1 ../data/CTCF.sga > CTCF.out
```

where:

> `CTCF.sga` is the input file containing the list of mapped CTCF reads
>
> `-A "CTCF +"` is the reference feature (CTCF plus strand)
>
> `-B "CTCF -"` is the target feature (CTCF minus strand)
>
> `-b` is the beginning of the range considered in the output histogram
>
> `-e` is the end of the range
>
> `-w` is the window width
>
> `-c` is the count cutoff value
>
> `-n` is the normalization mode (1 means "count density")
>
> `CTCF.out` is the output file containing the histogram values in text format.


In the above example, the reference and target features were contained
in the same input file. This is not always the case. In the following
example, we will analyze the distribution of CTCF peaks in mouse
embryonic stem cells relative to transcription start sites from the
Eukaryotic Promoter Database EPD. The input data for this analysis are
provided in two separate files: *ES_CTCF_peaks.sga.gz* for CTCF peaks in
mouse ES cells, and *Mm_EPDnew_001_mm9.sga.gz* for TSSs in mouse.

These files need first to be decompressed (see @sec:gunzip), then they must be
merged before they can be processed by *chipcor*.

```bash
$ sort -s -m -k1,1 -k3,3n -k4,4 ../data/Mm_EPDnew_001_mm9.sga \
  ../data/ES_CTCF_peaks.sga > merged.sga
$ ../src/chipcor -A TSS -B CTCF_P -b -2500 -e 2500 -w 50 \
  -c 1 -n 1  -o merged.sga > CTCF_peaks.out
```

When the 'oriented' option (`-o`) is set, the strand of the reference read
is taken into account in order to calculate the relative distance
between the reference read and all the target reads that fall within the
correlation range. In other words, the chromosome axis is reverted each
time the reference feature, namely the TSS in this example, is on the
minus strand.

*chipcenter*
------------

```bash
$ chipcenter [OPTIONS] [-f <feature>] -s <shift> [<] SGAfile
```

The *chipcenter* program reads a data file (or from `stdin`) in SGA
format (`<SGAfile>`), and shifts (by `<shift>` bases) read positions
corresponding to a specific feature (`<feature>`) to the estimated
center positions of DNA fragments. If no feature specification is set,
the program accepts all oriented lines of the input SGA. Strandless
features are ignored. The default output of *chipcenter* is a list of
shifted oriented positions in SGA format. If necessary, one can set the
output strand to zero by means of the `-z` option. The `-r` option is
used to replace the feature field with a new string.

The main parameters are:

| **Parameter**   | **Description**                                                                                                                                            |
| --------------- | -------------------------------------------------------                                                                                                    |
| `-f <feature>`  | Select a subset of input reads.  The feature name is specified in field 2 of the SGA-formatted input. If no feature is given, then all reads are selected. |
| `-s <shift>`    | Relative shift (in bp) of read positions to estimated center positions of DNA fragments.                                                                   |

Options are:

| **Option**       | **Description**                                                              |
| ---------------- | ------------------------------------------------------                       |
| `-h`             | Show the program usage.                                                      |
| `-d`             | Produce debugging output and check whether the input SGA is properly sorted. |
| `-i <dir>`       | Directory where the *chr_size* file is found                                 |
| `-z`             | Set output strand to zero.                                                   |
| `-c`             | Count cutoff value for all input reads (default: 10).                        |
| `-S`             | New feature name (for feature replacement)                                   |
| `-r`             | Deprecated, please use `-S`.                                                 |

(the format and function of the *chr_size* file are given under #.# Genome
information files).

In most applications, *chipcenter* is used as a data preprocessing step.

Here is an example of using *chipcenter* in order to center mapped CTCF
reads from human CD4+ cells:

```bash
$ ../src/chipcenter -i ../data -f CTCF -s 40 ../data/CTCF.sga \
  > CTCF_centered.sga
```

where:

> `CTCF.sga` is the input file containing the list of mapped CTCF reads
>
> `-f CTCF` is the feature name
>
> `-s 40` is the read position shift

For ChIP-Seq data, the shift value corresponds to half the average
fragment size of the sequence reads as estimated by the 5'end-3'end
correlation diagram produced by *chipcor*.

*chipextract*
-----------

```bash
$ chipextract [OPTIONS] -A <feature A> -B <feature B> \
  -b <from> -e <to> -w <bin> [<] SGAfile
```

The *chipextract* program reads a data file in SGA format (`<SGAfile>`)
containing two features, a reference feature (`<feature A>`) and a target
feature (`<feature B>`). It can also read from `stdin`. For each reference
feature, it extracts target feature reads that fall into a distance range
([`<from>, <to>`]) relative to the reference feature.  The output of
*chipextract* is a table in text format consisting of all reference features
(rows) with relative target read counts in bins of a given `<bin>` size
(columns). For visualization purposes, the *chipextract* table can be easily
converted into a heat map, using `R` or similar tools.

Mandatory parameters are:

| **Parameter**  | **Description**                                                                                                                                                                                                                                                                    |
| ----------     | --------------------------------------------------------                                                                                                                                                                                                                           |
| `-A <feature>` | Feature field name selecting reference reads in the SGA file. The strand can also be included as a feature specification, the format being the following: `<feature> = <name> [+-0(strandless)a(any)o(oriented)]`. If the strand is not specified, it is set to a(any) by default. |
| `-B <feature>` | Feature field name selecting target reads in the SGA file. The strand can also be included as a feature specification in the same way as for the reference feature, except that strand o has no meaning in this case (and leads to empty output).                                  |
| `-b <from>`    | Beginning position of the correlation analysis range (relative distance in bp).                                                                                                                                                                                                    |
| `-e <to>`      | End position of the correlation analysis range  (relative distance in bp).                                                                                                                                                                                                         |
| `-w  <bin>`    | Columns width (in bp) of the heat map table.                                                                                                                                                                                                                                       |

Options are:

| **Option** | **Description**                                                              |
| ---------- | ------------------------------------------------------                       |
| `-h`       | Show the program usage.                                                      |
| `-d`       | Produce debugging output and check whether the input SGA is properly sorted. |
| `-c`       | Count cutoff value for all input reads (default: 1).                         |

The exact position of the sliding window along the range is defined such
that the window is always centered at position 0. As a result, the range
might be slightly reduced.

Here is an example of using *chipextract* to analyze H3K4 histone
modifications in mouse embryonic stem cells relative to transcription
start sites from the Eukaryotic Promoter Database EPD. We would like to
extract H3K4 reads around transcription initiation sites (TSS). The
input data for this analysis are provided in two separate files:
*ES.K4.sga.gz* for H3K4me3 histone marks in mouse ES cells, and
*Mm_EPDnew_001_mm9.sga.gz* for TSSs in mouse.

We first need to center the histone reads (after decompressing the files -- see
@sec:gunzip) to estimated middle-positions of DNA fragments. We shift the reads
by 75 bp according to the 5'end-3'end correlation analysis:

```bash
$ ../src/chipcenter -i ../data -f H3K4me3 -s 75 ../data/ES.K4.sga \
  > ES.K4_centered.sga
```

As for *chipcor*, the input files first need to be merged before they
can be processed by *chipextract*:

```bash
$ sort -s -m -k1,1 -k3,3n -k4,4 ../data/Mm_EPDnew_001_mm9.sga \
  ES.K4_centered.sga > ES.TSS.K4_merged.sga
```

```bash
$ ../src/chipextract -A "TSS o" -B "H3K4me3 a" -b-1000 -e1000 -w20 \
  ES.TSS.K4_merged.sga > ES.TSS.K4_heatmap.out
```

where:

> `ES.TSS.K4_merged.sga` is the input file containing the list of
> centered histone marks reads together with the TSSs
>
> -`A "TSS o"` is the reference feature (oriented TSS)
>
> -`B "H3K4me3 a"` is the target feature (H3K4me3 any, i.e. on both
> strands)
>
> `-b` is the beginning of the range considered in the output table
>
> `-e` is the end of the range
>
> `-w` is the bin size (in bp)
>
> `ES.TSS.K4_heatmap.out` is the heat map table in text format
> consisting of target read counts in bins of 20bp relative to each TSS

*chippeak*
----------

```bash
$ chippeak [OPTIONS] [-f <feature>] -t <threshold> \
  -w <window> -v <vicinity> [<] SGAfile
```

The *chippeak* program reads a data file (or from `stdin`) in SGA
format (`<SGAfile>`), and detects signal peaks for read positions
corresponding to a specific feature (`<feature>`) when given. If no
feature is specified, all input reads or positions are equally
processed. The output is a list of peak center positions in SGA format.
If reads or positions on both strands are selected, the peak list is
strandless, whereas if features on only one strand are selected, the
resulting peak list is oriented. The peak counts are reported in the
field 5. The mandatory parameters are:

| **Parameter**     | **Description**                                                                                                                                            |
| ----------------- | -------------------------------------------------------                                                                                                    |
| `-f <feature>`    | Select a subset of input reads.  The feature name is specified in field 2 of the SGA-formatted input. If no feature is given, then all reads are selected. |
| `-t <threshold>`  | Peak threshold. Cumulative read counts within a range `<window>` should be greater than or equal to threshold `<threshold>.` The default value is 50.      |
| `-w <window>`     | Integration range (in bp) of read counts around each read position across the whole read distribution.                                                     |
| `-v <vicinity>`   | Minimal distance (in bp) amongst a group of local peak maxima (high count values).                                                                         |

The *chippeak* program implements a very simple method that works as follows.
The number of reads is counted in a sliding window of fixed width. Speed is
gained by considering only those windows that have at least one read at the
center position. At the end, all windows which have a number of reads not
smaller than the threshold value `<t>`, and in addition are locally maximal
within a so-called "vicinity range" `<v>`, are reported as peaks. Note that only
the peak mid-point positions are included in the output. If "peak refinement" is
selected, a post-processing step is turned on so as to improve peak location.
For initially-selected peaks, the position is recomputed as the center of
gravity of the counts in the region defined by the window `<w>` parameter.

For ChIP-Seq data, *chippeak* works best with centered read positions.

Options are:

| **Option**       | **Description**                                                                                                                                  |
| ---------------- | ------------------------------------------------------                                                                                           |
| `-h`             | Show the program usage.                                                                                                                          |
| `-d`             | Produce debugging output and check whether the input SGA is properly sorted.                                                                     |
| `-i <dir>`       | Directory where the *chr_size* file is found                                                                                                       |
| `-o`             | Revert the chromosome axis and swap the strand field (sign) of the target feature when the reference feature is on the – strand.                 |
| `-p`             | Refine peak positions.                                                                                                                           |
| `-r`             | Synonym for `-r` ("refine")                                                                                                                      |
| `-c`             | Count cutoff value for all input reads (default: 10).                                                                                            |

Here is an example of using *chippeak* on the CTCF centered reads produced
previously:

```bash
$ ../src/chippeak -i ../data -f CTCF -t 15 -w 200 -v 200 CTCF_centered.sga \
  > CTCF_peaks.out
```

where:

> `CTCF_centered.sga` is the input file containing the list of centered
> CTCF reads (see the examples in Section 3.2 (*chipcenter*)).
>
> -`f CTCF` is the feature name
>
> `-t 15` is the peak threshold (in read counts)
>
> `-w 200` is the window width
>
> `-v 200` is the vicinity range

With these parameters, the program detects 15,939 peaks.

*chippart*
----------

```bash
$ chippart [OPTIONS] [-f <feature>] -p <transition penalty> \
           -t <density threshold> [<] SGAfile
```

The *chippart* program reads a data file (or from `stdin`) in SGA
format (`<SGAfile>`), and finds signal-enriched regions for read
positions corresponding to a given feature `<feature>`. If no feature is
specified, all input reads are equally processed. It outputs a list of
signal-enriched regions in SGA format in two lines (beginning of the
region, end of the region). For *chippart* the strand field of the
output has a different meaning: the '+' character indicates the
beginning of the region, whereas the '-' character indicates the end of
the region.

The main parameters are:

| **Parameter**             | **Description**                                                                                                                                                                             |
| ------------------        | ----------------------------------------------------                                                                                                                                        |
| `-f <feature>`            | Select all subset of input reads.  The feature name is specified in field 2 of the SGA-formatted input. If no feature is given, then all reads are selected.                                |
| `-p <transition penalty>` | Negative score to the transition between signal-enriched and signal-poor regions. The parameter needs to be negative. It controls the fragment length, i.e. high penalty -> long fragments. |
| `-t <density threshold>`  | Count density threshold. A region must have a count density higher than the density threshold to be considered as a signal-enriched DNA stretch.                                            |
| `-s`                      | Deprecated, please use `-t`.                                                                                                                                                                |

The program also generates (to `stderr`) a statistical report with
the following information:

I. Number of sequences, total length, total counts, total number of fragments;

II. Total fragment length, average fragment length, total length as fraction of
    total sequence length;

III. Fraction of counts in fragments, average counts per fragment, counts per bp
     in fragments.

The two mandatory parameters of the program are used to optimize a partitioning
scoring function by means of a fast dynamic programming algorithm. The
scoring function is defined as a sum of scores of:

1.  Transition penalty (*penalty*)

2.  Signal-enriched scores : *length* \* (*local count-density* - *threshold*)

3.  Signal-poor regions : *length* \* (*threshold* - *local count-density*)

Options are:

| **Option**       | **Description**                                                              |
| ---------------- | ------------------------------------------------------                       |
| `-h`             | Show the program usage.                                                      |
| `-d`             | Produce debugging output and check whether the input SGA is properly sorted. |
| `-c`             | Count cutoff value for all input reads (default: 10).                        |

The *chippart* program is most suited to find signal regions such as
histone modifications that spread over large DNA regions.

Here is an example of using *chippart* to analyze H3K4 histone
modifications in mouse embryonic stem cells. As we did previously, we
first need to center the histone reads to estimated middle-positions of
DNA fragments. We shift the reads by 75 bp according to the 5'end-3'end
correlation analysis:

```bash
$ ../src/chipcenter -i ../data -f H3K4me3 -s 75 ../data/ES.K4.sga \
  > ES.K4_centered.sga
$ ../src/chippart -f H3K4me3 -p-10 -s0.014 ES.K4_centered.sga \
  > ES.K4_partit.out
```

where:

> `ES.K4_centered.sga` is the input file containing the list of centered
> histone marks reads
>
> `-f H3K4me3` is the feature name
>
> `-p-10` is the transition penalty
>
> `-t 0.014` is the count density threshold

The output of *chippart* conforms to a substandard of the SGA format called
"regions SGA". Each region is defined by two subsequent lines, the first one
(with "+" in the strand field) defines the beginning of the region, the second
(with "-" in the strand field) defines the end of the region.  This SGA-like
format can be easily converted into BED format, using the conversion tool
*partit2bed.pl*.

Here is an example of such pipeline using *chipcenter* and *chippart*:

```bash
$ ../src/chipcenter -i ../data -f H3K4me3 -s 75 ../data/ES.K4.sga \
  | ../src/chippart -f H3K4me3 -p-10 -t0.014 > ES.K4_partit.out
  | chippart -f H3K4me3 -p-10 -s 0.014
```

Another example using *chipcenter* and *chippeak* for peak calling is
the following:

```bash
$ ../src/chipcenter -i ../data -f CTCF -s 40 ../data/CTCF.sga \
  | ../src/chippeak -i ../data -f CTCF -t 15 -w 200 -v 200 \
  > CTCF_peaks.out
```


*chipscore*
-----------

```bash
$ chipscore [OPTIONS] -A <feature A> -B <feature B> \
  -b <from> -e <to> [-t <thres>] [<] SGAfile
```

The *chipscore* program reads a data file (or from `stdin`) in SGA format
(`<SGAfile>`) and extracts all reference sites (`<feature A>`) that are enriched
or depleted in target feature (`<feature B>`) sites according to a given count
threshold or score (`<thres>`). Output SGA lines are those reference features
that are enriched or depleted in target feature.  The counts corresponding to
the target feature are reported in a new field at the end of each reported
reference feature line. The program can thus be used several times in succession
for scoring the same reference feature with different target feature counts.

The mandatory parameters are:

| **Parameter**    | **Description**                                                                                                                                                                                                                        |
| ---------------- | ------------------------------------------------------                                                                                                                                                                                 |
| `-A <feature>`   | Feature field name selecting reference reads in the SGA file. The strand can also be included as a feature specification (`<feature> = <name> [+|-]`).                                                                                 |
| `-B <feature>`   | Feature field name selecting target reads in the SGA file. The strand can also be included as a feature specification (`<feature> = <name> [+|-]`).                                                                                    |
| `-b <from>`      | Beginning position of the vicinity range, in which target features are counted. The position is given as relative distance to the reference feature.                                                                                   |
| `-e <to>`        | End position of the vicinity range, in which target features are counted. The position is given as relative distance to the reference feature.                                                                                         |
| `-t <thres>`     | Output threshold or score. The program extracts reference features that are enriched (>=) or depleted (<=) in target reads according to a given threshold `<thres>`.  This is not a mandatory parameter and it is set to 0 by default. |

Options are:

| **Option**       | **Description**                                                                                                                   |
| ---------------- | ------------------------------------------------------                                                                            |
| `-h`             | Show the program usage.                                                                                                           |
| `-d`             | Produce debugging output and check whether the input SGA is properly sorted.                                                      |
| `-c`             | Count cutoff value for all input reads (default: 10).                                                                             |
| `-o`             | Reverse the chromosome axis and swap the strand field (sign) of the target feature when the reference feature is on the – strand. |
| `-R`             | Reverse extraction process. The program extracts reference sites that are depleted in target sites.                               |
| `-r`             | Older synonym of `-r`.                                                                                                            |
| `-q`             | Report feature B read counts as 'feature name =`<int>`' (in field 6).                                                     |

Here is an example of using *chipscore* to extract transcription start sites
from the Eukaryotic Promoter Database EPD that are enriched in CTCF peaks. The
input data for this analysis are provided in two separate files:
*ES_CTCF_peaks.sga* for CTCF peaks in mouse ES cells, and
*Mm_EPDnew_001_mm9.sga* for TSSs in mouse. They need to be uncompressed prior
to running this example --- see @sec:gunzip.

```bash
$ sort -s -m -k1,1 -k3,3n -k4,4 ../data/Mm_EPDnew_001_mm9.sga \
  ../data/ES_CTCF_peaks.sga > merged.sga
$ ../src/chipscore -A TSS -B CTCF_P -b -300 -e 10 -t 1 -c 1 \
  -o merged.sga > TSS_CTCF-enriched.sga
```

where:

> `merged.sga` is the input file containing the list of mapped TSSs and
> CTCF peaks
>
> -`A TSS` is the reference feature
>
> -`B CTCF` is the target feature (CTCF peaks)
>
> `-b` is beginning of the range considered in the analysis
>
> `-e` is the end of the range
>
> `-c` is the count cutoff value
>
> `-t` is the count threshold
>
> `-o` is the oriented option (TSSs are oriented features)
>
> `TSS_CTCF-enriched.out` is the output file containing those TSS's that
> contain a CTCF peak.

Alternatively, one can run the following pipeline:

```bash
$ sort -s -m -k1,1 -k3,3n -k4,4 ../data/Mm_EPDnew_001_mm9.sga \
  ../data/ES_CTCF_peaks.sga \
  | ../src/chipscore -A TSS -B CTCF_P -b -300 -e 10 -t 1 -c 1 -o \
  > TSS_CTCF-enriched.out
```


Auxiliary Tools for data reformatting
=====================================

We also provide a series of auxiliary tools (Perl scripts and C programs) that
can be used to perform format conversion tasks (see detailed descriptions of
individual tools at the end of this section).

Most of the ChIP-Seq data sets come in BAM or BED formats. If you have
BAM files, and you need to convert them to SGA format, the steps to
follow are:

1.  Find out which genome assembly has been used to generate the BAM
    files, and make sure that the chromosome names agree with the naming
    scheme of the UCSC genome browser.

2.  Then use the following type of command:

    ```bash
    $ bamToBed -i reads.bam | bed2sga -f <feature> \
      -s <species> | sort -s -k1,1 -k3,3n -k4,4 \
      | compactsga > reads.sga
    ```

The *bamToBed* program belongs to the **BEDTools** package, a suite of
utilities for comparing genomic features that can be installed from:

> [`https://github.com/arq5x/bedtools2`](https://github.com/arq5x/bedtools2)

*bamToBed* converts BAM alignments to BED format. The \'*bed2sga\'* tool
is a C program that converts BED files to SGA format. To find
instructions how to use it, type:

```bash
$ bed2sga -h
```

SGA format requires a *feature* field (field 2) containing a
character string that defines the genomic feature represented by the
file. The BED file does not have an equivalent field. You therefore need
to supply a feature for the conversion via the *-f* option. The
*species* is the name used by UCSC for the genome assembly to which the
reads have been mapped (*e.g.* hg18, mm9, dm3, etc.). *bed2sga* will
automatically convert UCSC chromosome names into corresponding
NCBI/RefSeq accession numbers. Currently we provide chromosome ID
conversion for the following assemblies: *hg19*, *hg18*, *mm8*, *mm9*,
*mm10*, *ce4*, *ce6*, *dm3*, *dm6*, *danRer4*, *danRer5*, *danRer7*, *rn4*,
*rn5*, *panTro2*, *sacCer3*, *araTha1*, *spo2* (*S. pombe*), and *plasFalc1* (*P
.falciparum*). The *species* is only required if one wants to translate
UCSC-based chromosome names into NCBI RefSeq identifiers for comparison
with data that use NCBI identifiers.

The information required to map NCBI RefSeq identifiers to chromosome
numbers is provided by a binary table called *chro_idx.nstorage*. The
binary file *chro_idx.nstorage* includes a hash table in Perl that, for
each assembly, stores chromosome number-NCBI identifier pairs as well as
chromosome lengths indexed by chromosome NCBI identifiers.

The reasons why sorting is required have been explained before. The
program \'*compactsga\'* is a C program that merges lines corresponding
to the same genome position (identical sequence name, position and
strand). For instance, the two input lines

```
  NC_000001.9 H3K4me3 5011 + 1
  NC_000001.9 H3K4me3 5011 + 1
```
are replaced by the following single line:

```
  NC_000001.9 H3K4me3 5011 + 2
```

Compacting SGA files saves space and reduces program execution time, but
unlike sorting is not formally required by the ChIP-Seq Tools.

The *bed2sga* has two basic modes of operations, *centered* and
*oriented*. In the *centered* mode, the midpoint between the *start* and
*end* position from the BED line (fields 2 and 3) will be used as
position. In the *oriented* mode, the conversion depends on the *strand*
indicated in the BED file (field 6). If the strand is + then the
value of the *start* field will be incremented by one and used as
position in the SGA file. If the strand is -, the value of the *end*
field will be used as position in the SGA file. Incrementing the start
position in *oriented* mode is necessary because the BED format has a
"zero-based" numbering system for chromosomal regions whereas SGA has a
1-based numbering system. The behavior of the two conversion modes is
illustrated by the following example:

BED input:


```
  chr1 100000266 100000291 . 0 +
  chr1 100000383 100000408 . 0 -
```

SGA output, centered mode:

```
  NC_000001.9 CHIPSEQ 100000278 + 1
  NC_000001.9 CHIPSEQ 100000395 - 1
```

SGA output, oriented mode:

```
  NC_000001.9 CHIPSEQ 100000267 + 1
  NC_000001.9 CHIPSEQ 100000408 - 1
```

By default, the conversion mode depends on the contents of the BED file. If the
strand field (which is optional in BED) is present, the conversion will be done
in *oriented* mode. Otherwise, the conversion will be done in centered mode, and
the strand field will be set to zero.  However, centered mode can be forced by
passing option `-c`.

The most common command pipeline to perform BED-to-SGA conversion is the
following:

```bash
$ bed2sga -f <feature> -s <species> reads.bed \
  | sort -s -k1,1 -k3,3n -k4,4 | compactsga > reads.sga
```

There are several additional reformatting programs that may be useful in
certain situations. The C program *featreplace* replaces all feature
names in an SGA file by a new name:

```bash
$ featreplace -f <feature> old.sga > new.sga
```

The formatting programs are typically used together with other
formatting tools belonging to publicly available software packages.

One example of such software is the *liftOver* program from UCSC that
can be used to convert chromosomal coordinates in a BED file from one
genome assembly to another, *e.g.*:

```bash
$ liftOver input.bed /db/liftOver/mm8ToMm9.over.chain.gz output.bed trash
```

Chain files for *liftOver* can be downloaded from the [UCSC Website](https://genome.ucsc.edu/).

Additional conversion scripts include:

* *sga2bed* SGA to BED
* *partit2bed.pl* Output of Partitioning tool (special SGA) to BED
* *fps2sga.pl* FPS to SGA
* *sga2fps.pl* SGA to FPS
* *gff2sga.pl* GFF to SGA
* *sga2gff.pl* SGA to GFF
* *sga2wig* SGA to WIG in both *fixedStep* and *variableStep* formats
* *bed2bed_display* BED to BED track format suitable for displaying
* ChIP-Seq peaks

The last two programs produce custom track files that can be uploaded to
the UCSC genome browser for visualization. The FPS (Functional Position
Set) format is the specific format used by the Signal Search Analysis
(SSA) tools, a set of programs developed by our group for motif analysis
. ([`https://epd.expasy.org/ssa/`](https://epd.expasy.org/ssa/))

*filter_counts*
---------------

```bash
$ filter_counts [OPTIONS] [<] <SGAfile>
```

The program reads a ChIP-Seq data file (or from `stdin` `[<])` in SGA format (`<SGA
File>`) and filters out all read counts that occur within user-defined regions of
interest, such as Repeat Mask regions (feature=`RMSK`).  If 'Retain Mode' is on,
it only retains those lines that are within the user-defined regions, in which
case the annotation of the user-defined regions is added to the output SGA file.

Options are:

| **Option**  | **Description**                                                        |
| ------      | ------------------------------------------------------                 |
| `-h`        | Show this help text                                                    |
| `-d`        | Print debug information                                                |
| `-f`        | Feature defining the user-defined regions of interest (default=`RMSK`) |
| `-k`        | Retain Mode on ("keep")                                                |
| `-r`        | Older synonym for `-k` ("retain")                                      |

Note that this programs requires a "mixed" SGA file as input with lines
attributed to at least two different features, one defining the filtering
regions, all others being the target of filtering. This program was initially
developed for filtering out ChIP-Seq reads or peaks falling into annotated
repeat regions. However, it can be used for plenty of other purposes such as
attributing any kind of genomic features to exons, introns, or intergenic
regions.

*compactsga*
------------

```bash
$ compactsga [options] < <SGA File>
```

The program reads a ChIP-Seq data file in SGA format (`<SGA File>`),
and merges equal tag positions into a single line adjusting the count
field accordingly

Options are:

| **Option** | **Description**         |
| ---------- | ---------------         |
| `-h`       | Show this help text     |
| `-d`       | Print debug information |


*featreplace*
-------------

```bash
$ featreplace [OPTIONS] -f <feature name> [<] <SGA File>
```

The program reads a ChIP-Seq data file (or from `stdin` `[<]`) in SGA format
(`<SGA File>`), and changes the name of the feature field according to the
specified parameter `<feature name>` (by default `<feature name>`='FT').

Parameters are:

| **Parameters** | **Description** |
| ---            | --------------- |
| `-f`           | feature name    |

Options are:

| **Option** | **Description**         |
| ------     | ---------------         |
| `-h`       | Show this help text     |
| `-d`       | Print debug information |

*bed2sga*
---------

```bash
$ bed2sga [OPTIONS] [-a <assembly>] [<] <BED file|stdin>
```

Convert BED format into SGA format.

Options are:

| **Option**                 | **Description**                                                                                                               |
| -------------------------- | ----------------------------------------------                                                                                |
| `-d|--debug`               | Produce debug information                                                                                                     |
| `-h|--help`                | Show this help text                                                                                                           |
| `-i|--db <path>`           | Use `<path>` to locate the assembly-specific *chr_NC_gi* file \[default is current working directory\]                        |
| `-f|--feature <ft>`        | Set Feature name `<ft>`                                                                                                       |
| `-a|--species <spec>`      | Assembly `<spec>` (e.g. hg19)                                                                                                  |
| `-s`                       | Deprecated, please use `-a`.                                                                                                  |
| `-C|--center`              | Generate a Centered SGA file                                                                                                  |
| `-c`                       | Deprecated, please use `-C|--center`                                                                                          |
| `-u|--unoriented`          | Generate an unoriented SGA file                                                                                               |
| `-r|--regional`            | Generate a 2-line\[+/-\] SGA file representing BED regions (e.g. RepeatMasker regions)                                        |
| `-x|--extend <f1,f2,...>`  | Produce an extended SGA file with additional fields specified by a comma-separated list of BED column numbers (from 1 to *n*) |
| `--useScore`               | Use the BED field 5 ('score') to set the SGA 'count' field                                                                   |
| `--useSigVal`              | Use the BED field 7 ('Signal Value') to set the SGA 'count' field \[This option is only valid for ENCODE narrowPeak\]        |
| `--narrowPeak`             | Use this option for ENCODE narrowPeak format \[Options `--useScore`, `-C`, and `-u` are ignored\]                             |

#### Input processing:

Without option `-a`, all sequence names (field 1 of BED input) will be accepted
and transferred as is to SGA output. If an assembly is specified via option
`-a`, only those sequence/chromosome names/numbers will be considered that are
listed in the genome information file *chr_NC_gi* under the corresponding
assembly, and they will be replaced by the corresponding accession number. All
other sequence names will be skipped in this case. Note that chromosome numbers
need to be prefixed with "chr" in the BED input file (unlike in *chr_NC_gi*
where they appear without prefix.)

*bed2sga* reports the starting point of a BED region (defined by fields 2 and
3) in field 3 (position) of the SGA output. The starting point is defined
relative to BED field 6 (strand). In other words, input fields 2 (start)
or 3 (end) are transferred to SGA output, depending on whether the BED regions
is attributed to the + or - strand, respectively. Note further that BED genomic
coordinates are zero-based. Therefore, a region's start position in the BED file
is increased by one in the SGA output.

If field 6 (strand) is absent or contains neither "+" or "-", output field 4
(strand) will be set to "0".

#### Notes on options:

-   Option `-f`: For minimal BED files with only three fields, the default
    feature name is "chIP". Otherwise, field 4 (name) from the BED input is
    transferred to field 2 (feature) in the SGA output.

-   Option `-C`: reports the midpoint of BED regions, the strand is preserved
    unless option `-u` is specified. Furthermore, the strand field will be set
    to "0".     

-   Option `--narrowPeak`: Field 10 (peak) of the narrowPeak format is
    added to field 2 (start) in order to compute the value of field 3 (position)
    in the SGA output.

### Example: Reformatting of public ChIP-Seq data from BAM or BED format into SGA

We use the following [GEO SAMPLE](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM2199199) as an example: 

  > **Title**: 5-6hr_GRH_ChIPseq; **Description**: Drosophila melanogaster
  > embryo 5-6 hours after egg laying, embryo, antibody GRH

from [GEO series](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE83305): 
   
  > **Title**: Stable Binding of the Conserved Transcription Factor Grainy Head
  > to Its Target Genes Throughout Drosophila melanogaster Development
  > Publication: Nevil et al. 2017, https://doi.org/10.1534/genetics.116.195685

This GEO series provides both read-alignment files in BAM format and 
peak files in BED format: 

#### Conversion of BAM file into SGA via BED

Download BAM file (**NOTE**: the URLs have been wrapped to fit on the page):
 
```bash
$ wget https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM2199nnn/\
GSM2199199/suppl/GSM2199199_5-6hr_GRH_mapped_dm6.bam
```

or

```bash
$ curl -O -L https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM2199nnn/\
GSM2199199/suppl/GSM2199199_5-6hr_GRH_mapped_dm6.bam
```

Then: 

```bash
$ bamToBed -i GSM2199199_5-6hr_GRH_mapped_dm6.bam 2>/dev/null \
  | ../src/bed2sga -i ../data  -f GRH  -a dm6 2> /dev/null \
  | sort -s -k1,1 -k3,3n -k4,4 | ../src/compactsga \
  > GSM2199199_5-6hr_GRH_mapped_dm6.sga
```

Note the assembly specification `-a dm6`. In this case, the assembly can be
inferred from the filename. Both dm3 and dm6 are supported by the genome
information files provided with this EPD ChIP-Seq Tools release (see [Appendix
@sec:genome-info-files]).  Option `-i` points to the directory where the genome
information files can be found.

You can now use this file with *chipcor*. Example strand correlation:

```bash
$ ../src/chipcor -A 'GRH +' -B 'GRH -' -b -1000 -e 1000 -w 10 -c 1 -n 1 \
    < GSM2199199_5-6hr_GRH_mapped_dm6.sga > GSM2199199_5-6hr_GRH_strand_cor.out
```

### Example: Conversion of peak file from BED format into file into BED:

Download:

```bash
$ wget https://ftp.ncbi.nlm.nih.gov/geo/samples/GSM2199nnn/\
GSM2199199/suppl/GSM2199199_5-6hr_GRH_peaks.bed.gz
# $ curl: curl -O -L https://ftp.ncbi.nlm.nih.gov/geo/samples/\
#GSM2199nnn/GSM2199199/suppl/GSM2199199_5-6hr_GRH_peaks.bed.gz
$ gunzip GSM2199199_5-6hr_GRH_peaks.bed.gz 
```

Conversion:

According to the GEO sample annotation, this file contains peak coordinates
relative to Drosophila melanogaster assembly dm3 ("ChIP reads were aligned to
the dm3 genome assembly using Bowtie ...".  Therefore `-a dm3`.
 
```bash
$ ../src/bed2sga -c -i ../data -f GRH_P -a dm3 \
  < GSM2199199_5-6hr_GRH_peaks.bed | sort -s -k1,1 -k3,3n -k4,4 \
  > GSM2199199_5-6hr_GRH_peaks_dm3.sga
```


It is unfortunate that this GEO entry provides reads and peaks files  mapped to
different genome assemblies. If we want to use the two files together in the
same workflow, we have to "lift over" the genome coordinates from one file to
the assembly used in the other. This can be done with the `liftOver` program,
which can be [downloaded](https://genome-store.ucsc.edu/) from UCSC. You will
also need to download the [chain
file](https://hgdownload-test.gi.ucsc.edu/goldenPath/dm3/liftOver/dm3ToDm6.over.chain.gz)
corresponding to the source and target assemblies of your planned liftOver
operation.

LiftOver of peak file in BED format from dm3 to dm6:

```bash
$ grep -Ev '^(track|browser)' GSM2199199_5-6hr_GRH_peaks.bed \
  > GSM2199199_5-6hr_GRH_peaks_dm3.bed
$ liftOver  GSM2199199_5-6hr_GRH_peaks_dm3.bed dm3ToDm6.over.chain.gz \
    GSM2199199_5-6hr_GRH_peaks_dm6.bed trash
```

The first line serves to eliminate header lines (track and browser) from the
downloaded bed file. This line does not correspond to the BED standard and
causes liftOver to abort with an error message. 

Conversion of new BED file into SGA format

```bash
$ ../src/bed2sga -c -i ../data -f GRH_P  -a dm6 \
  < GSM2199199_5-6hr_GRH_peaks_dm6.bed \
  | sort -s -k1,1 -k3,3n -k4,4 > GSM2199199_5-6hr_GRH_peaks_dm6.sga
```

You can now use this peak file together with the read alignment file to generate
a "read enrichment plot" for GRH peaks:

```bash
$ ../src/chipcenter -i ../data -f GRH -s 150 \
  < GSM2199199_5-6hr_GRH_mapped_dm6.sga 2> /dev/null \
  | sort -s -m -k1,1 -k3,3n -k4,4  - GSM2199199_5-6hr_GRH_peaks_dm6.sga \
  | ../src/chipcor -A "GRH_P 0" -B "GRH" -b -1000 -e 1000 -w 10 -c 1 -n 1 \
  > GSM2199199_5-6hr_GRH_peaks_dm6_read_enr.out
```

*fps2sga.pl*
------------

``` bash
$ fps2sga.pl [options] <FPS file>
```

The *fps2sga.pl* Perl script converts an input file in FPS format into SGA
format. FPS is the native format of the Signal Search Analysis (SSA) program
suite (see Appendix A). Note furthermore that the Eukaryotic Promoter Database
EPD uses an extended FPS standard as native format. This script serves
interoperability purposes between ChIP-Seq, SSA and EPD.

Options are:

| Option                    | Description                                                                           |
|---------------------------|-----------------------------------------------------                                  |
| `-h|--help`               | Show the program usage.                                                               |
| `-f|--feature <feature>`  | Set Feature name `<feature>`                                                          |
| `-a|--assembly <species>` | Assembly `<species>` (e.g. hg18)                                                       |
| `-x`                      | Generate an extended SGA file with field 6 equal to the FPS 'description' field |
| `--ignore0flag`           | Ignore 0 flag at position 29 of the FP line, so keep orientation the same as original |

Notes on options:

-   Option `-f`: the default Feature name is CHIP.
-   Option `-s` is obsolete; please use `-a`.
-   Option `-x` eliminates all white space from the FPS description field
    (columns 6-25). This modification is necessary since SGA is a
    whitespace-delimited text file format.
-   Option `--ignore0flag` currently has no effect. This is a bug.

### Parsing of FSP file

*fps2sga.pl* reads all lines starting with line code 'FP'. From these lines it
extracts:

-   columns 06-25 $\rightarrow$ description
-   columns 29-29 $\rightarrow$ orientation flag
-   columns 34-51 $\rightarrow$ sequence identifier

Note that this behavior is inconsistent with the FPS format specifications,
according to which columns 31-51 contain a composite sequence reference
consisting of a database (DB) code followed by '`:`', followed by a sequence
identifier. There is no imposed length on the DB code. As long as 2-letter DB
codes are used, EPD ChIP-Seq Tools programs will correctly extract sequence
identifiers from columns 34-51. All FPS files in the EPD database and in the MGA
repository use exclusively 2-letter DB codes.

**TODO** Proposed amendments for next release (open for vote):

-   Accept FPS file from standard input
-   Amendment of FPS format parsing (see above)
-   Indicate default feature name in the command line help instructions (and
    table above accordingly).
-   **TODO** Fix --ignore0flag bug (a new version with this bug fixed can be found at:
    <https://epd.expasy.org/epd2/chipseq/code/fps2sga_PB021023.pl> - the link is
    broken


*gff2sga.pl*
------------

```bash
$ gff2sga.pl [OPTIONS] <GFF file>
```

The *gff2sga.pl* Perl script converts an input file in GFF3 format (See Appendix
B) into SGA format.

Options are:

| **Option**              | **Description**                                                                                                                 |
| -------                  | ---------------                                                                                                                 |
| `-h|--help`              | Show this stuff                                                                                                                 |
| `-i|--db <path>`         | Use `<path>` to locate genome information file *chro_idx.nstorage*                                                               |
| `-f|--feature <feature>` | Set Feature name `<feature>`                                                                                                    |
| `-a|--species <species>` | Assembly `<species>` (e.g. `hg18`)                                                                                               |
| `-s`                     | Deprecated, please use -a.                                                                                                      |
| `-C`                     | Generate a Centered SGA file                                                                                                    |
| `-u`                     | Generate an Unoriented SGA file                                                                                                 |
| `-x`                     | Generate an extended SGA file with field 6 equal to the GFF 'type' field, and the feature field equal to the GFF 'source' |


### Input processing

The GFF3 header line `##gff-version 3` is not required.

Without an assembly specified by option `-a`, *gff2sga.pl* will transfer field 1
from GFF input to field 1 of SGA output as is. With option -a, it will convert
sequence names to unique sequence identifiers according to the translation
tables provided in the genome information file *chro_idx.nstorage* (see Appendix
C). Lines containing sequence names not registered in the genome information
file will be transferred as is. Both NCBI and UCSC-style sequence names (1 or
chr1 for human chromosome 1) are accepted.

*gff2sga.pl* reports the starting point of a feature in the GFF file in field 3
(position) of the SGA file. The starting point is defined relative
to the strand (field 7 in GFF file). In other words, GFF fields 3 (start) or 4
(end) are transferred to SGA output, depending on whether the feature is
attributed to the + or - strand, respectively.

Field 5 (counts) of the output SGA file will always contain 1. If field
6 (score) of the GFF file contains a number or character string different from
"1", it will be transferred as is to an optional field 6 in the output.

By default, *gff2sga.pl* transfers the contents of GFF field 3 (type) to field 2
(feature) of the SGA output. With option `-x`, field 3 will be transferred to an
optional field 6 in the output and field 2 of the SGA output will be filled with
GFF field 2 (source) unless another feature name is defined via the `-f` option.
Note that the optional field 6 in the output may harbor both fields 3 and 6 of
the GFF input line. In this case, the feature type (field 3) precedes the score
(field 6), separated by a space character.

Notes on options:

-   Option `-C`: reports the midpoint of GFF regions, the strand is preserved
    unless option `-u` is specified.

**TODO** Bug and proposed amendments for next release (open for vote):

-   With option `-x`, *gff2sga.pl* appends the character string " 0" (**TODO**,
    is that really " " (with a space?))to the GFF
    type in output field 6, if the GFF score is 1. This is inconsistent with
    the default behavior where no field 6 is provided if if the GFF score is
    1

-   The script doesn't accept GFF from standard input.


*partit2sga.pl*
---------------

```bash
$ partit2sga.pl [OPTIONS] <ChIP-Part outfile>
```

The program converts the output of the ChIP-Seq Tools partitioning program
(**TODO** shouldn't we mention said program's name? (*chippart*, I presume?) or
simply provide an example involving its use?)
to centered SGA format.

Options are:

| **Options**      | **Description**                                                   |
| -----            | ------------------                                                |
| `-h|--help`      | Show this stuff                                                   |
| `-i|--db <path>` | Use `<path>` to locate genome information file *chro_idx.nstorage* |

*partit2bed.pl*
---------------

```bash
$ partit2bed.pl [OPTIONS] <ChIP-Part out file>
```

The program converts the output of the ChIP-Seq Tools partitioning program (**TODO**
see previous TODO) to BED format.

Options are:

| **Options**             | **Description**                                                      |
| -------                 | ---------------                                                      |
| `-h|--help`             | Show this stuff                                                      |
| `-i|--db <path>`        | Use `<path>` to locate genome information file *chro_idx.nstorage*    |
| `-T|--track <track>`    | BED track name [default: Test-BED]                                       |
| `-t`                    | Deprecated, please use `-T`.                                         |
| `-d|--desc <desc>`      | Description field of the BED header line [default: ChIP-Seq]             |
| `--color <RRR,GGG,BBB>` | Define the main color for the annotation track [default: 200,100,0]      |
| `--chr <str>`           | Chromosome number (BED declaration lines) [default: 0 - all chromosomes] |
| `-b <int>`              | Chromosome start [default: -1 - entire chromosome region]                     |
| `-e <int>`              | Chromosome end [default: -1 - entire chromosome region]                       |

*partit2gff.pl*
---------------

```bash
$ partit2gff.pl <SGA file>
```

| **Options**      | **Description**                                                   |
| -----            | ------------------                                                |
| `-h|--help`      | show this help                                                    |
| `-i|--db <dir>`  | which dir to look for chro_idx.nstorage in                        |


*wigVS2sga.pl*
--------------

```bash
$ wigVS2sga.pl [OPTIONS] <WIG file|stdin>
```

Convert variableStep WIG files to SGA.

Options are:

| **Options**              | **Description**                                                                  |
| -------                  | ---------------                                                                  |
| `-h|--help`              | Show this stuff                                                                  |
| `--db <path>`            | Use `<path>` to locate genome information file *chro_idx.nstorage*                |
| `-f|--feature <feature>` | Set Feature name `<feature>` [def=WIG]                                           |
| `-a|--species <species>` | Assembly `<species>` [def=hg19]                                                  |
| `-s`                     | Deprecated, please use `-a`.                                                     |
| `-n <int>`               | Normalization factor for data value: it needs to be converted to INTEGER [def=1] |

*sga2bed*
---------

```bash
$ sga2bed [OPTIONS] [<] <SGA file|stdin>
```

Convert SGA format into BED format.

Options are:

| **Options**          | **Description**                                                                                                                                                                                                                                                                                                                                                                 |
| ------               | ---------------                                                                                                                                                                                                                                                                                                                                                                 |
| `-d|--debug`         | Produce debug information                                                                                                                                                                                                                                                                                                                                                       |
| `-h|--help`          | Show this help text                                                                                                                                                                                                                                                                                                                                                             |
| `-i|--db <dir>`      | Directory where the *chr_NC_gi* file is found (default: CWD)                                                                                                                                                                                                                                                                                                                       |
| `-5|--bkext <len>`  | Set 5p-end extension `<len>`(bp)                                                                                                                                                                                                                                                                                                                                                |
| `-3|--fwext <len>`  | Set 3p-end extension `<len>`(bp)                                                                                                                                                                                                                                                                                                                                                |
| `-l|--readlen <len>` | Set Read length `<len>`(bp) Unoriented SGA files are extended by +/-`<len>`/2                                                                                                                                                                                                                                                                                                   |
| `-n|--score <score>` | Normalisation factor for BED score field (5th) [score=1]                                                                                                                                                                                                                                                                                                                        |
| `-X|--extend`        | f1:F1[,f2:F2] Set SGA optional field(s) f1(,f2,...) to BED field(s) F1,(F2,..) Fields are specified by column numbers Accepted BED field values are 4, 5, and 7 Except BED field 5 (score field), BED fields 4 and 7 can be used to set multiple extension values from SGA Fields 5 and 7 convert into numerical values whereas BED field 4 takes character strings as they are |
| `-H|--nohdr`         | BED format without annotation track header lines                                                                                                                                                                                                                                                                                                                                |
| `-r`                 | Deprecated, please use `-H`.                                                                                                                                                                                                                                                                                                                                                    |
| `-x|--expand`        | Expand SGA lines into multiple BED lines                                                                                                                                                                                                                                                                                                                                        |
| `--name <name>`      | Set name for track name field [default: name=SGA-feature]                                                                                                                                                                                                                                                                                                                           |
| `--desc <desc>`      | Set track description field [default: desc="ChIP-Seq Custom data"]                                                                                                                                                                                                                                                                                                                  |
| `--color <col>`      | Define the track color in comma-separated RGB values [default: 100,100,100]                                                                                                                                                                                                                                                                                                         |

Input processing:

Field 1 (name) of the SGA input files must contain a sequence name or an
accession code registered in the genome information file *chr_NC_gi,* otherwise
the line will be skipped. If input field 1 contains a sequence name (chromosome
number) it must be prefixed with "chr" relative to the denomination in
*chr_NC_gi*. For instance, the left arm of *Drosophila* chromosome 2,
denominated "2L" in *chr_NC_gi*, must appear as "chr2L" in the SGA input file.

The file *chr_NC_gi* must exist, either in the current directory or at the
location specified via the `-i` option, otherwise he program will issue usage
instructions and immediately stop execution.

Output:

For oriented SGA lines (field 4 (strand) containing either "+" or "-"), a BED
line with six fields will be generated, featuring the strand sign in field 6.
For unoriented SGA lines (field 4 containing "0"), a BED line with only five
fields will be generated. By default, the bed region consists of a single base
pair.  Since BED-coordinates are zero-based, the value of field 2 (chromStart)
is the position number from the SGA input (field 3) decreased by 1 whereas field
3 (chromEnd) contains the original position number.

Notes on Options:

-   Option `-5|--bk_ext <len>`: `<len>` must be a negative integer. With strand
    field 4 (strand) set to "+" or "0" on the SGA input line, `<len>` is in a
    mathematical sense "added" to the value of the BED chromStart field (field 2)
    resulting in a decrease of value. With strand "-" on input, `<len>` is
    mathematically subtracted from BED field 3 (chromEnd) resulting in an in
    increase in value.
-   Option `-3|--fw_ext <len>`: `<len>` must be a positive integer. With 
    field 4 (strand) set to "+" or "0" on the SGA input line, `<len>` is added to the
    BED chromEnd field (field 3). With strand "-" on input, `<len>` is subtracted
    from BED field 2 (chromStarts).
-   Option `-l|--readlen <len>`: `<len>` must be a positive integer. With
    field 4 (strand) set to "+" on the SGA input line, `<len>`-1 is added to BED
    field 3 (chromEnd). With strand "-" on input, `<len>`-1 is subtracted from
    BED field 2 (chromStart). With strand "0" on input, `<len>`/2 (rounded
    off to the nearest integer) is subtracted from BED field 2 (chromStart)
    and `<len>`/2-1 is added to BED field 3 (chromEnd).
-   Option `-n|--score <score>`: Integer and decimals are accepted on input.
    Decimals are rounded off to integers prior to be used as multiplier of field
    5 (input count). The product is then transferred to BED field 5 (score).
-   Option `-X|--extend f1:F1[,f2:F2]`: Fields are specified by column numbers.
    `f1,f2,...` are SGA input fields, `F1,F2,...` are BED output fields. Only
    optional SGA fields (field 6 or higher) are accepted. Allowed BED extension
    fields are 4, 5, and 7. BED fields 4 and 7 can accept multiple SGA fields.
    BED field 4 uses space as internal field delimiter whereas field 7 uses
    tab as delimiter. The usage of space as an internal field delimiter is
    problematic as BED is officially a white-space delimited format.
-   Option `-x|--expand`: By default, input SGA field 5 (counts) is
    transferred to BED field 5 (score). With this option, the bed output
    line is repeated *n* times, *n* being the number in the input counts field.
    The BED score field is set to zero.
-   Option `--name <name>`: Single- and doubled-quoted strings are accepted as
    argument.
-   Option `--desc <desc>`: Single- and doubled-quoted strings are accepted as
    argument.

*sga2wig*
---------

```bash
$ sga2wig [OPTIONS] [<] <SGA file|stdin>
```

Convert SGA format into Wiggle Track format (WIG).
WIG format is line-oriented, and is composed of declaration lines and data lines.

- variableStep is for data with irregular intervals between new data points.
  It begins with a declaration line and is followed by two columns containing chromosome positions and data values:
  ```
    variableStep  chrom=chrN  [span=windowSize]
    chromStartA  dataValueA
    chromStartB  dataValueB
    ... etc ...  ... etc ...
  ```

- fixedStep is for data with regular intervals between new data values.
  It begins with a declaration line and is followed by a single column of data
  values (the `\` indicates a wrapped line):
  ```
    fixedStep  chrom=chrN  start=position  step=stepInterval  \
               [span=windowSize = stepInterval]
    dataValue1
    dataValue2
    ... etc ...
  ```

Options are:

| **Options**            | **Description**                                                                                                                                                                                                                |
| --------               | ---------------                                                                                                                                                                                                                |
| `-d|--debug`           | Produce debug information                                                                                                                                                                                                      |
| `-h|--help`            | Show this help text                                                                                                                                                                                                            |
| `-i|--db <path>`       | Use `<path>` to locate the *chr_NC_gi* and *chr_size* files [default is: `$CWD`]                                                                                                                                                     |
| `--format <0|1>`       | Set Wiggle Track data format: variableStep[def=0]/fixedStep[1]                                                                                                                                                                 |
| `--chr <int>`          | Chromosome number [default: 0 (all chromosomes)]                                                                                                                                                                                   |
| `--chrnb <int>`        | Deprecated, please use `--chr`                                                                                                                                                                                                   |
| `-b|--start <int>`     | Chromosome start [default: -1 (entire chromosome)]                                                                                                                                                                                 |
| `-e|--end <int>`       | Chromosome end [default: -1 (entire chromosome)]                                                                                                                                                                                   |
| `-C|--coff <int>`      | Count cut-off for the SGA input file [def=99999]                                                                                                                                                                               |
| `-s|--span <int>`      | Wiggle Track Span(/stepInterval) parameter [def=150] For fixedStep data format, it defines the step parameter                                                                                                                  |
| `-n|--norm <int>`      | Normalization factor for total tag counts within step intervals [def=0] This option is only valid for fixedStep data format                                                                                                    |
| `--peakf`              | Indicate that the The SGA input file represents a peak file [i.e. coordinates are peak centers] In such case, the span range begins upstream of [span=]150 bp chromosome position specified, and ends [span=]150 bp downstream |
| `--name <name>`        | Set name for track name field [default: name=SGA-feature]                                                                                                                                                                          |
| `--desc <desc>`        | Set track description field [default: desc="ChIP-Seq Custom data"]                                                                                                                                                                 |
| `--color <col>`        | Define the track color in comma-separated RGB values [default: 100,100,100]                                                                                                                                                        |
| `--autoscale <on|off>` | Data viewing parameter: set auto-scale to UCSC data view [def=OFF]                                                                                                                                                            |
| `--always0 <on|off>`   | Data viewing parameter: always include zero [def=OFF]                                                                                                                                                                         |
| `--wfunction <func>`   | Data viewing parameter: windowing function [def=mean+whiskers|maximum|mean|minimum]                                                                                                                                           |
| `--smoothing <grade>`  | Data viewing parameter: smoothing window [def=OFF[0], `<grade>`=0,2..16]                                                                                                                                                      |
| `--visibility <mode>`  | Display mode: [def=full\|dense\|hide]                                                                                                                                                                                            |

Notes on options:

-   Option `--chr`: This option also determines the contents of the browser
    line, see below.
-   Options `-b` and `-e`: These options have no effect if no chromosome is
    specified via the `--chr` option.
-   Option `--peakf`: This option forces variable space output. Each input line
    gives rise to one output line. The 5th (counts) field of the SGA file is
    transferred to the 2nd (dataValue) field of the output.
-   Options `--name`and `-desc`: The arguments may be double- or single-quoted
    strings including spaces.

Output:

The first line of the output is a "browser" line. "browser" lines are optional.
They can be used to define initial browser settings. With *sga2wig*, the content
of the browser line depends on the options and parameters given on the command
line to the program.

| **command line**                    | **browser line**                    |
|------------------------------------|------------------------------------|
| `sga2wig` (no options)              | `browser full refGene`               |
| `sga2wig` (without `--chr` option)  | `browser full refGene`               |
| `sga2wig --chr 1`                   | `browser position chr1:1-100000`     |
| `sga2wig --chr 1 -b 10000`          | `browser position chr1:10000-110000` |
| `sga2wig --chr 1 -e 2000000`        | `browser position chr1:1-2000000`    |
| `sga2wig --chr 1 -b 10000 -e 20000` | `browser position chr1:10000-20000`  |

The above Table also illustrates the behavior of the `-b` and `-e` options.
Starting position 1 is assumed if option `-b` is missing. A default region
length of 100'000 is assumed if option `-e` is missing.

The original motivation for choosing the default value `browser full refGene` is
no longer known today. It obviously makes no sense for genome assemblies, with
do not have a track named "refGene" at the genome browser host.

*sga2gff.pl*
------------

``` bash
$ sga2gff.pl [options] <SGA file>
```

The *fps2gff.pl* Perl script converts an input file in SGA format into GFF3
format.

Options are:

| Option                 | Description                                                                            |
|------------------------|---------------------------------------------------------                               |
| `-h|--help`            | Show the program usage                                                                 |
| `--db <path>`.         | Use `<path>` to locate genome information file *chro_idx.nstorage*.                     |
| `-l|--taglen <taglen>` | Set read length to `<taglen>`                                                             |
| `-x`                   | Expand SGA lines into multiple GFF lines                                               |
| `-e|--ext <ext_flag>`. | Use SGA field 6 to set GFF feature field and store SGA feature into GFF source field |

### Input processing:

_sga2gff.pl_ first reads the first line of the input SGA file. It accepts two
types of lines: (i) lines that start with "chr", (ii) lines that start with
a sequence identifier that

1.  conforms to the following Perl-compatible regular expression `N[CT]_\S+\.\d+`
2.  is recorded in the in the genome information file *chro_idx.nstorage*.

If the first line is of another type, the program aborts.

Otherwise, if the first line starts with "chr", all sequence identifiers will
be transferred as is to field 1 of the GFF file, regardless of whether
or not they start with "chr". No further checks will be done on subsequent lines.
If the first line starts with a valid sequence identifiers, the program will try
to convert all identifiers into chromosome names. If the identifier is not found
in genome information file file *chro_idx.nstorage*, field 1 of the GFF
line will be empty.

-   Strand "0" on input (field 4) will be converted to "." in GFF3 (field 7),
    and no length extension (**TODO** seems that something is missing here: "no
    length extension... will be performed", perhaps?) if a tag length is
    specified via option `-l|--taglen`.
-   If the `-e|--ext` option is specified, the program will check whether a
    field 6 is present on the first line of the input file. If this is not
    the case, the program will abort with an error message. Otherwise, if a
    subsequent input line doesn't contain a field 6, field 3 in the
    GFF3 output will be empty.
-   Without the `-e|--ext` option, the feature (2nd) field of the SGA input will
    transferred to the feature type (3rd) field in the GFF3 output. In this
    case, the source (2nd) field of the GFF3 output will always be filled with
    "ChIPSeq".
-   Field 5 (counts) of the SGA input will be transferred to field 6 (score)
    of the GFF3 file unless the `-x` option is specified, in which
    case field 6 with be filled with a dot (".").

Notes on output:

-   Field 8 (phase) of the GFF3 output file is always filled with a dot
    (".").
-   Field 9 (attributes) of the (**TODO** complete)

*sga2fps.pl*
------------

```         
$ sga2fps.pl [options] <SGA file|stdin>
```

The *sga2fps.pl* Perl script converts an input file in SGA format into FPS
format. FPS is the native format of the Signal Search Analysis (SSA) program
suite (see Appendix A). Note furthermore that the Eukaryotic Promoter Database
EPD uses an extended FPS standard as native format. This script serves
interoperability purposes between EPD ChIP-Seq Tools, SSA and EPD.

Options are:

| Option                   | Description                                                                                   |
|--------------------------|---------------------------------------------------                                            |
| `-h|--help`              | Show the program usage.                                                                       |
| `--db <path>`            | Use `<path>` to locate genome information file *chro_idx.nstorage*.                            |
| `-f <feature>`           | Set `<feature>` field                                                                         |
| `-s|--species <species>` | Assembly `<species>` (e.g. hg18)                                                               |
| `--set0flag`             | Set 0 flag at position 29 of the FP line, if field 4 in the input FPS file contains "0" |

Notes on options:

-   Option `-f`: The feature will be placed in the description space of all output
    FP lines (col. 6-25). If this option is missing and a field 6 is present
    in the SGA input file, field 6 is transferred to the descriptions
    space, otherwise field 2 (feature) from the SGA file is used. In
    this case, the character description is extended with character string
    `_<number>1`, where `<number>` is field 5 (counts) from the SGA
    input.
-   Option `-s` enables the translation of sequence name to ID. This option will be
    ignored if the first line of the SGA input file is a valid sequence ID (see
    below). Invalid sequence names will be translated to empty character
    strings, except on the first line where they cause instant program abortion.
-   Option `--ignore0flag`: SSA programs use column 51 as strand field. If field
    4 (orientation) in the input is "0", it will be changed to "+" for
    the strand field. If this flag is set, the "0" will be transferred to
    column 29. Note that columns 26-30 have no impact on the behavior of SSA
    programs but may be transferred to output (Program FINDM). The "0"
    (unoriented) flag can then be reconstituted upon backtranslation of an FPS
    file into SGA format with _fps2sga.pl_.

### Input processing:

-   _sga2fps.pl_ accepts sequence Identifiers that conform to the
    Perl-compatible regular expression

    ```         
       [NA][CT]_\S+\.\d+
    ```

    and exist as valid IDs in the genome information file *chro_idx.nstorage*.
    If a species assembly is specified via the `-s|--species` option then
    sequence names starting with "chr" are also accepted, on condition that they
    exist in *chro_idx.nstorage* under the correspding assembly. These sequence
    names will then be replaced by the corresponding sequence ID. Note, however,
    that the `-s|--species` option will not exclude valid IDs from other
    species.

-   Any sequence identifier appearing on the first line and not meeting the
    acceptance criteria will cause instant abortion of the program. Not control
    **TODO**: this sentence seems incomplete.

-   Input SGA files are assumed to contain sequence/chromosome names or IDs but
    not a mixture of both. Likewise, all lines should be either oriented (field
    4 "+" or "-+" **TODO** are the two options really "+" and "**-+**"?) or
    unoriented. The identifier and orientation status are inferred from the
    first line, and all subsequent lines are processed accordingly, with no
    further validity checks. There is no guarantee that SGA files not meeting
    the homogeneity criteria will be processed in a sensible way.

Notes on output:

-   The following columns in the output file are invariant:

    ```         
    col  1-15: "FP.  "
    col 27-28: ":+U"
    col 30-33: " EU:"
    col 52:52: "1"
    col 64:64: ";"
    col 71:71;
    ```

-   Columns 65-60 contain a serial number for each FP line.

-   If the `<feature>` defined by the `-d` option is too long, it will not be
    truncated and FPS output fields to the right of the description will be
    pushed to the right, in a violation of the FPS format standard. As a
    consequence, the output file will not be parseable by SSA programs.

**TODO** Bugs and proposed amendments for next release (open for vote):

-   Standard input is currently not accepted.
-   `-s|--species` option bug: Lines with invalid sequence names could be skipped
    or processed without name-to-ID translation.
-   `-f <feature>` option bug: The `<feature>` string be should truncated such
    that the total length of the description will not exceed 19 characters.
-   Indicate default feature name in the command line help instructions (and
    table above accordingly).
-   Add an option `--dbcode <dbcode>`. SSA programs recognize sequence
    references of the form `<dbcode>`:`<entry_name>`. The interpretation of the
    dbcode depends on a local configuration file (SSA2DBC.def. To provide
    flexibility to the users, dbcodes different from "EU" should be allowed.
    Note that there is no explicit size limit for the dbcode. However, the
    overall length of the sequence identifiers must not exceed 21 character.

**TODO** ck following link:
Some of these bugs have been fixed in a new version that can be found at:
[`https://epd.expasy.org/epd2/chipseq/code/sga2fps_PB191123.pl`](https://epd.expasy.org/epd2/chipseq/code/sga2fps_PB191123.pl).

This version

-   accepts SGA files from standard input,
-   does not require sequence IDs to match regular expression
-   truncates the description provided by the `-f` option to 19 characters.


*bed2bed_display*
-----------------

```bash
$ bed2bed_display [OPTIONS] [-m <min BED score>] \
                  [-M <max BED score>] [<] <BED file|stdin>
```

Convert BED file to BED format suitable for displaying ChIP-Seq peaks.
Two formats are supported: 1) BED Track with Score field; 2) BedGraph.
For BED track format, only the graphical parameter 'visibility' can be set.

Parameters are:

| **Parameters** | **Description**   |
| -----          | ---------------   |
| `-m`           | minimum BED score |
| `-M`           | maximum BED score |

Options are:

| **Options**            | **Description**                                                                      |
| -------                | ---------------                                                                      |
| `-d|--debug`           | Produce debug information                                                            |
| `-h|--help`            | Show this help text                                                                  |
| `-f|--feature <ft>`    | Set feature name `<ft>`                                                              |
| `--format <1|2>`       | Set output format (1:BED[Def] 2:BedGraph)                                            |
| `-o <1|2>`             | Deprecated (please use `--format`)                                                     |
| `--oformat <1|2>`      | Deprecated (please use `--format`)                                                     |
| `--name <name>`        | Set name for track name field [default: name=SGA-feature]                                |
| `--desc <desc>`        | Set track description field [default: desc="ChIP-Seq Custom data"]                       |
| `--color <col>`        | Define the track color in comma-separated RGB values [default: 100,100,100]              |
| `--autoscale <on|off>` | Data viewing parameter: set auto-scale to UCSC data view [def=OFF]                  |
| `--always0 <on|off>`   | Data viewing parameter: always include zero [def=OFF]                               |
| `--wfunction <func>`   | Data viewing parameter: windowing function [def=mean+whiskers\|maximum\|mean\|minimum] |
| `--smoothing <grade>`  | Data viewing parameter: smoothing window [def=OFF[0], `<grade>`=0,2..16]            |
| `--visibility <mode>`  | Display mode: [def=dense\|full\|hide] or [def=1\|2\|3]                                   |


\appendix
\titleformat{\section}{\normalfont\sc\Large\bfseries}{Appendix \thesection\quad{}---}{1em}{}

EPD ChIP-Seq tools summary {#sec:tools}
======================

### Correlation Tools

| **Program name**                | **Description**                                                                                  | **Input Format**  | **Output Format**  | **Language**  |
| --------------------------      | ----------------------------------------------------                                             | ----------------- | ------------------ | ------------- |
| *chipcor*                       | Positional correlation of two genomic features and generation of an aggregation plot (AP)        | SGA               | Text (AP)          | C             |
| *chipextract*                   | Extract genome annotation features around reference genomic anchor points                        | SGA               | Text (Table)       | C             |
| *chipscore*                     | Select ChIP-Seq reads from feature A that are enriched (or depleted) in feature B reads          | SGA               | SGA                | C             |

### Peak-finding Tools

| **Program name**                | **Description**                                                                                  | **Input Format**  | **Output Format**  | **Language**  |
| --------------------            | ----------------------------------------------------                                             | ----------------- | ------------------ | ------------- |
| *chippeak*                      | Narrow peak caller using a fixed width peak size                                                 | SGA               | SGA                | C             |
| *chippart*                      | Broad peak caller used for large regions of enrichment (i e histone marks)                       | SGA               | Special SGA        | C             |

### Preprocessing Tools

| **Program name**                | **Description**                                                                                  | **Input Format**  | **Output Format**  | **Language**  |
| ------------------------------- | ----------------------------------------------------                                             | ----------------- | ------------------ | ------------- |
| *chipcenter*                    | Shift ChIP-Seq reads to estimated center positions of DNA fragments                              | SGA               | SGA                | C             |
| *filter_counts*                 | Filter out or select all ChIP-Seq reads that occur within a set of DNA regions (e g repeat mask) | SGA               | SGA                | C             |

### Reformatting Tools

| **Program name**                | **Description**                                                                                  | **Input Format**  | **Output Format**  | **Language**  |
| ------------------------------- | ----------------------------------------------------                                             | ----------------- | ------------------ | ------------- |
| *compactsga*                    | Merge equal ChIP-Seq read positions into a single line adjusting the count field                 | SGA               | SGA                | C             |
| *featreplace*                   | Change the name of the `<feature>` field                                                         | SGA               | SGA                | C             |

### Conversion Tools

| **Program name**                          | **Description**                                                                                  | **Input Format**  | **Output Format**  | **Language**  |
| ----------------------------------------- | ----------------------------------------------------                                             | ----------------- | ------------------ | ------------- |
| *bed2sga*                                 | Convert BED format to SGA format                                                                 | BED               | SGA                | C             |
| *fps2sga.pl*                              | Convert FPS format to SGA format                                                                 | FPS               | SGA                | Perl          |
| *gff2sga.pl*                              | Convert GFF format to SGA format                                                                 | GFF               | SGA                | Perl          |
| *partit2sga.pl*                           | Convert the output from the *chippart* tool into centered SGA format                               | Special SGA       | BED                | Perl          |
| *partit2bed.pl*                           | Convert the output from the *chippart* tool into BED format                                        | Special SGA       | BED                | Perl          |
| *partit2gff.pl*                           | Convert the output from the *chippart* tool into GFF format                                        | Special SGA       | GFF                | Perl          |
| *wigVS2sga.pl*                            | Convert Wig Variable Step format into SGA format                                                 | Wig               | SGA                | Perl          |
| *sga2bed*                                 | Convert SGA format to BED format                                                                 | SGA               | BED                | C             |
| *sga2wig*                                 | Convert SGA format to Wig format                                                                 | SGA               | Wig                | C             |
| *sga2gff.pl*                              | Convert SGA format to GFF format                                                                 | SGA               | GFF                | Perl          |
| *sga2fps.pl*                              | Convert SGA format to FPS format                                                                 | SGA               | FPS                | Perl          |
| *bed2bed_display*                         | Convert BED format to BED track                                                                  | BED               | BED                | C             |

File Formats {#sec:formats}
============

## SGA format

SGA (Simplified Genome Annotation) is the native format of the EPD ChIP-Seq
Tools. It is a whitespace-delimited, plain text format with five obligatory
fields and an unlimited number of optional fields following the obligatory
fields.

"Whitespace-delimited" means that any character string exclusively consisting
of blanks and tabs is interpreted as a field separator. Note that this implies
that fields must not include blanks or tabs. Putting text between quotes offers
no work-around regarding this rule.

The meaning of the five obligatory fields have been described in [Section
@sec:basics]. They
have to conform to the following Perl-compatible regular expression:

```         
^(\S+)\s+(\S+)\s+(\d+)\s+([+-0])\s+(\d+)
```

While all EPD ChIP-Seq Tools programs accept whitespace-delimited input, they invariantly
use tab as field delimiter on output.

### Notes on obligatory fields

1.  Unique sequence identifiers: There are no special restrictions but some
    programs require that the identifiers are present in the genome information
    files (*chr_NC_gi*, *chr_size*, *chro_idx.nstorage*, see [Appendix
    @sec:genome-info-files]). If you are working with public sequences from ENA,
    RefSeq, GenBank etc. we recommend using versioned sequence identifiers (e.g.
    NC_000001.11 for human chromosome 1 from assembly hg38). This prevents
    mixing sequences from different assemblies of the same organism (for
    instance chr1 from hg19 and hg38).

2.  Feature: no special restrictions. Some programs require input lines of two
    different types identified by the Feature field. If you are using SIB
    ChIP-Seq Tools programs in UNIX pipes, we recommend avoiding characters that
    have a meaning in shell syntax such as parentheses, vertical bars,
    semicolons, etc.

3.  Position: always refers to a single base. Note that a position *i* in an SGA
    file corresponds to a "region" in a BED file starting at position *i*-1 and
    ending at position *i*.

4.  Strand: The value "0" for unoriented features is probably unique to the SGA
    format. It is replaced by "+" upon conversion into other formats. With
    programs converting other formats to SGA, the strand field can typically be
    forced to be zero with a command line options.

5.  Counts field: It always contains a non-zero integer. For NGS data, it
    represents the number of sequence reads mapped to the corresponding
    position. For other genomic features it can have other meanings, for
    instance conservation strength in case of sequence conservation tracks.

### Notes about optional fields

They can be used for any purpose. Some EPD ChIP-Seq Tools programs accept
optional fields on input while others return results in newly-added optional
fields. Read the documentation of individual programs carefully when using
optional fields. SGA files with any number of additional fields can be imported
into software that process data tables, such as R.

### Types of SGA files (sub-standards):

-   "unsorted" SGA. By default, an SGA files is sorted according to the rules
    given in Chapter 2. If this is not the case, the file should explicitly be
    designated as "unsorted SGA". Note that some EPD ChIP-Seq Tools programs
    produce unsorted SGA output under certain conditions. This generally happens
    when chromosome names are replaced by RefSeq IDs identifier. In this case,
    the output should be sorted before passed as input to another EPD ChIP-Seq
    Tools program.

-   "regions SGA". Such a file defines a set of genomic regions. Lines with
    strand field "+" or minus "-" define the beginning or the end of region,
    respectively. Regions SGA files are often generated from BED files. If
    regions overlap, it may not be possible to know which starting point
    corresponds to which end point. Regions SGA files are used with the program
    *filter_counts* to filter out or retain other SGA lines which fall in a
    particular type of genomic regions, *e.g.* an annotated repeat regions.
    Regions of the same type are supposed to have the same feature field.

    Example: **TODO** example is missing...

-   "partit SGA": Such a file defines a set of non-overlapping genomic regions.
    Partit SGA files is returned by *chippart*, which partitions the genome into
    two types of regions: "signal-enriched" and "signal-depleted".  Lines with
    strand field "+" mark the beginning of a signal-enriched region (and
    implicitly the end of a signal depleted region). Lines with strand field "-"
    mark the end of a signal-enriched region (and implicitly the beginning of a
    signal-depleted region).

-   Oriented SGA files: All lines have strand field set to "+" or "-".

-   Unoriented SGA files: All lines have strand field set to "0".

-   Homogeneous SGA files: All lines are either oriented or unoriented, and have
    identical feature fields.

-   Big and small SGA files: These are types based on actual size and contents,
    not sub-standards. There are no additional constraints imposed on such
    files. Big SGA files typically contain tens of million lines. They typically
    harbor NGS read mapping data, or high-resolution quantitative genomic
    features such as sequence conservation scores. Small SGA files are typically
    below 100'000 lines. They typically correspond to derived genomics feature
    obtained by processing or manual annotation of primary data. Examples are
    ChIP-Seq peaks and transcription start sites. In small SGA files, the
    biological objects represented are sometimes named via an optional
    field 6.

Named sub-standards help to describe the input/output format requirements and
compliance of programs, precise knowledge of which is necessary when programs
are used in a pipe. Sub-standard names can also be used to characterize subsets
of lines of an SGA file, identified via the feature field.

## FPS format:

FPS stays for "Functional Position Set" and is the native format of the Signal
Search Analysis (SSA) package and the Eukaryotic Promoter Database EPD. Like
SGA, it is used to identify and annotate single base positions in genomic
sequences. The SSA package offers motif oriented-programs for analysis of
genomic sequences in the vicinity of so-called "functional positions", which
could be ChIP-Seq peak summits for instance.

FPS is a fixed field-length (card-reader type) format. An FPS file may contain
lines of several types, identified by a two-letter code at the beginning of the
line. EPD ChIP-Seq Tools programs recognize only one type, FP (Functional Positions).

The general format of an FP line as required by SSA program is:

 column  content
-------  --------
 1- 2    "FP"
 3- 5    blank
 6-30    description
31-55    Sequence reference (<database code>:<accession>)
52-52    Sequence type ("1" for linear, "2" for circular)
53-53    strand ("+" or "-")
54-63    position number
64-64    ";"

EPD ChIP-Seq Tools programs conform to a slightly deviant format standard:

 column   content
-------   -------- 
 1- 2     "FP"
 3- 5     blank
 6-25     description
26-28     free (":+U" on output)
29-29     orientation flag ("0" or blank on output)
31-55     Sequence reference (<database code>:<accession>)
          31-33 <database code> ("EU: on output)
          34-51 Sequence Id (<accession>)
52-52     free ("1" on output)
53-53     strand ("+" or "-")
54-63     position number
64-64     ";"
65-70     Functional position Id (ignored in input)

The main differences is that the sequence identifier occurs at a fixed position,
which imposes that the database code is of length 2. However, SSA imposes no
length restrictions on the database code. Furthermore, the database code has no
meaning to EPD ChIP-Seq Tools programs. In an SSA environment, it is part of an indexed
sequence environment that allows rapid, direct access to internal positions in
chromosomes.

Column 29 is used by EPD ChIP-Seq Tools programs to indicate that the feature is
unoriented. If present in the FPS input file, It will be automatically
transferred to the strand field by the *fps2sga.pl* program unless option
`--ignore0flag` is used.

## BED format

An authoritative definition of the BED format can be found at the UCSC genome
browser website:

> [https://genome.ucsc.edu/FAQ/FAQformat.html](https://genome.ucsc.edu/FAQ/FAQformat.html)

The standard BED format has 3 obligatory and 9 optional fields. Like SGA, it can
be whitespace-delimited (including  tab-delimited). If used as custom tracks for display
in a genome browser, BED files may include additional track header lines
beginning with keywords such as "browser" or "track". However, these lines are
not part of the basic BED standard and may cause programs, which expect BED
input, to crash.

By default, EPD ChIP-Seq Tools programs interpret the first six fields according to the BED
official specifications.

1.  **chrom** - name of the chromosome (sequence)
2.  **chromStart** - starting position of the feature (0-based)
3.  **chromEnd** - ending position of the feature
4.  **name** - name of the BED line (feature)
5.  **score** - a score between 0 and 1000
6.  **strand** - either "." (no strand) or "+" or "-".

0-based means that the first base in a sequence numbered 0. More generally,
chromStart refers to the base position just before the feature starts.

Additional fields may be transferred to/from BED files by format conversion
programs. This process is controlled by command line options of the respective
programs.

The number in the SGA counts field can be transferred to the BED score field, or
represented by multiplication of the corresponding BED line *n* times in the
output.

Note that the number of fields in the input BED file may influence the behavior
of the EPD ChIP-Seq Tools programs. BED files with fewer than 6 fields (missing the strand
field) will automatically be converted into unoriented SGA files.

EPD ChIP-Seq Tools programs support two additional BED-like formats, BedGraph

> <https://genome.ucsc.edu/goldenPath/help/bedgraph.html>

and narrowPeak:

> <https://genome.ucsc.edu/FAQ/FAQformat.html>

BedGraph is a UCSC genome browser track format exclusively used for data
visualization. It can be generated from BED with the EPD ChIP-Seq Tools utility
*bed2bed_display*.

narrowPeak is a BED format with 10 fields, where fields 7-10 have the following
meaning:

7.  **signalValue** - Measurement of overall enrichment for the region.
8.  **pValue** - Measurement of statistical significance (-log10).
9.  **qValue** - False discovery rate (-log10).
10. **peak** - Summit (point-source called for this peak)

If EPD ChIP-Seq Tools program *bed2sga* is used with option `--narrowPeak`, it
transfers input fields 7 (signalValue) and 10 (peak) to SGA fields 5 (counts)
and 3 (position).

## GFF3 format

The format specifications can be found at:

> <https://www.ensembl.org/info/website/upload/gff3.html>

This is a tab-delimited format with nine fields:

1.  **seqid** - name of chromosome or sequence
2.  **source** - name of program generating the feature, data source, or project
3.  **type** - type of feature (supposed to be from SOFA sequence ontology)
4.  **start** - Starting position 1-based (unlike BED)
5.  **end** - End position
6.  **score** - A floating point value.
7.  **strand** - + (forward) or - (reverse).
8.  **phase** - 0, 1 or 2 (only meaningful for exons),
9.  **attributes** - A semicolon-separated list of tag-value pairs

Not that the official GFF3 standard requires the following header line:

> `##gff-version 3`

EPD ChIP-Seq Tools programs ignore this rule. They neither require a header line on input
nor generate one output.

The mapping between GFF3 fields 2 and 3 (source and type) and SGA fields 2
(feature) and an optional field six (if present) depends on command line
switches of the EPD ChIP-Seq Tools conversion programs.

As with BED, the number in the SGA counts field can be transferred to the GFF3
score field, or represented by multiplication of the corresponding GFF3 line n
times in the output.

## Other file formats:

Some EPD ChIP-Seq Tools programs require additional information about sequences referenced
in field 1 of an SGA input file. This information is provided by so-called
genome information files. The format of these files is described in [Appendix
@sec:genome-info-files].

Genome Information Files {#sec:genome-info-files}
========================

Some EPD ChIP-Seq Tools programs require, or make use of, specific information
about sequences referred to in field 1 of SGA files.

-   *chr_NC_gi*: text file used by C programs, associates sequences identifiers
    with alternative names, often chromosome names.
-   *chr_size*: text file used by C programs, informs about the length of the
    sequences.
-   *chro_idx.nstorage*: binary file used by Perl scripts, automatically
    generated from *chr_NC_gi* and *chr_size*, contains information from both
    files.

By default, these files are expected to reside in the current directory. The
location can be changed by the command line option `-i` or `--db`.

Changing between sequence identifiers and sequence names is useful when working
with public sequences referred to in different ways. For instance, the sequence
of human chromosome 1 may be referred to as chr1 or by the versioned RefSeq
identifier `NC_000001.11`. The number 11 is the version number, which for this
chromosome corresponds to the human genome sequence assembly hg38. Note in this
context, that `NC_000001.11` identifies a unique DNA sequence whereas chr1 is
ambiguous if no corresponding assembly is specified. As different versions of
human chromosome 1 have different lengths, the genomic coordinates of the same
feature change from one assembly to another. Mixing coordinates from different
assembles leads to wrong or empty output with ChIP-Seq Tools. Therefore, all SGA
files included as examples in this distribution, use versioned NCBI identifiers.

The possibility to switch between different types of identifiers enhances
interoperability with external software and data resources. For instance, to
display peaks called with ChIP-Seq Tool *chippeak* in a genome browser, it may
be necessary to convert unique NCBI identifiers into chromosome names. Likewise,
importing genomic features available in BAM, BED or GFF3 format my require
conversion of chromosomes names into unique sequence identifiers (executable
examples in the main part if this document).

If you are working with your own private sequence, you will have to generate the
genome information files yourself. The file *chr_NC_gi* serves no essential
tasks in this case, but some programs require that it exist, and that sequence
names appearing in SGA files are registered therein.

If you are working with a public genome not covered by the *chr_NC_gi* file
included in this distribution, you will have to generate a new *chr_NC_gi* file
or add the corresponding information to an already existing one (see format
specification below).

We recommend using genomes from RefSeq or GenBank if available. Both
repositories provide a file containing comprehensive assembly information. For
instance, you can find a complete distribution of human genome assembly hg38
(nucleotide sequences, metadata and gene annotation) in the following directory:

> <https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/>

This directory contains a file named:

> _GCF_000001405.40_GRCh38.p14_assembly_report.txt_

You may download this file to your local directory to explore its content. For
instance, type on the UNIX command line:

```         
$ grep NC_000001\.11  GCF_000001405.40_GRCh38.p14_assembly_report.txt \
 | cut -f1,2,7,9,10
```

You will see:

```         
1  assembled-molecule  NC_000001.11  248956422  chr1
```

This line is extracted from a subsection of the file that contains a
tab-delimited table with information on individual sequences. The column headers
of the extracted fields are:

-   (1) Sequence-Name
-   (2) Sequence-Role
-   (7) RefSeq-Accn
-   (9) Sequence-Length
-   (10) UCSC-style-name

Note that there are three different sequence identifiers: Sequence-Name,
RefSeq-Accn, and UCSC-style-name. The Sequence-Name usually consists of a
number, a single letter, or a short alphanumeric code (e.g. 2L for left arm of
_Drosophila_ chromosome 2). The UCSC-style-name usually consist of the NCBI
Sequence-Name prefixed by "chr".

Note that the "Sequence-Role" of human chromosome 1 is "assembled-molecule".
This is the "role" assigned to complete chromosomes. Genome assembly hg38
contains sequences with other roles such as "alt-scaffold". Those are not
registered in the genome information files provided with this release. As a
consequence, ChIP-Seq Tools will ignore them on input. Note, however, that data
from other sources provided in BAM, BED or GFF format may include such
sequences. In such situations, the genome information files serve to restrict
the output to the set of sequences registered in *chr_NC_gi*.

The text-based genome information (*chr_NC_gi* and *chr_size*) files provided
with this release contain Sequence-Names. (e.g. 1 for human chromosome 1). On
output, ChIP-Seq Tools format conversion utilities will convert these names into
UCSC-style names by adding the prefix "chr". On input, they accept both NCBI and
UCSC-style names.

### Format of genome information files

#### 1. *chr_NC_gi*

Remember, this file converts sequence identifiers (ACs) from input to sequence
names on output, or vice-versa. It is a sequentially organized (order of lines
matters), whitespace-delimited text files with two line types:

-   Assembly lines: they start with "`#`" immediately followed by the assembly
    name (e.g. `#hg38`). Only field 1 is used by the programs. The
    following lines are assumed to belong to this assembly, unless a new
    assembly line is encountered.

-   Sequence lines: The first two fields are read by the program. These fields
    contain the sequence name and the corresponding unique identifier. Both are
    limited to 64 characters. If the genome comes from RefSeq (as is the case
    for the *chr_NC_gi* file provided with this distribution), field 2
      corresponds to the RefSeq-Accn (see example above).

Additional characteristics:

-   Both line types may contain additional fields (e.g. notes by the author),
    which are ignored by all programs.

-   The sequence names are invariantly alphanumeric character strings
    (`A-Za-z0-9`). Sequence identifiers contain "." (dot) and sometimes "\_"
    (underscore) in addition. Both are limited to 64 characters.

#### 2. *chr_size*

This file is used by some programs to prevent output of sequence coordinates
that fall beyond the length of the sequence (which could provoke an error exit
of a third party programs that is used downstream in an analysis pipelines).

The file has a similar structure as *chr_NC_gi*, with the following notable
differences:

-   The *chr_size* file included in this distribution is *de facto*
    tab-delimited.

-   Field 2 of the sequence lines contains the length of the sequence.

#### 3. *chro_idx.nstorage*

This is a binary file generated from *chr_NC_gi* and *chr_size* by the script
*make_chro_idx.nstorage.pl*. Its use is as follows:

``` perl
make_chro_idx.nstorage.pl <chr_NC_gi> <chr_size> <chro_idx.nstorage>
```

The file is used by Perl scripts. It contains the combined information from
*chr_NC_gi* and *chr_size* in a data structure suitable for Perl. In principle,
it should be information-wise equivalent to the two text files used by the C
programs, with one notable difference however: It contains UCSC style sequence
names starting with "chr" instead of NCBI style names. This makes no difference
from the user's perspective as C programs automatically add the prefix "chr" to
sequence names. On query input, both UCSC and NCBI style names are accepted. For
example, with option -s hg38 all programs map "1" and "chr1" to NC_000001.11.

You may explore the contents of *chro_idx.nstorage* with a few lines of Perl
code, as exemplified below:

Reading the file, loading the data structure:

``` perl
use Storable qw (retrieve nstore);
my $chr2SV = retrieve('chro_idx.nstorage');
```

Given accession, find assembly:

``` perl
$assembly = $chr2SV->{'assembly'}->{'NC_000001.11'};
print "$assembly\n";
```

Given accession, find sequence name:

``` perl $name = $chr2SV->{'NC_000001.11'}; print "$name\n"; ```

Given assembly + sequence name, find accession:

``` perl
$accession = $chr2SV->{'hg38'}->{'1'};
print "$accession\n";
$accession = $chr2SV->{'hg38'}->{'chr1'};
print "$accession\n";
```

Given accession, find sequence length:

``` perl
$length = $chr2SV->{"length"}->{'NC_000001.11'};
print "$length\n";
```

Author Conventions
==================

The following conventions are meant for consistency, but that readers can be
unaware of without hampering their understanding.

* "White space" (2 words) as a noun, and used in the singular, but "whitespace"
  (1 word) as an attribute or when part of a compound adjective (i.e. linked to
  another word with a hyphen); e.g. _separated by white space_ but _whitespace
  character_ or _whitespace-delimited format_
* UNIX in all caps
* `stdin`, `stdout`, `stderr` in lowercase, monospace.
* "field _n_", instead of "the _n_-th field" or "field #n" (KISS)

----

Original version: Giovanna Ambrosini

Conversion to Markdown and additional material: Thomas Junier

Precisions about conversion utilities: Philipp Bucher

