---
title:  _Chipseq_ 
colorlinks: true
toc: true
classoption:
 - onecolumn
 - a4paper
---

Memory-checking Chipseq Code
============================

During a discussion with PB, SM and GA on 2022-06-10, it was decided that I
should try to memory-check the Chipseq code in case the erratic behaviours (see
above) were caused by memory leaks or some such.

Accordingly, I downloaded the code from `/local/chipseq/src` on `srv4`,
installed Valgrind, and proceeded to build the binaries. Well, I rapidly hit a
snag:

```bash
# in /home/tjunier/projects/epd/src/chipseq/src
$ make
gcc -O3 -std=gnu99 -fPIC -lm -o chippeak chippeak.c hashtable.o
/usr/bin/ld: hashtable.o: warning: relocation against `free@@GLIBC_2.2.5' in read-only section `.text'
/usr/bin/ld: hashtable.o: relocation R_X86_64_PC32 against symbol `free@@GLIBC_2.2.5' can not be used when making a PIE object; recompile with -fPIE
/usr/bin/ld: final link failed: bad value
collect2: error: ld returned 1 exit status
make: *** [Makefile:32: chippeak] Error 1
```

After googling a bit, I found the suggestion to add `-static` to the linker
flags. This solves the problem (but there remain compiler warnings).

### Tests

I now wanted to make sure my freshly-compiled binaries behave in the same way as
the original ones on `grbuchersrv4`. To this end I used the tests found in
`./tests/README`, e.g.

```bash
# grbuchersrv4
$ time ./chipcenter -f "DNaseI-HS" -s 70 < \
/export/data/ftp/mga/hg19/encode/Duke-DNaseI-HS/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga \
> /tmp/centered.sga
$ time ./chipcenter -f "DNaseI-HS" -s 70 < /export/data/ftp/mga/hg19/encode/Duke-DNaseI-HS/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga  > /tmp/centered.sga
WARNING: TAG: NC_000017.10      DNaseI-HS       63      -       1        goes beyond chromosome boundaries (pos after TAG Shift=-7, chrom size = 81195210)
WARNING: TAG: NC_000017.10      DNaseI-HS       65      -       1        goes beyond chromosome boundaries (pos after TAG Shift=-5, chrom size = 81195210)
Total Tag Counts : 109097676 , Total Sequence Len : 3035405456
./chipcenter -f "DNaseI-HS" -s 70 <  > /tmp/centered.sga  51.40s user 6.48s system 82% cpu 1:10.40 total
```

Where I substituted the current location of the SGA file for the one in the
`README` (namely,
`/data/mga/hg19/encode/Duke-DNaseI-HS/wgEncodeOpenChromDnaseUrothelAlnRep2V2.sga`
- which no longer exists), and directed the output to some place where I can
  write.

```bash
# my laptop
$ time ./chipcenter -f "DNaseI-HS" -s 70 < wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga > chipcenter.out
WARNING: TAG: NC_000017.10	DNaseI-HS	63	-	1	 goes beyond chromosome boundaries (pos after TAG Shift=-7, chrom size = 81195210)
WARNING: TAG: NC_000017.10	DNaseI-HS	65	-	1	 goes beyond chromosome boundaries (pos after TAG Shift=-5, chrom size = 81195210)
Total Tag Counts : 109097676 , Total Sequence Len : 3035405456
./chipcenter -f "DNaseI-HS" -s 70 <  > chipcenter.out  46.03s user 4.59s system 99% cpu 50.985 total
```

The standard outputs look identical, what about the output files?

```bash
# srv4
$ md5sum /tmp/centered.sga
8d8b9ad8ec0a2a2d0b54e9fb3407d893  /tmp/centered.sga
# my laptop
$ md5sum chipcenter.out
8d8b9ad8ec0a2a2d0b54e9fb3407d893  chipcenter.out
```

Sounds all right. I can now proceed to run `chipcenter` under Valgrind. For this,
I just need to recompile it with `-g -O0`, because without `-g` I won't see line
numbers, and optimization can obscure what's really happening. I will also make
a rather smaller version of the input file, because runing under Valgrind slows
things down quite a bit:

```bash
$ head -1000000 < wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga > wg_head.sga
```
 This just _one hundredth_ of the original! ok, so

```bash
$ gcc -g -O0 -std=gnu99 -fPIC -lm -static -o chipcenter chipcenter.c hashtable.o
```

And:

```bash
$ valgrind --leak-check=yes \
./chipcenter -f "DNaseI-HS" -s 70 < wg_head.sga > chipcenter.out 2> valgrind.out
```

`valgrind.out` looks like this:

```bash
$ head -25 < valgrind.out
==235581== Memcheck, a memory error detector
==235581== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==235581== Using Valgrind-3.19.0 and LibVEX; rerun with -h for copyright info
==235581== Command: ./chipcenter -f DNaseI-HS -s 70
==235581==
==235581== Syscall param set_robust_list(head) points to uninitialised byte(s)
==235581==    at 0x455D3A: __tls_init_tp (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x4082E2: __libc_setup_tls (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x407542: (below main) (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==  Address 0x40006b0 is in the brk data segment 0x4000000-0x4000dbf
==235581==
==235581== Conditional jump or move depends on uninitialised value(s)
==235581==    at 0x465D6E: getrandom (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x41E6F5: ptmalloc_init.part.0 (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x421E54: malloc (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x47E7FB: _dl_get_origin (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x456B06: _dl_non_dynamic_init (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x4584C8: __libc_init_first (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x4075E8: (below main) (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==
==235581== Conditional jump or move depends on uninitialised value(s)
==235581==    at 0x421C6F: malloc (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x47E7FB: _dl_get_origin (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x456B06: _dl_non_dynamic_init (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
==235581==    by 0x4584C8: __libc_init_first (in /home/tjunier/projects/epd/src/chipseq/src/chipcenter)
```

But it also ontains **this interesting section**:

```bash
==235581== HEAP SUMMARY:
==235581==     in use at exit: 0 bytes in 0 blocks
==235581==   total heap usage: 0 allocs, 0 frees, 0 bytes allocated
==235581==
==235581== All heap blocks were freed -- no leaks are possible
```

This is quite encouraging, and also very impressive. Good work, GA! 

Now I did not call the program with the exact same arguments as were mentioned
in the discussion, so I'll do that now; I'll also have a look at those
uninitialised values.

Well, two things:

1. I still couldn't find any leaks when calling as `./chipcenter -i . -z -s 75 -c 1 STAT1_stim_head.sg`
2. The messages about uninitialised values are myserious. In the cases where a
   line number is given, I scanned the code for any potentially uninitialised
   variable, gave it an initial value (typically `NULL`), recompiled, and
   re-launched under Valgrind. No effect was ever observed. Moreover, it seemed
   that the variables always had an initial value anyway: there was no value in
   the variable's _declaration_, but I couldn't find a case in which a variable
   was accessed before it was set.

I then tried to fix the code that produced warnings (there were no `break`
statements in a `switch` statement). This is not necessarily wrong, since
there are cases in which a fall-through is wanted (C is unusual in allowing
this; all other languages I know that have an equivalent of C's `switch`
(e.g. Ruby's `case...when` exit the statement at the end of the inner
clauses - except for Bash, which also has a way of doing this). Moreover, the
code is within the hash table function, which I confess I do not fully
understand, and it might be the case that this is precisely one of the rare
cases where this fall-through behaviour is actually warranted. IOW, this wants
immediate testing.

Well, the results are identical to those produced by the code without the added
`break` statements, and the warnings have disappeared, so at least I have
probably not introduced bugs. Bug did it fix the uninitialized values problem?
No.

The next effort was to try to get the line numbers for the memory errors. but I
was unable to obtain them. Finally, I tried updating the system, having read
circumstancial evidence that this unexpected behaviour on the part of Valgrind
was down to some bug in the Linux kernel, or the glibc, or whatnot. No luck. I'm
putting this on ice for now, because (as decided in the 2022-08-05 meeting) we
don't want to spend more time on this.

Automated Tests
===============

File  `../src/chipseq/src/tests/README` contains commands that exemplify the use
of several of the Chipseq suite commands. These should make good starting points
for tests, although the following must be observed:

* There are no "expected" results (correct output files), so we have to generate
  them using the code as it now stands, assuming it works as designed.
* Many tests check run times, which are machine-dependent.

I converted the file into a [Bats](https://github.com/bats-core/bats-core)
script, staying as close as possible to the original commands (see code). The
main issue I faced was that the original input files are so large that running
the whole test suite on my machine takes on the order of an hour. This makes
development impossible, so I implemented two running modes: "full" and "devel".
Each mode uses its own set of input and expected-results files.

During the 2022-08-05 meeting it was decided that I should use "short" versions
of the input files, made by only considering chromosomes 21 and 22. So I added
another mode ("short") that did just that. Another idea was to compress the
input data, which are rather large.

Now there is no direct indication of chromosome in the `.sga` files, only
`NC...`. But there happens do be precisely 23 of these NC numbers, and the count
of lines from `NC_000001` to `NC_000023` (`cut -d'.' -f1 <
wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga | uniq -c`) shows a steady (if
imperfect) decrease. I therefore deem it safe to assume that these numbers do
indeed denote chromosomes. So, to extract 21 and 22, I did:

```bash
$ grep '^NC_00002[12]' \
 < wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga \
 > wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga
$ grep '^NC_00002[12]' \
 < wgEncodeOpenChromDnaseUrothelAlnRep2V2.sga \
 > wgEncodeOpenChromDnaseUrothelAlnRep2V2-short.sga
```

in `./src/chipseq/data/`.

### Latest Docker build

Instead of the container in the Docker Hub, I tried to build the chipseq
container, as above, but using the latest `Dockerfile`. So it's just

```bash
$ cd ~/Downloads/chipseq/docker/
$ echo $CHIPSEQ_VERSION
$ docker build -t chipseq:$CHIPSEQ_VERSION --no-cache=true \
  --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
  --build-arg CHIPSEQ_VERSION=$CHIPSEQ_VERSION  \
  -f Dockerfile . 2>&1 >ChIP-Seq.Dockerfile.log
```

I note that this time the `docker build` command is done by a regular user, not
via `sudo` as was the case before. 

Then I tried running the command again, but with that new image:

```bash
$ docker run --rm -it -v /home/tjunier/projects/epd/src/chipseq/data:/home/chipseq/data sibswiss/chipseq:latest chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 < data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga
zsh: no such file or directory: data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga
```

Ok, that's not a surprise: the `<` is not escaped, so Zsh tried to perform
redirecion, and fails because no matching file is found. Maybe we can just
escape that `<`?

```bash
$ docker run --rm -it  --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data chipseq:1.5.5 chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 \< data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga

Unable to open '<': No such file or directory(2)
```

Hm, this is less obvious: the `<` is most likely passed to Docker, but it
interprets it as a filename argument. Maybe the arguments to `docker run` do not
go through a shell at all (a bit like a call to `exec(1)`)? 

OK, then what about passing everything within quotes (not that I see how that
would magically make Docker treat the `>` as a redirection, but heck, many
things are mysterious)?

```bash
$ docker run --rm -it  --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data chipseq:1.5.5 'chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 < data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga'
docker: Error response from daemon: failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: "chipcor -A \"DNaseI-HS +\" -B \"DNaseI-HS -\" -b-1000 -e1000 -w 10 < data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga": stat chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 < data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga: no such file or directory: unknown.
```

Ah, our good ole friend `failed to create shim task` (I still don't have the
faintest idea what on Earth a "shim" task may be). I take it it wasn't such a
good idea. Now, the `chipcor` help says that it can also take its output as an
argument instead of on STDIN. Shall we give it a try?

```bash
$ docker run --rm -it  --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data chipseq:1.5.5 chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga 
#Norm : Raw Counts , Reference Feature : DNaseI-HS +, Target feature : DNaseI-HS - , Total Sequence Len : 99362947 , Total Reference Counts : 1743593 , Total Target Counts : 1856242
   -990.0     723591
   -980.0     725757
   -970.0     727177
   -960.0     727799
   -950.0     728556
   -940.0     731163
   -930.0     731616
   -920.0     732637
   -910.0     733532
...
```

Success! And it also works with the `sibswiss/chipseq:latest` container (not
shown).

Still, there are two points I wish were less obscure:

* Why does passing all arguments in a single quoted string cause that weird
  behaviour?
* What if we _need_ to pass arguments to STDIN, e.g. if our binary is a pure
  filter like `tr`? Well, you can pass it to a shell, like this:

```bash
$ docker run --rm -it  --mount
type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data
sibswiss/chipseq:latest \
  sh -c 'chipcor -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000
-e1000 -w 10 < data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga'
```

Of course that supposes that `/bin/sh` or equivalent is available in the
container, but I'd say it's a rather safe bet.

### Tests of Other Programs

Now I'll add tests for the programs other than the `chip*` suite. These are
found in `src/chipseq/src` and `src/chipseq/src/tools/`; here is a list of
executable files in these dirs (except those with names starting with `chip`):

```bash
# in src/chipseq/src
$ sl -l *~chip*(*)
-rwxr-xr-x 1 tjunier tjunier 879216 Jul  7 11:10 compactsga
-rwxr-xr-x 1 tjunier tjunier  17784 Sep 21  2018 counts_filter
-rwxr-xr-x 1 tjunier tjunier 884400 Jul  7 11:10 countsga
-rwxr-xr-x 1 tjunier tjunier 883928 Jul  7 11:10 featreplace
-rwxr-xr-x 1 tjunier tjunier 884024 Jul  7 11:10 filter_counts
# in src/chipseq/src/tools/
$ ls -l *(*)
-rwxr-xr-x 1 tjunier tjunier   183 Jun 20  2017 array_size.pl
-rwxr-xr-x 1 tjunier tjunier 28064 May  6 15:37 bed2bed_display
-rwxr-xr-x 1 tjunier tjunier 28064 May  6 15:34 bed2sga
-rwxr-xr-x 1 tjunier tjunier 27520 May  6 15:37 compactsga
-rwxr-xr-x 1 tjunier tjunier 18832 May  6 15:36 countsga
-rwxr-xr-x 1 tjunier tjunier 18352 May  6 15:37 featreplace
-rwxr-xr-x 1 tjunier tjunier 18640 May  6 15:37 filter_counts
-rwxr-xr-x 1 tjunier tjunier  8344 May 17  2017 getopt
-rwxr-xr-x 1 tjunier tjunier 14129 May 18  2017 hash_table
-rwxr-xr-x 1 tjunier tjunier   302 Sep 20  2018 rmsk2bed.pl
-rwxr-xr-x 1 tjunier tjunier   276 Sep 20  2018 rmsk2sga.pl
-rwxr-xr-x 1 tjunier tjunier 28088 May  6 15:34 sga2bed
-rwxr-xr-x 1 tjunier tjunier 32432 May  6 15:34 sga2wig
-rwxrwxrwx 1 tjunier tjunier  3765 Jul  7  2017 sga2wigSmooth_FS.pl
-rwxr-xr-x 1 tjunier tjunier  8100 May 19  2017 str_split
-rwxr-xr-x 1 tjunier tjunier   526 Sep 18  2018 test_split.pl
-rwxr-xr-x 1 tjunier tjunier  6844 May 15  2017 test_sscanf
```

Some of them (such as `getopt`, `hash_table`, `test_sscanf` and possibly
`str_split`) do not look like bioinformatics programs, but rather like test
programs for more general functions. Indeed, the presence of `getopt.h`, as well
as `#include <getopt.h>` in several source files, supports this hypothesis. So
we won't write test cases for these.

File `src/chipseq/src/tools/README_test.txt` contains many examples of usage, as
well as sample outputs. This can be very useful for writing tests. The problem
is that at least some of the input files mentioned therein are nowhere to be
found (case in point: `test_narrowPeak.bed`). This is possibly just down to a
name change; at any rate I'll see if I can find a BED file that contains the
features found in the sample outputs.

`chr_NC_gi` and `chro_idx.nstorage`
-----------------------------------

### By passing `-h`

```bash
# in ./src/chipseq/src
$ for bin in **/*~*rmsk2*(*); do
  echo $bin;
  ./$bin -h 2>&1 | grep -E '(chr_NC|chro_)'
done | sed -nE '/(chro_|chr_NC)/{x;p};h'
tools/bed2bed_display
tools/bed2sga
tools/sga2bed
tools/sga2wig
```

**NOTE**: `tools/rmsk2bed.pl` hangs when passed `-h` (it expects something on
STDIN and does not accept any option). The same goes for `rmsk2sga.pl`. Thus
they are excluded in the glob above.

### By `grep`ping through the source

```bash
$ grep -lE '(chro_|chr_NC)' **/*.{c,h,pl}
tools/bed2sga.c
tools/sga2bed.c
tools/sga2wig.c
tools/sga2wig_tmp.c
tools/sga2wigSmooth_FS.pl
```

### Final list

These are the programs that can use at least one of the two files:

* `tools/bed2bed_display`
* `tools/bed2sga`
* `tools/sga2bed`
* `tools/sga2wig`
* `tools/sga2wigSmooth_FS.pl`
* `tools/sga2wig_tmp`

I redid the above searches but looking only for `chro_`, and found nothing. So
it appears that `chro_idx.nstorage` is not mentioned either in the source or the
help outputs; to be extra sure I also scanned the manpage sources (`zgrep chro_
**/*1.gz`) and still found nothing.

Conclusion: I couldn't find any program that mentions `chro_idx.nstorage` at
all, and six that mention `chr_NC_gi`.

`man` vs `-h`
-------------

### `chipcenter`

* discrepancy in the default to option `-c`: 1 (help) vs. 10 (man)
* option `-r` (new feature name) only in help
* option `-i` (path to the `chr_size` file) only in help

### `chipcor`

* discrepancy in the default to option `-c`: 1 (help) vs. 10 (man)
* (not a discrepancy, added this later) default to option -w is not indicated
  (neither man nor help)

### `chippart`

No discrepancies found.

### `chippeak`

* discrepancy in the default to option `-c`: 1 (help) vs. 10 (man)

### `chipscore`

* discrepancy in the default to option `-c`: 9999 (help) vs. 10 (man)

Manpages Options Check {#sec:man-opts-ck}
======================

Man pages: check that any options mentioned in the synopsis are actually
required (in the sense that if the option is not supplied, then the program
complains or otherwise refuses to run). If not, then the default should be
stated (lest the run proceed with unknown parameter values). This does not
concern options mentioned only in the Options section below.

The options are as follows:

Program       Options in Synopsis  
--------      -------------------- 
`chipcor`     `ABbew`              
`chipscore`   `ABbe`               
`chippeak`    `fwv`                
`chippart`    `fps`                
`chipcenter`  `fs`                 

### `chipcor`

 * no `-A`: help
 * no argument to `-A`: help
 * no `-B`: help
 * no argument to `-B`: _run_
 * no `-b`: run (no obvious default)
 * no argument to `-b`: help
 * no `-e`: run (no obvious default)
 * no argument to `-e`: help
 * no `-w`: help
 * no argument to `-w`: help

### `chipscore`

 * no `-A`: help
 * no argument to `-A`: help
 * no `-B`: help
 * no argument to `-B`: run (with error messages)
 * no `-b`: run (no obvious default)
 * no argument to `-b`: error message
 * no `-e`: run (no obvious default)
 * no argument to `-e`: error message (SGA on STDIN) or hangs (SGA as argument)

### `chippeak`

 * no `-f`: run (default in man)
 * no argument to `-f`: help
 * no `-w`: help
 * no argument to `-w`: help
 * no `-v`: help
 * no argument to `-v`: help

### `chippart`

 * no `-f`: run (default in man)
 * no argument to `-f`: help
 * no `-s`: help
 * no argument to `-s`: help
 * no `-p`: help
 * no argument to `-p`: help

### `chipcenter`

 * no `-f`: run (default in man)
 * no argument to `-f`: help
 * no `-s`: help
 * no argument to `-s`: help

Singularity
===========

**NOTE**, on Manjaro Linux, `singularity` is a game, not a container manager.
Anyway it seems that Singularity (the container software, that is) has recently
been renamed Apptainer (not a particularly happy change, if you ask me^[But then
nobody asks me.]). So I did 

```bash
# pacman -Syyu apptainer
```

And everything went smoothly. The next thing was (after a few tests, which need
not concern us here), to try and access the Chipseq Docker container.

```bash
$ apptainer pull docker://sibswiss/chipseq:latest
INFO:    Converting OCI blobs to SIF format
WARNING: 'nodev' mount option set on /tmp, it could be a source of failure during build process
INFO:    Starting build...
Getting image source signatures
Copying blob 3b65ec22a9e9 done
Copying blob b003e6acd190 done
Copying blob 1924a251462c done
Copying config 43b13b038c done
Writing manifest to image destination
Storing signatures
2022/09/22 11:42:45  info unpack layer: sha256:3b65ec22a9e96affe680712973e88355927506aa3f792ff03330f3a3eb601a98
2022/09/22 11:42:45  warn xattr{etc/gshadow} ignoring ENOTSUP on setxattr "user.rootlesscontainers"
2022/09/22 11:42:45  warn xattr{/tmp/build-temp-718061161/rootfs/etc/gshadow} destination filesystem does not support xattrs, further warnings will be suppressed
2022/09/22 11:42:46  info unpack layer: sha256:b003e6acd19057de67af82d12eafa6664222c636896b827070ad98b867799d34
2022/09/22 11:42:46  warn xattr{etc/gshadow} ignoring ENOTSUP on setxattr "user.rootlesscontainers"
2022/09/22 11:42:46  warn xattr{/tmp/build-temp-718061161/rootfs/etc/gshadow} destination filesystem does not support xattrs, further warnings will be suppressed
2022/09/22 11:42:48  info unpack layer: sha256:1924a251462cd4b552af7b4b3c785f0cdadaa618797f355f287b156899d057ff
2022/09/22 11:42:48  warn xattr{home/chipseq} ignoring ENOTSUP on setxattr "user.rootlesscontainers"
2022/09/22 11:42:48  warn xattr{/tmp/build-temp-718061161/rootfs/home/chipseq} destination filesystem does not support xattrs, further warnings will be suppressed
INFO:    Creating SIF file...
```

That's quite a few warnings (I might have to come back later to look at them
more carefully), but at least it created a SIF file:

```bash
$ file chipseq_latest.sif
chipseq_latest.sif: a /usr/bin/env run-singularity script executable (binary
data)
```

So let's try to run, say, `chipcor` from the container:

```bash
$ apptainer exec --bind /home/tjunier/projects/epd/src/chipseq/data/:/home/chipseq/data  chipseq_latest.sif chipcor -A 'DNaseI-HS +' -B 'DNaseI-HS -' -b -1000 -e 1000 -w 10 /home/chipseq/data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-short.sga | head
#Norm : Raw Counts , Reference Feature : DNaseI-HS +, Target feature : DNaseI-HS - , Total Sequence Len : 99362947 , Total Reference Counts : 1743593 , Total Target Counts : 1856242
   -990.0     723591
   -980.0     725757
   -970.0     727177
   -960.0     727799
   -950.0     728556
   -940.0     731163
   -930.0     731616
   -920.0     732637
   -910.0     733532
```

My, this seems to work!

Further Work on Test Suite
-------------------------

I reworked the whole test harness (at least twice) to accommodate requests by PB
(such as option `-k`); this led to the idea (mine) of storing commands in a TSV
file instead of independent scripts (which also entail lots of boilerplate as
only one line in about 20 differs from one script to another). But there are
also advantages to independent scripts (not least that they are, well,
independent and can be run on their own), not to mention that calling `docker`
on a stored command can only be done by passing the whole command to a shell and
any variables through the environment. Bottom line: we're going to do _both_
stored commands _and_ independent scripts.

**Note** that storing commands for later execution can be tricky. If the command
involves no expansion or redirection, it is straightforward to store it in a
variable (be it by direct assignment or with `read`), and then embed it in a
script:

```bash
$ cmd='ls'
$ cat<<END > script.sh
> #!/bin/bash
>
> $cmd
> END
$ chmod 700 script.sh
$ ./script.sh # runs ls
```

but if the command involves parameter expansion, embedding it in this way will
cause the expansion to occur when the script is _run_, not when it is written:

```bash
$ cmd='echo $USER'
$ cat<<END > script.sh
> #!/bin/bash
>
> $cmd
> END
$ cat script.sh
#!/bin/bash
echo $USER
```

note that `USER` has _not_ been expanded. It will be expanded if you run the
script.

One way around this is to use `eval`, which performs parameter expansion among
other things. The problem is, however, that among these other things is
evaluation: if we just pas the command to `eval`, _it will be evaluated_
(surprise!) when the script is created:

```bash
$ cmd='echo $USER'
$ expanded_cmd=$(eval $cmd)
$ cat<<END > script.sh
#!/bin/bash
$expanded_cmd
END
$ ./script.sh
./script.sh: line 2: tjunier: command not found
$ cat script.sh
#!/bin/bash
tjunier
```

Note that variable `USER` has been expanded, as desired, but the resulting
command `echo tjunier` has been _executed_ by `eval`, so that `expanded_cmd`
contains the _output_ of the command, rather than the command itself, as we'd
wish.

One workaround is to prefix the command with `echo`: this causes the `eval`d
command to be `echo` itself, and `$cmd` to be its arguments: the output of
`eval` becomes the command, minus `echo`, and with all expansions performed:

```bash
$ cmd='echo $USER'
$ expanded_cmd=$(eval echo $cmd)
$ cat<<END > script.sh
#!/bin/bash
$expanded_cmd
END
$ ./script.sh
tjunier
$ cat script.sh
#!/bin/bash
echo tjunier
```

Now the script contains the complete `echo tjunier` command, not just its output
as before. Progress!

But there still is a glitch. Suppose the command contains a redirection:

```bash
$ cmd='echo $USER > my_username'
$ expanded_cmd=$(eval echo $cmd)
$ cat<<END > script.sh
#!/bin/bash
$expanded_cmd
END
$ ./script.sh
$ cat script.sh
#!/bin/bash

```

The redirection occurs during the call to `eval` (and the output goes to
`my_username` at that moment). So the output captured by `$()` is actually
empty, and so is `expanded_cmd`.

I'm not saying there is no workaround, notably perhaps by escaping the `>`, but
the point is that I'd rather keep the commands intact. So the only solution, I'm
afraid, is to embed the command _verbatim_ in the script, and rely on parameter
expansion within the script. This requires any variables not in the environment
to be set by the script.

```
$ cmd='echo "name: $NAME, surname: $SURNAME"'
$ cat<<END > script.sh
#!/bin/bash
NAME=Homer
$cmd
END
$ ./script.sh 
name: Homer, surname:
$ cat script.sh
#!/bin/bash
NAME=Homer
echo "name: $NAME, surname: $SURNAME"
```

Here SURNAME is not set, so the output stops at `surname: `. Passing `set -u`
would catch this.

Altogether this seems like the best solution, although it is not without its
drawbacks as it forces us to explicitly pass any and all required variables, as
well as some unneeded ones (not all scripts need the same information) or else
to tailor the variable list to every script, which would be pretty awkward. This
also forces us to update the variable list every time it changes, which whould
not be required if we could rely on `eval`. Oh well.

So I implemented it in this way and it does seem to work. I have  (2022-10-25 or
thereabouts) ported all the previous Bats tests to the new system (discounting
those that tested runtimes, which don't seem to be paramount), and they work
fine in all four environments, and with quite a few sanity checks built-in.  At
its simplest, it's just

```bash
$ cd src/chipseq/src/tests
$ ./test_main.sh
```

but passing `-h` will tell you more.

### Remaining issues:

* `compactsga`: For this one I had to concoct an input file that actually
  contained repeated positions, so that the compaction would take place and
  could be tested. BTW this has only a filter form, so there is no ARG1 test
  case.

* `countsga`: Here again I don't have an input file. The problem is that all the
  SGA files I do have only contain one feature (STAT1 or DNaseI-HS): I tried to
  come up with a file that contains more (see `test_countsga-full.sga`), but I
  can't seem to get option `-f` right.
 
* Not sure what the binaries called `chippeak_epd` and `chippeak_new` might be,
  nor how they might be related to `chippeak` itself (they are much smaller).
  They are also absent from the `Makefile`. To add to the confusion, there is a
  `chippeak_epd.c` but no `chippeak_new.c`, and the `_epd` source is actually
  _larger_ than the plain `chippeak.c`, despite the fact that the binary is
  smaller. 

#### `tools`

The issues below concern the executables in the `tools` subdir; the commands are
as listed in `README_test.txt`

* `bed2bed_display -f STAT1_p -a 27 -b 525 < chippeak_STAT1.bed` - fails;
  options are apparently not compatible.

* `rmsk2bed.pl` - can't find suitable input file (including on the website: this
  script is not among the available tools).

#### `chipscore` et al.

The `chipscore` test seems to work in all environments but Docker:

```bash
$ ./test_main.sh -m d -p "*chipscore*" -s a       # OK
$ ./test_main.sh -m d -e a -p "*chipscore*" -s a  # OK 
$ ./test_main.sh -m d -e d -p "*chipscore*" -s a  # err msg!
# Run the generated scripts, kept b/c of -s a
$ ./test_chipscore_STDIN-native.sh                # OK
$ ./test_chipscore_STDIN-apptainer.sh             # OK
$ ./test_chipscore_STDIN-docker.sh                # err msg!
```

Ok, what happens if we run the docker command from the shell? Got the literal
command by setting `xtrace` in the ecript, and:

```bash
$ docker run --rm \
  --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data \
  --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/src/tests,target=/home/chipseq/tests \
  --env BIN_DIR=/usr/bin \
  --env UT189=/home/chipseq/data/wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2-devel.sga \
  --env ALREP=/home/chipseq/data/wgEncodeOpenChromDnaseUrothelAlnRep2V2-devel.sga \
  --env H3K4=/home/chipseq/data/TSS_H3K4me3_32733-devel.sgaf \
  --env COMPACTSGA_IN=/home/chipseq/data/test-compactsga-devel.sga \
  --env COUNTSGA_IN=/home/chipseq/data/test_countsga-devel.sga \
  --env CHR_SIZE_DIR=/home/chipseq/data \
  sibswiss/chipseq:latest \
  bash -c '"$BIN_DIR"/chipscore -A DNaseI-HS' '+  -B DNaseI-HS' '- -b-100 -e100 -t 200 < "$ALREP" 2>/dev/null'
Usage: /usr/bin/chipscore [options] -A <feature A> -B <feature B> -b<from> -e<to> [-t <thres>] [<] <SGA file>
      - version 1.5.5
      where options are:
  		 -h     Show this help text
  		 -d     Print debug information and check SGA file
  		 -o     Oriented strand processing
  		 -r     Reverse extraction process
[...]
```

Same thing. PB wondered if it might be due to the redirection (which forces me
to use a shell). Does it change anything if I pass the input as an argument?

Ok, it was due to single quotes in the stored command. Since this is passed to
`bash` (within the container) as `-c '<cmd>'`, the command must not contain
single quotes. To guard against this error, I enabled comment lines in
`test_cmds.tsv` and modded `test_main.sh` so that they are ignored.

The same problem caused `chipextract` to result in error.

#### `compactsga`

For `compactsga`, however, it's a different story. Run from the terminal, I get:

```bash
$ ../compactsga < ../../data/test-compactsga-devel.sga
NC_000001.10	DNaseI-HS	10245	+	3
NC_000001.10	DNaseI-HS	10246	+	1
NC_000001.10	DNaseI-HS	10265	-	4
NC_000001.10	DNaseI-HS	10267	-	1
NC_000001.10	DNaseI-HS	10458	+	3
NC_000001.10	DNaseI-HS	10459	+	3
NC_000001.10	DNaseI-HS	10461	+	1
NC_000001.10	DNaseI-HS	10463	+	1
NC_000001.10	DNaseI-HS	10464	+	1
NC_000001.10	DNaseI-HS	10467	+	2
```

But from the Docker container, I get:

```bash
$ docker run --rm --mount type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data sibswiss/chipseq:latest bash -c 'compactsga < /home/chipseq/data/test-compactsga-devel.sga'
Usage: compactsga [options] [-a <min BED score>] [-b <max BED score>] [<] <BED file|stdin>
      - version 1.5.5
      where options are:
  		 -d|--debug                Produce Debug information
  		 -h|--help                 Show this Help text
  		 -f|--feature <ft>         Set Feature name <ft>
  		 -o|--oformat <1|2>        Set output Format (1:BED[Def] 2:BedGraph)
  		 --name <name>             Set name for track name field [def. name=SGA-feature]
  		 --desc <desc>             Set track description field [def. desc="ChIP-Seq Custom data"]
  		 --color <col>             Define the track color in comma-separated RGB values [def. 100,100,100]
  		 --autoscale  <on|off>     Data viewing paramenter: set auto-scale to UCSC data view [def=OFF]
  		 --always0    <on|off>     Data viewing paramenter: always include zero [def=OFF]
  		 --wfunction  <func>       Data viewing paramenter: windowing function [def=mean+whiskers|maximum|mean|minimum]
  		 --smoothing  <grade>      Data viewing paramenter: smoothing window [def=OFF[0], <grade>=0,2..16]
  		 --visibility <mode>       Display mode: [def=dense|full|hide] or [def=1|2|3]

	Convert BED file to BED format suitable for displaying ChIP-seq peaks.
 	Two formats are supported: 1) BED Track with Score field; 2) BedGraph.
 	For BED track format, only the graphical parameter 'visibility' can be set.
```

Note that I am mounting in Docker the very same data directory I get the input
file from when calling natively. So the input data file is literally the same.

And, contrary to what I thought, it's NOT due to a difference in versions. If I
call the command natively but using v. 1.5.5, I get the same results as natively
with 1.5.4, namely:

```bash
$ ~/Downloads/chipseq-1.5.5/bin/compactsga < ../../data/test-compactsga-devel.sga
NC_000001.10	DNaseI-HS	10245	+	3
NC_000001.10	DNaseI-HS	10246	+	1
NC_000001.10	DNaseI-HS	10265	-	4
NC_000001.10	DNaseI-HS	10267	-	1
NC_000001.10	DNaseI-HS	10458	+	3
NC_000001.10	DNaseI-HS	10459	+	3
NC_000001.10	DNaseI-HS	10461	+	1
NC_000001.10	DNaseI-HS	10463	+	1
NC_000001.10	DNaseI-HS	10464	+	1
NC_000001.10	DNaseI-HS	10467	+	2
```

I also tried to bypass Bash, but with the same results:

```bash
$ docker run --rm --mount \
  type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data \
  sibswiss/chipseq:latest \
  compactsga  /home/chipseq/data/test-compactsga-devel.sga
Usage: compactsga [options] [-a <min BED score>] [-b <max BED score>] [<] <BED file|stdin>
[...]
```

One thing: the help seems sto be wrong so far as it specifies `[<] <BED
file|stdin>`. This implies that the peogram accepts its data either on STDIN or
as a filename, but only STDIN seems to work:

```bash
# Works
$ ~/Downloads/chipseq-1.5.5/bin/compactsga < ../../data/test-compactsga-devel.sga
# Hangs
$ ~/Downloads/chipseq-1.5.5/bin/compactsga ../../data/test-compactsga-devel.sga
```

BTW why does it expect a BED file? Shouldn't this be SGA?

I'm starting to wonder if the binaries in my install of 1.5.5 are the same as
those in the docker container. They're targeting the same architecture after all
(`uname -a` in the container returns (almost) the same as natively).

```bash
$ docker run --rm --mount \
type=bind,source=/home/tjunier/projects/epd/src/chipseq/data,target=/home/chipseq/data \
sibswiss/chipseq:latest \
bash -c 'ls -l /usr/bin/chip*'
-rwxr-xr-x 1 root root 35552 Aug 11 11:43 /usr/bin/chipcenter
-rwxr-xr-x 1 root root 35152 Aug 11 11:43 /usr/bin/chipcor
-rwxr-xr-x 1 root root 60072 Aug 11 11:43 /usr/bin/chipextract
-rwxr-xr-x 1 root root 30824 Aug 11 11:43 /usr/bin/chippart
-rwxr-xr-x 1 root root 44048 Aug 11 11:43 /usr/bin/chippeak
-rwxr-xr-x 1 root root 30896 Aug 11 11:43 /usr/bin/chipscore
```

Contrast this with the host binaries:

```bash
# In ~/Downloads/chipseq-1.5.5/bin
$ ls -l chip*
-rwxr-xr-x 1 tjunier tjunier 39480 May 27 10:24 chipcenter
-rwxr-xr-x 1 tjunier tjunier 39144 May 27 10:24 chipcor
-rwxr-xr-x 1 tjunier tjunier 59976 May 27 10:24 chipextract
-rwxr-xr-x 1 tjunier tjunier 30736 May 27 10:24 chippart
-rwxr-xr-x 1 tjunier tjunier 43840 May 27 10:24 chippeak
-rwxr-xr-x 1 tjunier tjunier 34888 May 27 10:24 chipscore
```

While similar-sized, the binaries are all different. So clearly we're not
dealing with the same binaries, but then I had to modify the Makefile in order
to make compilation succeed at all (see above). And in any case the problem does
not occur.

Also, the Makefile in the `./tools` subdir of the chipseq-1.5.5 distrib from
[SM's repo](https://gitlab.sib.swiss/EPD/chipseq.git) has a bizarre build rule
(l.  45):

```Make
    compactsga : $(BED2BED_DISPLAY_SRC) $(OBJS)
          $(CC) $(LDFLAGS) -o compactsga $^
```

Notice that the target and object file are called `compactsga`, but the _source_
file is `bed2bed_display.c` (l. 25). As it happens, the error message output by
the Docker version of `compactsga` is almost exactly that of `bed2bed_display`,
and very different from `compactsga`'s (tellingly, it refers to a BED file in
input, which seems bizarre). Note that the
`~/Downloads/chipseq-1.5.5/bin/compactsga` binary does give the correct result
(see above, same section). I wonder if running `$ make compactsga` would yield
the same binary:

```bash
# in ~/Downloads/chipseq-1.5.5/tools/
$ make compactsga
gcc -fPIC -lm -o compactsga bed2bed_display.c ../hashtable.o
```

Note that `compactsga.c` is _not_ used, which is fishy. And the two binaries
have different sizes:

```bash
$ ls -l compactsga
-rwxr-xr-x 1 tjunier tjunier 35576 Nov 17 09:22 compactsga
$ ls -l ../bin/compactsga
-rwxr-xr-x 1 tjunier tjunier 21408 May 27 10:24 ../bin/compactsga
```

Now, I wouldn't be suprised if the newly compiled binary behaved strangely:

```bash
$ ./compactsga < ~/projects/epd/src/chipseq/data/test-compactsga-devel.sga
Usage: ./compactsga [options] [-a <min BED score>] [-b <max BED score>] [<] <BED file|stdin>
      - version 1.5.5
      where options are:
  		 -d|--debug                Produce Debug information
  		 -h|--help                 Show this Help text
  		 -f|--feature <ft>         Set Feature name <ft>
  		 -o|--oformat <1|2>        Set output Format (1:BED[Def] 2:BedGraph)
  		 --name <name>             Set name for track name field [def. name=SGA-feature]
  		 --desc <desc>             Set track description field [def. desc="ChIP-Seq Custom data"]
  		 --color <col>             Define the track color in comma-separated RGB values [def. 100,100,100]
  		 --autoscale  <on|off>     Data viewing paramenter: set auto-scale to UCSC data view [def=OFF]
  		 --always0    <on|off>     Data viewing paramenter: always include zero [def=OFF]
  		 --wfunction  <func>       Data viewing paramenter: windowing function [def=mean+whiskers|maximum|mean|minimum]
  		 --smoothing  <grade>      Data viewing paramenter: smoothing window [def=OFF[0], <grade>=0,2..16]
  		 --visibility <mode>       Display mode: [def=dense|full|hide] or [def=1|2|3]

	Convert BED file to BED format suitable for displaying ChIP-seq peaks.
 	Two formats are supported: 1) BED Track with Score field; 2) BedGraph.
 	For BED track format, only the graphical parameter 'visibility' can be set.
```

Indeed. So very likely the makefile is wrong (and I'm not blaming anyone - I
know from experience that writing these things is no walk in the park). I
changed the Makefile so that the relevant rule now reads:

```bash
compactsga : $(COMPACTSGA_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o compactsga $^
```

```bash
./compactsga < ~/projects/epd/src/chipseq/data/test-compactsga-devel.sga
NC_000001.10	DNaseI-HS	10245	+	3
NC_000001.10	DNaseI-HS	10246	+	1
NC_000001.10	DNaseI-HS	10265	-	4
NC_000001.10	DNaseI-HS	10267	-	1
NC_000001.10	DNaseI-HS	10458	+	3
NC_000001.10	DNaseI-HS	10459	+	3
NC_000001.10	DNaseI-HS	10461	+	1
NC_000001.10	DNaseI-HS	10463	+	1
NC_000001.10	DNaseI-HS	10464	+	1
NC_000001.10	DNaseI-HS	10467	+	2
```

Seems to solve the problem. Let's quickly compare the two versions:

```bash
$ diff -s <(./compactsga < ~/projects/epd/src/chipseq/data/test-compactsga-devel.sga) \
         <(~/projects/epd/src/chipseq/src/compactsga < ~/projects/epd/src/chipseq/data/test-compactsga-devel.sga) 
Files /proc/self/fd/11 and /proc/self/fd/12 are identical
```

Good.

Comparing v. 1.5.5 and v. 1.5.4
-------------------------------

The commands were launched in `./src/chipseq/src`, i.e. in the v. 1.5.4 source
tree. In each command, therefore, the first file is version 1.5.4 and the second
is 1.5.5

### Main dir, C application source

#### `chipcenter.c`

```bash
$ diff chipcenter.c ~/Downloads/chipseq-1.5.5/chipcenter.c
109c109
<   char *chrSizeFile = NULL;
---
>   char *chrSizeFile;
702c702
<   FILE *input = NULL;
---
>   FILE *input;
763d762
<           printf("argv[%d]: '%s'\n", optind, argv[optind]);
```

Strangely enough, v. 1.5.5 no longers initializes pointers to NULL (which would
seem a rather good idea to me). As for the removed `printf` statement, it
appears to have been debugging code.

#### `chipcor.c`

```bash
$ diff -s chipcor.c ~/Downloads/chipseq-1.5.5/chipcor.c
Files chipcor.c and /home/tjunier/Downloads/chipseq-1.5.5/chipcor.c are identical
```

#### `chipextract.c`

```bash
$ diff -s chipextract.c ~/Downloads/chipseq-1.5.5/chipextract.c
Files chipextract.c and /home/tjunier/Downloads/chipseq-1.5.5/chipextract.c are identical
```

#### `chippart.c`

```bash
$ !!:gs/extract/part/
$ diff -s chippart.c ~/Downloads/chipseq-1.5.5/chippart.c
Files chippart.c and /home/tjunier/Downloads/chipseq-1.5.5/chippart.c are identical
```

#### `chipscore.c`

```bash
$ diff -s chipscore.c ~/Downloads/chipseq-1.5.5/chipscore.c
Files chipscore.c and /home/tjunier/Downloads/chipseq-1.5.5/chipscore.c are identical
```

### Main dir, C headers

```bash
$ diff hashtable.h ~/Downloads/chipseq-1.5.5/hashtable.h
16c16
< // forward declaration
---
> /* forward declaration */
95c95
< // element operations
---
> /* element operations */
121c121
< // hash table operations
---
> /* hash table operations */
223c223
< size_t hash_table_get_keys(hash_table_t *, void **);
---
> size_t hash_table_get_keys(hash_table_t *, void ***);
232c232
< size_t hash_table_get_elements(hash_table_t *, hash_table_element_t *** );
---
> size_t hash_table_get_elements(hash_table_t *, hash_table_element_t ***);
```

The first three changes are just a change from old-style (K&R) comments to C99;
the fourth adds a level of indirection to its second argument (`void ***`
instead of `void **`); the last just removes a space before the closing `)`.

```bash
$ diff version.h ~/Downloads/chipseq-1.5.5/version.h
1,2c1,2
< #ifndef VERSION          /*  Make sure this file is included only once  */
< #define VERSION "1.5.4"
---
> #ifndef VERSION          // Make sure this file is included only once
> #define VERSION "1.5.5"
```

Rather expectedly, the `VERSION` constant changes from 1.5.4 to 1.5.5.

There is also a `debug.h`, which as it name implies is probably used only for
debugging; anyway it is identical in the two versions.

**NOTE** that the other C source files in v. 1.5.4 (such as `compactsga.c`,
`chippeak_end.c`, etc.) are not found in 1.5.5.

### Tools dir

Instead of doing every `diff` by hand, let's automate a little. Here is a list
of source files (C and Perl) present in both 1.5.4 and 1.5.5:

```bash
$ common=$(comm -12 <(echo tools/*{c,h,pl}(:t) | tr ' ' '\n' | sort) \
  <(echo ~/Downloads/chipseq-1.5.5/tools/*{c,h,pl}(:t) | tr ' ' '\n' | sort))
```

Now let's compute checksums for both versions of each of these source files:

```bash
$ echo $common | sed 's|^|./tools/|' | xargs md5sum > v1.5.4.md5sum
$ echo $common | sed 's|^|/home/tjunier/Downloads/chipseq-1.5.5/tools/|' | xargs md5sum > v1.5.5.md5sum
```

Then we can see which are identical:

```bash
$ paste v1.5.4.md5sum v1.5.5.md5sum | awk '$1 == $3 {print $2}' | xargs -i basename {}
array_size.pl
bed2bed_display.c
compactsga.c
countsga.c
filter_counts.c
hashtable.h
sga2wig.c
test_split.pl
```

And finally we need to call `diff` on the rest, namely:

```bash
$ paste v1.5.4.md5sum v1.5.5.md5sum | awk '$1 != $3 {print $2}' | xargs -i basename {}
bed2sga.c
rmsk2bed.pl
rmsk2sga.pl
sga2bed.c
sga2wigSmooth_FS.pl
version.h
```

#### `bed2sga.c`

```bash
95,96c95,96
< int
< match(char text[], char pattern[])
---
> int
> match(char text[], char pattern[])
```

The only difference is in a tab (`diff -Z`), and in this case it makes no
difference.

#### `rmsk2bed.pl`

```bash
$ diff -csZ tools/rmsk2bed.pl ~/Downloads/chipseq-1.5.5/tools/rmsk2bed.pl
Files tools/rmsk2bed.pl and /home/tjunier/Downloads/chipseq-1.5.5/tools/rmsk2bed.pl are identical
```

#### `rmsk2sga.pl`

```bash
$ diff -csZ tools/rmsk2sga.pl ~/Downloads/chipseq-1.5.5/tools/rmsk2sga.pl
Files tools/rmsk2sga.pl and /home/tjunier/Downloads/chipseq-1.5.5/tools/rmsk2sga.pl are identical
```

#### `sga2bed.c`

```bash
$ diff -csZ tools/sga2bed.c ~/Downloads/chipseq-1.5.5/tools/sga2bed.c
Files tools/sga2bed.c and /home/tjunier/Downloads/chipseq-1.5.5/tools/sga2bed.c are identical
```

#### `sga2wigSmooth_FS.pl`

```bash
$ diff -Z tools/sga2wigSmooth_FS.pl ~/Downloads/chipseq-1.5.5/tools/sga2wigSmooth_FS.pl 
1c1
< #!/usr/local/bin/perl
---
> #!/usr/bin/perl
```

Just a change in the path to the interpreter, maybe it would be better to use
the `#!/usr/bin/env perl` form.

#### `version.h`

```bash
$ diff -Z tools/version.h ~/Downloads/chipseq-1.5.5/tools/version.h
1,2c1,2
< #ifndef VERSION          /*  Make sure this file is included only once  */
< #define VERSION "1.5.4"
---
> #ifndef VERSION          // Make sure this file is included only once
> #define VERSION "1.5.5"
```

Again, this is expected.

**Conclusion**: there is no reason to believe that the program logic differs
from 1.5.4 to 1.5.5. This applies to all programs. The only possible caveat is
the extra level of indirection in `hashtable.c`, but this seems likely to be a
bug fix rather than a change in logic.

## Shorter Files - Apptainer problem

In late November/early December 2022 I worked towards replacing large input
files with smaller ones. PB sent me a script (`new_chipseq_test.sh`) with a
series of test cases as well as instructions to download their inputs. This
works well (only minor changes were needed), _except for Apptainer/Singularity_.
I was able to track the issue to the fact that those new commands often contain
pipes (`|`). 

Having encountered a similar problem with Docker (namely, that commands are not
interpreted by a shell unless explicitly passed to one), I tried the same
approach, namely to have Apptainer launch a shell and pass it the command:

```bash
apptainer run chipseq_latest.sif bash -c '"$BIN_DIR"/bed2bed_display [...]'
```

This does solve the pipe problem, but a new one appears (which Docker
interestingly does not suffer from): something seems to interfere with
double-quoting. Consider this:

```bash
$ docker run --rm sibswiss/chipseq:latest bash -c  'echo "A B C"'
A B C
```

So far so good. But now try this:
```bash
$ apptainer -s run chipseq_latest.sif bash -c 'echo "A B C"'
A
```

Now where are B and C gone? This turns out to pretty much prevent the use of any
double-quoted string in the commands, and since we can't use single quotes at
all (seeing as the whole command is passed _verbatim_ between a pair of `''`),
we apparently _cannot pass whitespace-containing strings_ as arguments in the
Apptainer environment. An example of this is `--desc "Robertson 2007, HeLa S3
cells, ChIP-seq STAT1 peaks."`, passed to `bed2bed_display`.

Ok, so after some googling I found that `run` should actually be `exec`:

```bash
$ apptainer -s exec chipseq_latest.sif bash -c 'echo "A B C"'
A B C
```

Perl Tools
==========

Actual vs. Documented Options {#sec:perl-opts}
-----------------------------

We're going to check that the options accepted by the Perl tools in the
distribution match the ones mentioned in the tools' help (be that `-h`,
`--help`, `man <tool>`, plain `$ ./tool`, or however else a help page may be
elicited).

Cloned `https://gitlab.sib.swiss/EPD/chipseq.git` in `~/Downloads`.

Let's make a list of all Perl tools in the repo:

```bash
$ find . -name '*.pl'
./tools/array_size.pl
./tools/check_bed.pl
./tools/chr_replace_sga.pl
./tools/eland2sga.pl
./tools/fetch_sga.pl
./tools/fps2sga.pl
./tools/gff2sga.pl
./tools/partit2bed.pl
./tools/partit2gff.pl
./tools/partit2sga.pl
./tools/rmsk2bed.pl
./tools/rmsk2sga.pl
./tools/sga2fps.pl
./tools/sga2gff.pl
./tools/sga2wigSmooth_FS.pl
./tools/test_split.pl
```
### `array_size.pl`

This is just a 11-line script that does not provide any help or accept any
option.

### `check_bed.pl`

The program has no `-h` option, but a usage message is printed out if called
without arguments.^[Or at least, that's how it should work, except that before
that the script checks for `/home/local/db/genome/chro_idx.nstorage`, and aborts
if not found.]
This program has two "options", `-s <species>` and `-f <BED file>`, but the code
seems to imply that they are actually _required_:

```perl
# l. 16
&Usage() unless ($opts{'f'} && $opts{'s'});
```

In this case it might be better to pass the values as positional parameters.

### `chr_replace_sga.pl`

Same remark as for `check_bed.pl`, above.

### `eland2sga.pl`

Similar situation as in `check_bed.pl` above, but with three "options":

```bash
my %opts;
# ll. 11-13
getopt('asf:', \%opts);  # -a, -s, & -f take arg. Values in %opts, Hash keys will be the switch names

die("\nusage: eland2sga.pl  -a feature -s species -f eland_file\n\n") \
   unless (($opts{'f'})&&($opts{'a'})&& ($opts{'s'}));
```

On the other hand, this program does output a usage message when called without
arguments.

### `fetch_sga.pl`

This script does not accept any option or provide any help.

### `fps2sga.pl`

This script provides help with `-h` / `--help` or when called without arguments
or wrong ones, as follows:

```bash
$ ./fps2sga.pl --help

        fps2sga.pl [options] <FPS file>

    where options are:
            -h|--help                Show this stuff
	    -f|--feature <feature>   Set Feature name <feature>
	    -s|--species <species>   Assembly <species> (i.e hg18)
	    -x                       Generate an extended SGA file with the
	                             6th field equal to the FPS 'description' field
      --ignore0flag            Ignore 0 flag at position 29 of the FP line,
                               so keep orientation the same as original
```

While it's not clear from the code that `species` is ever used, all other
non-help opions (and only those) are handled in the code.

### `gff2sga.pl`

**NOTE**: this script depends on `Math::Round` (do I gather that Perl's default
arithmetic is a little subpar?). It's just a matter of

```bash
$ perl -MCPAN -e shell
cpan[1]> install Math::Round
```

Here again the script provides a help page when called without arguments:

```bash
$ ./gff2sga.pl

        gff2sga.pl [options] <GFF file>

    where options are:
      -h|--help                Show this stuff
	    --db <path>              Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'
	    -f|--feature <feature>   Set Feature name <feature>
	    -s|--species <species>   Assembly <species> (i.e hg18)
	    -c                       Generate a Centered SGA file
	    -u                       Generate an Unoriented SGA file
	    -x                       Generate an extended SGA file with the
              	    			     6th field equal to the GFF 'feature' field,
				                       and the feature field equal to the GFF 'source'
```

The following line (l. 15) seems to imply that the options are at least
recognized by the program (and that there are no undocumented options):

```perl
my @options = ( "help", "h", "species=s", "s=s", "feature=s", "f=s", "c", "u", "x", "db=s");
```

### `partit2bed.pl`

Help available when called with `-h` or `--help` (though not when called without
arguments, which produces an error):

```bash
$ ./partit2bed.pl -h

    partit2bed.pl [options] <ChIP-Part out file>

    where options are:
        -h|--help                Show this stuff
        --db <path>              Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'
        -t|--track <track>       BED track name [def: Test-BED]
        -d|--desc <desc>         Description field of the BED header line [def: ChIP-Seq]
        -n <int>                 Chromosome number (BED declaration lines) [def: 0 - all chromosomes]
        -b <int>                 Chromosome start [def: -1 - entire chrom region]
        -e <int>                 Chromosome end [def: -1 - entire chrom region]

    The program converts the output of the ChIP-Seq partitioning program to BED format.
```

Again, I can't vouch for the logic, but at least the options mentioned in the
help (and only those) are handled during the parsing of arguments:

```perl
# l. 11
my @options = ("help", "h", "desc=s", "d=s", "track=s", "t=s",
               "n=i", "b=i", "e=i", "db=s");
```

### `partit2gff.pl`

Shows a usage message when called without arguments, but does not mention any
options - nor are any handled in the code.

### `partit2sga.pl`

Behaviour similar to that of `partit2sga.pl`, albeit with fewer options:

```bash
$ ./partit2sga.pl -h

    partit2sga.pl [options] <ChIP-Part out file>

    where options are:
        -h|--help                Show this stuff
        --db <path>              Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'

    The program converts the output of the ChIP-Seq partitioning program to centered SGA format.
```

The options (and only them) are all handled in the code, as expected.

### `rmsk2bed.pl`

This is a simple 13-liner that doesn't mention or accept any option. In fact it
doesn't offer any help page whatsoever.

### `rmsk2sga.pl`

Same case as `rmsk2bed.pl`.

### `sga2fps.pl`

Similar case to `partit2sga.pl`, but with the following options:

```bash
$ ./sga2fps.pl -h

        sga2fps.pl [options] <SGA file|stdin>

    where options are:
            -h|--help                Show this stuff
            --db <path>              Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'
            -f <feature>             Set <feature> field
            -s|--species <species>   Assembly <species> (i.e hg18)
            --set0flag               Set 0 flag at postion 29 of the FP line, forcing unoriented output
                                     (default : blank)
```

Again these options (and no others) are recognized in the code.

### `./sga2gff.pl`

This script provides help when called without arguments as well as when passed
`-h` or `--help`:

```bash
$ ./sga2gff.pl

        sga2gff.pl [options] <SGA file>

    where options are:
        -h|--help                Show this stuff
        --db <path>              Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'
        -l|--taglen <taglen>     Set Read length <taglen>
        -x                       Expand SGA lines into multiple GFF lines
        -e|--ext <ext_flag>      Use SGA 6th field to set GFF feature field
                                 and store SGA feature into GFF source field

```

All the above options (and they alone) are handled in the code.

### `sga2wigSmooth_FS.pl`

Shows a help when called without arguments (but does not appear to support a
help option):

```bash
$ ./sga2wigSmooth_FS.pl
Convert SGA format to WIG with Fixed Step.

Usage:
       sga2wigSmooth_FS.pl <options> <SGA file>

where options are:
       -a   Assembly used in the SGA file
       -f   Fragment size of ChIP-seq experiment. Default value 150
       -s   Span used in the WIG sile, default set to 10
       -c   Count cut-off, defoult set to 1
```

All the options (and just these) are handled in the code, except that `-a`
appears not to be optional at all, in the sense that the program aborts
(showing the help message) is `-a` is not passed.

### `test_split.pl`

A very short (3 lines, excluding comments) script, without any help or options.

List of scripts in Doc vs. List of Scripts in Repo
--------------------------------------------------

We want to make sure that the list of scripts mentioned in the repository
(`doc/ChIP-Seq_Tools-UsersGuide.pdf`, last page) matches the list in the
repository. The list in the PDF is 

* `chipcor`
* `chipextract`
* `chipscore`
* `chippeak`
* `chippart`
* `chipcenter`
* `filter_counts`
* `compactsga`
* `featreplace`
* `bed2sga`
* `fps2sga.pl`
* `gff2sga.pl`
* `partit2sga.pl`
* `partit2bed.pl`
* `partit2gff.pl`
* `wigVS2sga.pl`
* `sga2bed`
* `sga2wig`
* `sga2gff.pl`
* `sga2fps.pl`
* `bed2bed_display`

... which I saved as `doc-progs`. Now presumably only those in `.pl` would be
called "scripts", the rest being written in C (according to the PDF itself).
As for the scripts in the repository:

```bash
# Zsh! 
# echo, not ls - otherwise (:t) won't work.
$ echo ~/Downloads/chipseq/tools/*.pl(:t) | tr ' ' $'\n' | sort > repo-scripts
```

And now it's three tasks for good ol'`comm`:

```bash
# Scripts found in both the documentation and the repository
$ comm -12 <(grep '.pl$' < doc-progs | sort) repo-scripts
fps2sga.pl
gff2sga.pl
partit2bed.pl
partit2gff.pl
partit2sga.pl
sga2fps.pl
sga2gff.pl
```

```bash
# Scripts found only in the repository:
$ comm -13 <(grep '.pl$' < doc-progs | sort) repo-scripts
array_size.pl
check_bed.pl
chr_replace_sga.pl
eland2sga.pl
fetch_sga.pl
rmsk2bed.pl
rmsk2sga.pl
sga2wigSmooth_FS.pl
test_split.pl
```

```bash
# Scripts found only in the manual:
$ comm -23 <(grep '.pl$' < doc-progs | sort) repo-scripts
wigVS2sga.pl
```

Review of `ChIP-Seq_Tools-UsersGuide.pdf`
=========================================

From the TODO:

> Have a look at the Chipseq Tools User.doc and check that everything is clear
> and complete (check e.g. that all tools mentioned are also described).

I tabulated every tool mentioned in the manual, as well as where it is first
mentioned and described (if at all) (@tbl:tools-in-guide).

### Tool mention and description

Tool             1st Mention [p.]  Description [p.]
--------------- ----------------- -----------------
`chipcenter`                    1                 4
`chipcor`                       1                 2
`chipextract`                   1                 3
`chippart`                      1                 8
`chippeak`                      1                 7
`chipscore`                     1                10
`compactsga`                    2                12
`partit2bed.pl`                 9                14
`bed2sga`                      11                12
`featreplace`                  13                13
`filter_counts`                14                14
`fps2sga.pl`                   14                14
`gff2sga.pl`                   14                14
`partit2sga.pl`                14                14
`partit2bed.pl`                14                14
`partit2gff.pl`                14                14
`wigVS2sga.pl`                 14                14
`sga2bed`                      14                14
`sga2wig`                      14                14
`sga2gff.pl`                   14                14
`sga2fps.pl`                   14                14
`bed2bed_display`              14                14

: Tools mentioned in `ChIP-Seq_Tools-UsersGuide_annot.pdf` {#tbl:tools-in-guide}

#### Notes

* A program called `bamToBed` is mentioned on p. 12, but it is not part of
  ChIPSeq suite.
* `featreplace` is described only briefly (but then, its operation is rather
  simple).
* The Appendix (p. 14) contains only brief descriptions. Programs described only
  there are therefore described in significantly less detail than those
  described in the main body of the document.

So it would seem that all tools are indeed described, though not always to the
same extent.

Other than that, I just found a few typos.^[In particular, the word "properly"
was found spelled (improperly...) "proPerly" seven times, which makes me think
that some hazardous operation à la `s/perl/Perl/g` must have been performed
(`s/\<perl\>/Perl/g` does it, well, properly). Anyway, that's NOT sufficient
reason to switch to Python ;-).] See the notes in my annotated version of the
document, `ChIP-Seq_Tools-UsersGuide_annot.pdf`.

Additional notes/remarks
----------------

* The manual has no TOC or page numbering, both could be useful.
* There seems to be an informal convention for the use of roman, fixed-width
  fonts for code snippets (e.g. at
  [Oracle](https://docs.oracle.com/javadb/10.6.2.1/getstart/rgsdocs18277.html),
  [VMWare](https://docs.vmware.com/en/VMware-Smart-Assurance/10.1.3/ncm-dsr-config-guide-1013/GUID-C43C649F-C238-49F8-B92E-A74762060F94.html)
  and
  [O'Reilly](https://www.oreilly.com/library/view/programming-the-perl/1565926994/pr04s02.html)).
  A similar convention is to use italics for file names, although some (e.g.
  [IBM](https://www.ibm.com/docs/en/odm/8.5?topic=documentation-typographic-conventions))
  use fixed-width for that too. We might want to follow these conventions.
* I would suggest including a prompt before the commands, e.g. simply `$ `. This
  shows that the beginning of the command isn't missing (unfortunately, there is
  no easy way of doing this for the command's end).
* I would align the commands to the left, maybe with a small indentation,
  instead of centering them (admittedly, that's personal taste, but it seems
  easier to read).
* I find fully-justified text easier to read than left-aligned.

Markdown Version of the Manual
------------------------------

Saved the latest .doc version of the (corrected) manual as .docx, and converted
it to Markdown with Pandoc (actually had to save it as .docx from LibreOffice -
the .docx from MS Word had problems (can you believe that?)).

I had to split the table in the Appendix into separate tables, because no
Markdown renderer supports cells that span multiple columns (such cells were
used in the original to structure the table by grouping similar programs
together).

Then when I pushed to Gitlab, it appeared that Gitlab's Markdown renderer is not
as flexible as Pandoc's, so I had to change a few things, among others:

* Tables - Gitlab's Markdown only accepts "pipe" tables.

# Backport to older versions of Bash {#sec:old-bash}

The `-v` test of Bash (that tests if a variable is set) does not exist on older
versions of Bash (such as on older Macs).^[New Macs come with Zsh as the default
shell, but the test script is written in Bash.]. I thus backported the test
script so that it uses a different construct, namely:

```bash
[[ ${VAR+x} ]]
```

I mention it here because it is one of the few use cases I know of the `+`
default operator: the expression above will evaluate to "x" if (and only if)
`VAR` is _set_. Note that the simpler `[[ $VAR ]]` will behave
identically, _except_ when`VAR` happens to be whitespace:

```bash
VAR=''; [[ $VAR ]] && echo set || echo unset # WRONG!
VAR=''; [[ ${VAR+x} ]] && echo set || echo unset # Ok
```

Used the test framework itself to check that all tests passed before and after
the change.

# SGA Format Inconsistency {#sec:sga-fmt-incons}

From the TODO list:

>  The user doc has an inconsistency: the sort cmd on line 105 (p. 3) works with
>  _any_ whitespace, but l. 35 states that SGA is TAB-delimited. This is wrong, as
>  SGA may use any whitespace as delimiter.

To recap, SGA has the following fields:

1. Sequence name (Char String)
1. Feature (Char String)
1. Sequence Position (Integer)
1. Strand (+/- or 0)
1. Tag Counts (Integer)

The `Sequence name` and `Feature` fields may conceivably contain spaces, so it does
matter if we split by TABs only or by any whitespace: in the latter case,
literal spaces would have to be escaped in some way. So I examined
documentation, actual SGA files, and code that reads them in order to estimate
the best course of action.

## Documentation

In fact, the [closest thing I found to a
specification of SGA](https://ccg.epfl.ch/chipseq/sga_specs.php) clearly states that
SGA is TAB-delimited. It is also decribed as a derivative of
[GFF](https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md),
which is also explicitly TAB-delimited.

## Actual SGA files

I counted the number of fields in each and every line of every `*.sga` file I
could find^[Except some with names in `.sga` but that turned out to be other
things, hence the `file` step in the commands below] on my machine, once using
any whitespace and once using only TABs:

```bash
$ locate '*.sga' | xargs file | awk -F ':' '/ASCII/ {print $1}' \
  | xargs awk '{print NF}' | sort -u
5
7284:[tjunier@tj-laptop:~/projects/epd/doc]-[13:45]-[1]-[!master]
$ locate '*.sga' | xargs file | awk -F ':' '/ASCII/ {print $1}' \
  | xargs awk -F $'\t' '{print NF}' | sort -u
5
```

It makes no difference, so I'd assume that, in practice, those files _are_
TAB-separated.  The set of SGA files used above contains 23 files for a total of
is 40,859,578 lines, so there seems to be a clear consensus about that.

## Code

At least some code does indeed split on whitespace, e.g. `compactsga.c`:

```c
127     /* Get SGA fields */
128     /* SEQ ID */
129     while (*buf != 0 && !isspace(*buf)) {
130       if (i >= SEQ_ID) {
131         fprintf(stderr, "Seq ID is too long \"%s\" \n", buf);
132         exit(1);
133       }
134       seq_id[i++] = *buf++;
135     }
```

This seems to be the general rule among C programs (try `grep -A 20 'Get SGA
fields' *.c` in `./src`.

On the other hand, Perl programs that read SGA do seem to split by TABs:

```bash
$ grep split sga2*.pl | uniq
sga2fps.pl:my @f = split(/\t/,$firstline);
sga2fps.pl:        my @ar=split(/\t/,$lin);
sga2gff.pl:my @f = split(/\t/,$firstline);
sga2gff.pl:            my @ar=split(/\t/,$lin);
sga2wigSmooth_FS.pl:    my @fields = split(/\t/, $line);
sga2wigSmooth_FS.pl:    ($chr, $f, $pos, $strand, $high) = split(/\t/, $line);
```

So we can make a quick test by producing a SGA file with spaces in the sequence
or feature:

```bash
$ head -5 ../data/test_countsga-wSpc.sga
NC_000001.10	DNaseI-HS	10245	+	1
NC 000001.10	DNaseI-HS	10246	+	1
NC_000001.10	DNaseI HS	10265	-	2
NC_000001.10	DNaseI-HS	10267	-	1
NC_000001.10	DNaseI-HS	10458	+	3
```

Note the space in l. 2 (sequence) and l. 3 (feature). Well, `sga2bed` certainly
chokes on this:

```bash
$ ../src/bed2sga < <(head -5 test_countsga-wSpc.sga)
NC_000001.10	DNaseI-HS	1	+	1
NC	.10	1	0	1
NC_000001.10	DNaseI	0	0	1
NC_000001.10	DNaseI-HS	0	-	1
NC_000001.10	DNaseI-HS	1	+	1
```

Without these spaces (i.e., with TABs as separators), the output is:

```bash
7311:[tjunier@tj-laptop:~/Downloads/chipseq/data]-[14:33]-[3]-[!master]
$ ../src/bed2sga < <(head -5 test_countsga.sga)
NC_000001.10	DNaseI-HS	1	+	1
NC_000001.10	DNaseI-HS	1	+	1
NC_000001.10	DNaseI-HS	0	-	1
NC_000001.10	DNaseI-HS	0	-	1
NC_000001.10	DNaseI-HS	1	+	1
```

It is likely that the other SGA-reading programs will be affected in similar ways.

### Conclusion {#sec:sga-fmt-incons-conc}

We thus have the following options:

a. Decide that SGA is indeed (strictly) TAB-delimited, and reflect this in the C
code (and shell code that reads SGA, such as on p. 3 of the manual). This should
mostly be a matter of replacing `!isspace(*buf)` with `'\t' != *buf` in the
relevant files. We can use the above snippet of space-containing SGA, or
similarly modified test inputs,  to test the programs.

b. Consider that SGA accepts any whitespace as a delimiter, and keep the C code
as is (the Perl code as well as the documentation would have to be modified in
consequence).

I have a preference for switching to strict TABs, for the following reasons:

1. This would match the existing documentation (as well as about half the code).
2. It allows literal spaces in sequence and feature names (although, as we have
   seen, these do not seem to be used in practice (OTOH maybe the reason they're
   not used is precisely that the C tools would not work)).
3. SGA could be trivially converted to TSV by prepending a header line.

However, PB prefers spaces (2023-08-18 Zoom TC). One important point is that
consecutive whitespace should count as _one_ delimiter. So we need to check two
things:

1. That space is also allowed as a delimiter (not just tabs)
1. That any mix of consecutive whitespace characters functions as one (and only
   one) delimiter. This should be done by crafting test cases; indeed one could
   simply re-use the existing ones, modifying them to include a suitable mix of
   whitespace.

I thus went back to the code and looked for any use of the TAB character:

```bash
$ grep "'\\\t'" *.c # Yes, you need 3 \
sga2bed.c:      tokens = str_split (ext, '\t');
```

The `str_split()` function was written by GA, as we can see it accepts a
delimiter character, so it should be relatively trivial to pass it `' '` instead
of `'\t'`. The problem, of course, is that by doing this we'd disable tabs. What
I don't understand is why this particular program does it this way, while all
the others use a variation of

```c
        /* Get SGA fields */
        /* SEQ ID */
        while (*buf != 0 && !isspace(*buf)) {
            if (i >= SEQ_ID) {
                fprintf(stderr, "Seq ID is too long \"%s\" \n", buf);
                exit(1);
            }
            seq_id[i++] = *buf++;
        }
        while (isspace(*buf))
            buf++;
        /* FEATURE */
        i = 0;
        while (*buf != 0 && !isspace(*buf)) {
            if (i >= FT_MAX) {
                fprintf(stderr, "Feature is too long \"%s\" \n", buf);
                exit(1);
            }
            ft[i++] = *buf++;
        }
        while (isspace(*buf))
            buf++;
        /* etc. */
```

This piece of code in particular appears to be duplicated at least once (in
`sga2bed.c` and `sga2wig.c`). Given the ubiquitousness of the task (namely, to
split a line on whitespace), I wonder if this should not be moved to a separate
function. Indeed, this may be exactly what GA intended to do when she wrote
`str_split()` mentioned above. But this is a bit of work, so maybe it should be
for the next release.

As for the Perl code, the change should be as simple as replacing

```perl
split(/\t/,$lin);
```

with

```perl
split(/\s+/,$lin);
```

wherever relevant.

## New Tests {#sec:sga-fmt-tests}

So I added a series of tests that check how programs handle
field separation in SGA, as follows (in `./data`):

1. I extracted the first ten lines of a _bona fide_ SGA file, that (like all
   SGA files I've ever seen so far) uses only (single) TABs as a field separator:
    ```bash
    $ head wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga >
      test-whitespace-onlyTabs.sga
    ```

2. I run whatever script I want to test, using this file as an input, and use
   the output as expected output, e.g.:
   ```bash
   $ ../src/perl/sga2fps.pl test-whitespace-onlyTabs.sga \
     > ../src/tests/exp/test_sga2fps_WS.exp
   ```
   After this, I just test the program once on that output, to make sure it
   works (which it usually does).

3. I copied `test-whitespace-onlyTabs.sga` as `test-whitespace-mixed.sga`, then
   I altered it to (i) use more than one TAB as a separator, (ii) use a single
   space, (iii) use more than one space, and (iv) mix spaces and tabs.

4. Now I add a test of the program on this new input file, but _keeping the same
   expected output_. This checks that the program treats arbitrary whitespace
   exactly as it does single TABs.

The tests are `test_sga2fps_WS`, `test_sga2gff_WS`, and `test_sga2wigSmooth_FS-WS`.

# Manual-vs-Code Options Consistency {#sec:man-code-opt-cons}


From PB's 2023-05-30 mail:

>  The command line options listed in the User guide do not always correspond the
>  ones mentioned in the User Guide. Thomas, could you check all C programs
>  and Perl scripts in this respect, and amend the User Guide accordingly. You
>  do not have to describe the effects of the options if you can’t guess it. In
>  this case, just supply a place holder (?, to be explained, etc.).

Since PB wrote "user guide" twice, he must have meant either the code, the
manpages, or the help (when they exist). Since what really matters is the code,
I think the manpages, help and user guide should reflect that.

**NOTES**:

* This is a different task from checking the actual vs. documented options of
  Perl tools (see @sec:perl-opts), and it's also different from the manpages
  options check (@sec:man-opts-ck).
* The User Guide does not yet contain details on the use of the Perl scripts, so
  the checks PB asks for cannot (yet) be done. However, in the same mail he
  says: 

  > could you already include text sections in the User guide for the tools,
  > which we certainly will include and where a description is currently
  > missing. Preliminary text should be based on the usage instructions returned
  > by the programs

  Which would the issue, so that's what I did.

## C programs

What I did was to check what options/parameters/arguments^[Since mandatory
parameters are also passed via options, the distinction seems rather moot.] are
actually read by the program, and if they require an argument or not. Then I
compared this list with what is "advertised" by the user guide, and checked that
the lists were identical (including whether the options required an argument).
**Note** that options are listed in two places in the Guide: (i) the synopsis,
just below the section title named after the program (e.g. "2.1 chipcor"), and
(ii) the options and parameters tables below the program description.
In the code, the options are also listed in two places, namely (i) as arguments to
`getopt()` (and in the associated `switch` statement), as well as (ii) in the help text.

I therefore also checked that these four lists were consistent (bearing in mind that
the synopsis usually only lists mandatory parameters).

The programs are handled in the order in which they appear in the guide.

### `chipcor`

```c
631   int c = getopt (argc, argv, "c:n:dhoA:B:b:e:w:");
```

Ok (see Guide, p. 4).

### `chipcenter`

Added description of option `-i` (which was IIRC recently added to the code).

### `chipextract`

Ok.

### `chippeak`

Fixed `--f` -> `-f`.

### `chippart`

Adde arg to `-c`.

### `chipscore`

Fixed `--A` -> `-A`.

## Perl scripts

Since this a large change, I created a new branch named `ugperl`.

The first step is to list which of the Perl scripts are meant to be in
the release (and therefore covered in the Guide). These are listed in PB's mail,
which as I observed previously may be interpreted in different ways ("below"
seems to conflict with "not to be included"). My understanding is that all Perl
scripts except `array_size.pl` are to be included. But I will start with those
that are already mentioned (if only briefly described) in the Appendix.


# Harmonizing Program Behaviour with Insufficient Arguments {#sec:hpbia}

From the TODO list:

> Harmonize the behaviour of programs (both C and Perl) when passed with
> insufficient arguments. Note that some (e.g. `bed2sga`, `compactsga`) may wait
> for data on STDIN, while others (`sga2bed`) simply quit.

I am assuming here that the best behaviour in this situation is to exit with a
help message on STDERR and a nonzero exit code. So I called all the programs in
turn with _no arguments_ to see their behaviour. That's certainly not enough
arguments, but note that this doesn't necessarily show program behaviour when
only _some_ arguments are missing.

Program                    Behaviour   Exit code
-------                    ---------- ----------
`bed2bed_display`                help          1
`bed2sga`                       hangs         NA
`chipcenter`                     help          1
`chipcor`                        help          1
`chipextract`                    help          1
`chippart`                       help          1
`chippeak`                       help          1
`chipscore`                      help          1
`compactsga`                    hangs         NA
`countsga`                      hangs         NA
`featreplace`                   hangs         NA
`filter_counts`                 hangs         NA
`sga2bed`                     nothing          1
`sga2wig`                     nothing          1
`check_bed.pl`                   help          1
`check_replace_sga.pl`           help          1
`eland2sga.pl`                   help        255
`fetch_sga.pl`                nothing          0
`fps2sga.pl`                     help          1
`gff2sga.pl`                     help          1
`make_chro_idx.nstorage.pl`      help          1
`partit2bed.pl`                  help          1
`partit2gff.pl`                  help          1
`partit2sga.pl`                  help          1
`rmsk2bed.pl`                   hangs         NA
`rmsk2sga.pl`                   hangs         NA
`sga2fps.pl`                     help          1
`sga2gff.pl`                     help          1
`sga2wigSmooth_FS.pl`            help          0
`wigVS2sga.pl`                   help          1

In this context, 'nothing' means that the program silently exits with no output.

We note that `sga2bed` simply exits while `bed2sga` hangs. Maybe this will give
us a clue as to why there are different behaviours?  Well, one big clue is that
redirecting stdin to a file actually _doesn't work_ with `sga2bed`:

```bash
# in ./data
$ ../src/sga2bed test_countsga.sga | head -3
track name="DNaseI-HS" description="ChIP-Seq Custom data" visibility=1 color=100,100,100
chr1	10244	10245	DNaseI-HS	1	+
chr1	10245	10246	DNaseI-HS	1	+
$ ../src/sga2bed < test_countsga.sga | head -3
# no output
```

What happens here is that the program exits if there are no options or
arguments:

```c
// sga2bed.c, in main()
771   if (!((optind == 1) && (optind == argc))) {
        // stuff
778   } else {
779     return 1; // main() -> exit 1 
780   }
```

Now, when `sga2bed` is called without arguments, and due to the way
`getopt_long()` works (called in a loop starting at l. 611), both `argc` and
`optind` equal 1 when execution hits l. 771. The `if` evaluates to false, and
the next statement is `return 1;`, which causes the program to exit (since we're
in `main()`).

But of course, a redirection is not an argument, so when called as `sga2bed <
test_countsga.sga`, the exact same situation as before obtains, and the SGA is
never processed, contrary to what the help suggests (try passing `-h`). This is
clearly unintended: the program reads stdin if `argc` and `optind` are the same,
or (if the former is larger than the latter) if the first argument is '-':

```c
696   if (argc > optind) {
697       if(!strcmp(argv[optind],"-")) {
698           input = stdin;
699       } else {
            // set input to file named by 1st arg
          }
709   } else {
710       input = stdin;
711   }
```

Corollary: I believe that those programs that hang when called without input
actually exhibit the _correct_ behaviour, just like `cat` will wait forever if
given no argument and nothing on STDIN.

So I removed the `else` clause at ll. 778-780 so that `sga2bed` no longer exits
when called without arguments. This being C, this led to a good old segmentation
fault, which trusty Gdb tracked to a hash lookup.  Well, it turns out that the
"stuff" at ll. 772-777 (that is, when there _are_ options or parameters)
consists in building the AC hash (using the now-familiar `chr_NC_gi` file), but
as we've just seen this is not done when no arguments are passed, so that there
is no hash when data is passed on stdin. At some point a `NULL`  or
uninitialized pointer must be dereferenced, with predictable results. So I made
the hash-building step unconditional, and now it works.

The situation for `fetch_sga.pl` was different: this program does require
arguments (exactly 2), and I changed it so that instead of silently quitting or
trying to open a nonexistent file when not enough arguments were passed, it now
`die()`s with a usage message. There was no test for this script, so I added one
- but I had to guess what it does and how it does it, so my test may be wrong.

# Converter Tests {#sec:converter-tests}

From the TODO:

> Ensure that there is at least one test case for each of the "translator"
> utils (i.e. those with names of the form x2y). Input files for testing are
> referenced to in PB's 2023-05-31 mail (cf. next TODO item): they're either the
> output of other programs (such as chippart or repeatmasker), or the mail
> contains pointers to them.

The C converters (namely, `bed2bed_display`, `bed2sga`, `sga2bed`, and
`sga2wig`) all already have at least one test case. For the Perl ones, however,
it's the opposite. So I went to PB's mail (actually it was from 2023-05-30 - see
`.doc/mail/PB_2023-05-30.txt`), as well as another mail from PB on 2023-06-15
(`./doc/mail/PB_2023-06-15.txt`) that contained a tarball
(`chipseq_reformat_test.tar.gz`) with input files, expected outputs, and test
commands for some of the converters (in a Bash script named `tests.sh`). I
extracted the tarball in `./data` under the `chipseq` repo. 

**NOTE** that the Perl scripts in this tarball are a little older than the ones
in the latest commit (`cf1b90522367e8421f06a6be7309d0465e4f1a4d`) - for example,
they still have the older `my $DB = '/home/local/db/genome/';` instead of the
newer `my $DB = './';`. So I'll be using the more recent versions, in
`./src/perl`.

PB also points out that many (most ?) of the Perl utils have no `--db` option.

What follows is what I managed to do for each util, in alphabetical order (`ls
*2*.pl`).

## `chr_replace_sga.pl` {#sec:chr_replace_sga}

There is no sample test case in `tests.sh` nor is there an expected-output file,
so I inferred a test input from the program's description in the comments.
**THIS MUST BE VALIDATED!**

Also, I added option `-d` to specify a dir for the database file. This works
exactly like the `--db` option of other programs (I used a short option only
because the script uses `Getopt::Std`, not `Getopt::Long`), namely:

```perl
# sga2fps.pl, l. 32
# $DB is set to the arg to --db, or ./ by default
open FH, $DB."chro_idx.nstorage"
```

Note that this **only works if `$DB` contains a trailing slash** (this is why
the default is `./`, not just `.`). In my humble opinion something like

```bash
open FH, $DB . '/' . "chro_idx.nstorage";
# or
open FH, join('/', ($DB, "chro_idx.nstorage"));
```

might be better and would be more in line with usual behaviour. In fact, it's
not clear to me why we specify the _directory_ rather than the file itself: why
not have a default of `./chro_idx.nstorage` and pass a different filename to
`--db` when needed?

## `eland2sga.pl`

There are three input files which are the output from Eland, a sequence mapper:
`test_eland_[123].txt`. However, there is no command in `tests.sh` that could
indicate how `eland2sga.pl` is meant to be called, nor is there any output file
to check the results. The [link in PB's
mail](https://epd.expasy.org/chipseq/elandformat.php) leads to a page with what
seems to be the same Eland output files as in the tarball, but I wasn't able to
find any details about the expected output or the program's arguments.

Ok, let's try to at least guess how this is meant to be called. There are 3
mandatory arguments (all passed via options - I don't like this, but ok):
feature, species and input file. The last one should be obvious; the species is
probably an assembly code such as stored in `chro_idx.nstorage` (see
@sec:chro_aside below); as for the feature it's a little tricky: I first thought
it must correspond to one of the ELAND format's fields, but these are (according
to the above link):

1. Sequence name
1. Sequence
1. Type of match
1. Number of exact matches found
1. Number of 1-error matches found
1. Number of 2-error matches found (For unique best match, U, only:)
1. Genome file in which match was found (it indicates the chromosome number)
1. Position of match
1. Direction of match (F, forward; R, reverse)
1. How N characters in read were interpreted (., not applicable; D, deletion; I, insertion)
(For U1 and U2 matches only:)
1. Position and type of the first substitution error
1. Position and type of the second substitution error

none of which is called "feature". Does this feature then denote a part of the
_output_ instead? Might well be: the expression `$opts{'a'}` only ever appears
in `print` statements. It may well mean something like "do (whatever the program
does), and call the feature (whatever that may be) "my feature". So if I give it
a real Eland file, a real assembly and a completely arbitrary feature name, it
just might work:

```bash
# Assuming hg19
$ ../src/perl/eland2sga.pl -a DUMMY -s hg19 -f chipseq_reformat_test/test_eland_1.txt
parsing error of orientation in following line:
>1-1-199-437	GGTTGCATTGGTGCAATTGTT		U1	0	1	3	Homo_sapiens.NCBI36.42.dna.chromosome.6.fa	50704915	F	..	19A
NC_000001.10	DUMMY	82693040	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000004.11	DUMMY	116410190	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000005.9	DUMMY	16286199	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000006.11	DUMMY	150586572	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000007.13	DUMMY	25555362	-	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000010.10	DUMMY	104455324	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000013.10	DUMMY	95075033	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000016.9	DUMMY	45926437	-	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
```

Ok, well there's at least something to work with. Note that I had to give the
command in a directory that contains the famous `chro_idx.nstorage`- but I'll
fix this later as I did for the other programs.

First, what's with the _parsing error of orientation_? This only seems to happen
with `test_eland_1.txt`, btw - the other two don't have the problem (although to
be fair, `test_eland_2.txt` yields no output at all with the above paramters.
Well, the only field that may something to do with orientation is #9 ("Direction
of match"), so let's see about that in the code...

Indeed:

```perl
 46         if ($ar[8] eq 'F'){
 47             $score_ref->{$chr2AC->{$species}->{$chr}}->{'+'}->{$ar[7]} +=1;
 48         }
 49         elsif ($ar[8] eq 'R'){
 50             my $pos=$ar[7] + length($ar[1]);
 51             $score_ref->{$chr2AC->{$species}->{$chr}}->{'-'}->{$pos} +=1;
 52         }
 53         else{
 54             warn "parsing error of orientation in following line:\n$lin\n";
 55         }
```

Clearly the 9^th^ field (Perl's arrays are 0-based, so that's `$ar[8]`) is
matched against `F` or `R` (the acceptable values, namely "forward" and
"reverse"), and if it's neither we get an error. So far so good, but it seems
that the offending line, namely

```
>1-1-199-437  GGTTGCATTGGTGCAATTGTT   U1  0 1 3 Homo_sapiens.NCBI36.42.dna.chromosome.6.fa  50704915  F ..  19A
```

does contain a `F` as its ninth field... so what gives? Maybe good ol'`od` might
help?

Sure enough:

```bash
$ grep GGTTGCATTGGTGCAATTGTT < chipseq_reformat_test/test_eland_1.txt | od -c
0000000   >   1   -   1   -   1   9   9   -   4   3   7  \t   G   G   T
0000020   T   G   C   A   T   T   G   G   T   G   C   A   A   T   T   G
0000040   T   T  \t  \t   U   1  \t   0  \t   1  \t   3  \t   H   o   m
0000060   o   _   s   a   p   i   e   n   s   .   N   C   B   I   3   6
0000100   .   4   2   .   d   n   a   .   c   h   r   o   m   o   s   o
0000120   m   e   .   6   .   f   a  \t   5   0   7   0   4   9   1   5
0000140  \t   F  \t   .   .  \t   1   9   A  \n
```

See those two consecutive `\t` at position 42? There should most likely be only
one of them. So I changed this, and

```bash
$ ../src/perl/eland2sga.pl -a DUMMY -s hg19 -f chipseq_reformat_test/test_eland_1.txt
NC_000001.10	DUMMY	82693040	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000004.11	DUMMY	116410190	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000005.9	DUMMY	16286199	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000006.11	DUMMY	50704915	+	1
NC_000006.11	DUMMY	150586572	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000007.13	DUMMY	25555362	-	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000010.10	DUMMY	104455324	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000013.10	DUMMY	95075033	+	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
NC_000016.9	DUMMY	45926437	-	1
grep: .//eukaryote.ptr: No such file or directory
	END		0	1
```

Problem solved. On to the next one, which is this error message from `grep`:
`grep: .//eukaryote.ptr: No such file or directory`. This is on l. 116:

```perl
116    my $line = `grep $chr2AC->{$species}->{'chr'.$chro} $DB/eukaryote.ptr`;
```

Well, I'm afraid I have no idea where to find this `eukaryote.ptr` file. Note in
passing that this snippet uses a different convention from the one used to
locate the nstore file, e.g. on l. 24:

```perl
 24    my $chr2AC = retrieve($DB."chro_idx.nstorage");
```

On l. 24, `$DB` is expected to end with a `/`, while on l. 116 it's not (though
it won't do any real harm since the [UNIX
specification](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_271)
states that consecutive slashes are (almost always) equivalent to a single
slash).

In any case, without this `eukaryote.ptr` file I'll have to come back to this later.

Well, now it's later :-) and PB told me where to find that file, namely on
`epdnew.vital-lt.ch` (see mail on 2023-08-21). Unfortunately, I cannot log into
that machine from `rserv01` (logging into `rserv01` itself works seamlessly,
using `openfortivpn`).

Ok, well it was a stupid mistake on my part (tried to login from the wrong
machine), so I've been able to get that `eukaryote.ptr` as indicated. 

And...

```bash
../src/perl/eland2sga.pl -a DUMMY -s hg19 -f chipseq_reformat_test/test_eland_1.txt
NC_000001.10	DUMMY	82693040	+	1
NC_000001.10	END	249250621	0	1
NC_000004.11	DUMMY	116410190	+	1
NC_000004.11	END	191154276	0	1
NC_000005.9	DUMMY	16286199	+	1
NC_000005.9	END	180915260	0	1
NC_000006.11	DUMMY	50704915	+	1
NC_000006.11	DUMMY	150586572	+	1
NC_000006.11	END	171115067	0	1
NC_000007.13	DUMMY	25555362	-	1
NC_000007.13	END	159138663	0	1
NC_000010.10	DUMMY	104455324	+	1
NC_000010.10	END	135534747	0	1
NC_000013.10	DUMMY	95075033	+	1
NC_000013.10	END	115169878	0	1
NC_000016.9	DUMMY	45926437	-	1
NC_000016.9	END	90354753	0	1
```

Now, PB in his 2023-08-21 mail says that this file is obsolete anyway, and shows
how to get rid of it by using only `chr_size`. However, this means changing the
code, so to minimize the risk of introducing bugs, I will first add a test case
with the above parameters and expected output, and _then_ do the changes
suggested by PB, using the test as a reference.

Well, now it's later :-) and PB told me where to find that file, namely on
`epdnew.vital-lt.ch` (see mail on 2023-08-21). Unfortunately, I cannot log into
that machine from `rserv01` (logging into `rserv01` itself works seamlessly,
using `openfortivpn`).

## `fps2sga.pl`

Added a test case based on `tests.sh` - ok. PB mentions that this script "works:
has no --db option", which can mean either that it works ok without the option
(and doesn't need one) or that even though it works it would still be useful to
add the option. The code doesn't seem to have a `$DB` variable (which is where
the Perl scripts in the suite typically store the location of the main data
file, be it `chro_idx.nstorage` or `genome` or else `*.chrom.sizes`), though, so
I'll veer towards the first hypothesis and leave it at that.

## `gff2sga.pl`

As above; also added a test of the `--db` option (this was already implemented,
just added the test, especially because the original test passed `./` as
argument to `--db` - this being in fact the default, I added a test that used a
different value.

## `partit2bed.pl`

As above - added two tests, one with and one without `--db`.

## `partit2sga.pl`

This scripts fails with "can't open .//mm9/mm9'.'.chrom.sizes' : No such file or
directory at ../src/perl/partit2sga.pl line 50, <PART> line 1." It apears to be
searching for a file in some subdirectory of `$DB` (passable via `--db`, default
is `./`) named after the assembly (which it obtains by looking up the first
field of the first line in a hash stored in `chro_idx.nstorage`). The filename
in the error message above seems a tad garbled, as I understand it should be
something like `./mm9/mm9.chrom.sizes` (give or take a subdir...). Anyway
there are no subdirs in `./data`, so there is no way this can work without
adding the relevant data at the required location.

Furthermore, there is no "ground truth" (that is an expected-output file) for
this script. I supose we could pipe the resulting SGA to `sga2bed` and see if we
get the same result as when we call `partit2bed.pl`.

## `partit2gff.pl`

This script works without problems (e.g. `../src/perl/partit2gff.pl
chippart_ES_H3K27me3.sga`) but there is no expected-output file or example command in
`tests.sh`. I tried to pass the (GFF) output to `gff2sga.pl`, and then to
`sga2bed` in the hope of obtaining the same output as with the more direct
`partit2bed.pl` (see above), but the outputs are different.

During the 2023-08-18 Zoom TC, PB brought the following information:

> use `data/chippart_164940_172769.{sga,gff}`, mind the difference in 1st colum
> ('1' vs 'chr1', etc.). 

The "difference in the 1st column" is this:

```bash
$ ../src/perl/partit2gff.pl chippart_164940_172769.sga | head -3
##gff-version   3
chr1	ChIPSeq	H3K27me3	3659282	3666069	112	+	.	1
chr1	ChIPSeq	H3K27me3	4478422	4489966	428	+	.	2
$ head -3 chippart_164940_172769.gff                            
##gff-version   3
1	ChIPSeq	H3K27me3	3659282	3666069	112	+	.	1
1	ChIPSeq	H3K27me3	4478422	4489966	428	+	.	2
```

This has to do, as PB explains is his subsequent mail (2023-08-21), with the
version of `chro_idx.nstorage` used. Bottom line: PB sees no need for action, so
I'll leave it at that, but I'll see if we can make the expected and actual
outputs coincide by just prefixing `chr` to the chromosome numbers (or letters)
in the expected-output file:

```bash
# In ./data
$ sed '/^[1-9XY]/s/^/chr/' < chippart_164940_172769.gff > test_partit2gff.exp
$ ../src/perl/partit2gff.pl chippart_164940_172769.sga > out
$ diff -s out test_partit2gff.exp
Files out and test_partit2gff.exp are identical
```

So I added a test for `partit2gff.pl` using the above file for expected results.
All tests ok.

## `rmsk2bed.pl`

Added test as in `tests.sh`.

## `rmsk2sga.pl`

Added test as in `tests.sh`.

## `sga2fps.pl`

No indications for this one either, but the help message is enough to come up at
least with a rgression test (i.e., one that checks that the output isn't
accidentally modified - whether said output is _correct_ is another matter).

Added a test to the test set; also added a test for whitespace handling (see @sec:sga-fmt-tests).

## `sga2gff.pl`

Again, no indications for this one, but again the help message is enough to come up at
least with a rgression test.

As above, added a test to the test set; also added a test for whitespace handling.

## `sga2wigSmooth_FS.pl`

Same as above.

# Aside: `chro_idx.nstorage` {#sec:chro_aside}

`chro_idx.nstorage` is one of the few binary files (apart from images and
executables, of course) in our distribution. It's a Perl `Storable` file, i.e. a
file that stores Perl data (à la Python's Pickle, Shelve, et al.). Compared to
storing our data in a text file, it's faster and leaves the parsing to Perl
rather than forcing us to do it ourselves.

The code seems to use it to retrieve data of various kinds:

```perl
# assembly, given AC (eland2sga, l.70)
$chr2AC->{$ac} =~ s/chr//;
# length, given (presumably) an AC (partit2bed.pl, l. 205) - this hints at the
# existence of a hash of lengths
$chroac->{'length'}->{$field[0]}
```

But since the storage file is binary, it's obscure. So what does it contain?
Well, `Storable`'s `retrieve` function returns a reference to a hash, so we can
get an idea of the data structure with `Data::Dumper`:

```perl
#!/usr/bin/env perl

use strict;
use Storable qw(retrieve);
use Data::Dumper;

my $nstorage = $ARGV[0];
my $key      = $ARGV[1];

my $href = retrieve($nstorage);
# Dumps the whole structure
#print Dumper(%$href);
# Dumps the value associated to $key
print Dumper($href->{$key});
```

With the above in a file named `show-nstore.pl`, we can begin to peek:
following means:

```bash
$ ./show-nstore.pl ~/Downloads/chipseq/data/chro_idx.nstorage length | head -5
$VAR1 = {
          'NW_003382930.1' => '1640',
          'NW_016711928.1' => '774',
          'NW_016791955.1' => '218',
          'NW_016801328.1' => '202',
```

So the value asociated with key `length` is a hash of AC-indexed lengths.

```bash
$ ./show-nstore.pl ~/Downloads/chipseq/data/chro_idx.nstorage assembly | head -5
$VAR1 = {
          'NW_016756234.1' => 'xenLae2',
          'NW_016734505.1' => 'xenLae2',
          'NW_016726620.1' => 'xenLae2',
          'GL187286.1' => 'xenTro3',
```

In the same vein, we have a hash of AC-indexed assemblies. When we pass an
assembly as key, we get a hash of... AC/chr# -> AC?

```bash
$ ./show-nstore.pl ~/Downloads/chipseq/data/chro_idx.nstorage mm8 H
$VAR1 = {
          '8' => 'NC_000074.4',
          'chr9' => 'NC_000075.4',
          '16' => 'NC_000082.4',
          'chr2' => 'NC_000068.5',
          'chr4' => 'NC_000070.4',
          '13' => 'NC_000079.4',
          'chr16' => 'NC_000082.4',
          'chr10' => 'NC_000076.4',
          'NC_000085.4' => 'NC_000085.4',
```

And what happens when we pass an AC?

```bash
$ ./show-nstore.pl ~/Downloads/chipseq/data/chro_idx.nstorage NC_000074.4
$VAR1 = 'chr8';
$ ./show-nstore.pl ~/Downloads/chipseq/data/chro_idx.nstorage GL174090.1
$VAR1 = 'GL174090';
```

## BED File Format

From the TODO: "check if BED is TAB- or whitespace-delimited."

The [official specification](https://samtools.github.io/hts-specs/BEDv1.pdf) of
BED says that it is "whitespace-separated", but with the following important
precision about separators:

> **ﬁeld separator**: One or more horizontal whitespace characters (space or tab).
> The ﬁeld separator must match the regex `[ \t]+`. The ﬁeld separator can vary
> throughout the ﬁle.  Some capabilities of the BED format, however, are
> available only when a single tab is used as the ﬁeld separator throughout the
> ﬁle

The above regular expression means that a _string of spaces and/or tabs_
functions as a _single_ separator (at least in some cases). This is possible, as
I understand, only because all of BED's fields have a fixed format that can be
parsed with regular expressions, e.g. the `chrom` field must match
`[[:alnum:]_]{1,255}` (see Table 2 in the spec). In other words, if we denote
the regex for field 1 as RE1, etc, the whole BED line can be parsed by
`RE1\s+RE2\s+RE3\s+ ... RE12`. But even then, feature names may contain
whitespace only if the separator is a _single_ tab (see below).

In particular, the regexp for the `name` field, which describes the feature that
the BED line is about, is `[\x20-\x7e]{1,255}`. Its description reads:

> `name`: String that describes the feature. `name` must be 1 to 255 non-tab
> characters. `name` must not contain whitespace, unless the only ﬁeld separator
> is a single tab.`

## Characters allowed in SGA Feature Field

From the TODO list:

> Make a list of characters that are allowed in SGA feature field (2023-08-18
> TC). Emit a recommendation to users about preferred characters. 

for the _preferred_ characters, I suggest adopting the same convention as BED
  does for its `chrom` field (**not** `name`, which allows whitespace), but
  perhaps without the length cap: essentially `[[:alnum:]_]+`. On top of that,
  we could _allow_ punctuation signs like `:_$§@#%&/(){}.`

## Re-making of the genome information files

Using the latest tarball (`chipseq_reformatting_test.tar.gz`), I first made
backup copies of `chr_NC_gi` and `chr_size`. Then I made a list of assemblies
common to both:

```bash
# in ./data/chipseq_reformatting_test
$ comm -12 <(sed -n '/^#/{s/\s.*$//;p}' < chr_NC_gi | sort) \
           <(sed -n '/^#/{s/\s.*$//;p}' < chr_size | sort) \
  | sed 's/^/^/;s/$/\\s/' > common-assemblies # suitable for grep
```

Then we remove the unwanted assemblies by first converting the files to
single-line, then `grep`ping on the above file:

```bash
$ sed -n '1{h;d};/^#/{x;s/\n/§/g;p;x;h};/^#/!H;${x;s/\n/§/g;p}' < chr_NC_gi \
  | grep -f common-assemblies | tr '§' '\n' | sed '/^$/d' > chr_NC_gi_new
```

and _mutatis mutandis_ for `chr_size`:

```bash
$ sed -n '1{h;d};/^#/{x;s/\n/§/g;p;x;h};/^#/!H;${x;s/\n/§/g;p}' < chr_size \
  | grep -f common-assemblies | tr '§' '\n' | sed '/^$/d' > chr_size_new
```

**Note**: this could be done in one go with `sponge`, but I kept temp files to
enable more sanity checks, which all passed. So eventually:

```bash
$ mv chr_NC_gi_new chr_NC_gi
$ mv chr_size_new chr_size
```

And as a final sanity check, we look for any assembly unique to either file
(i.e., _not_ common to both):

```bash
$ comm  -3 <(sed -n '/^#/{s/\s.*$//;p}' < chr_NC_gi | sort) \
           <(sed -n '/^#/{s/\s.*$//;p}' < chr_size | sort) \
# no output
```

Ok - the assembly lists are the same. We now check that the sequence ACs are
identical (fields #2 and #1 of `chr_NC_gi` `chr_size`, respectively):

```bash
$ diff  -q <(grep -v '^#' < chr_NC_gi | awk '{print $2}' | sort) <(grep -v '^#' < chr_size | awk '{print $1}' | sort)
Files /proc/self/fd/11 and /proc/self/fd/12 differ
```

Uh-oh. They're not _quite_ identical. Let's take a closer look:

```bash
$ diff   <(grep -v '^#' < chr_NC_gi | awk '{print $2}' | sort) <(grep -v '^#' < chr_size | awk '{print $1}' | sort) H
19716a19717
> NC_000845.1
19750,19752c19751,19773
< NC_001320.1
< NC_001666.2
< NC_001751.1
---
> NC_001224.1
> NC_001224.1
> NC_001224.1
```

Let's take the first AC on that list, namely `NC_000845.1`:

```bash
$ grep NC_000845.1 < chr_NC_gi
$ grep NC_000845.1 < chr_size
NC_000845.1	16613
```

Just to make sure that this error was not introduced by my procedure:

```bash
$ grep NC_000845.1 < chr_NC_gi.orig
$ grep NC_000845.1 < chr_size.orig
NC_000845.1	16613
```

Nope, the situation was already in the originals. I'll have to see about this
with PB.

**UPDATE** (2023-09-28) PB replied with a list of what to do with each sequence,
which I implemented (by hand for `chr_NC_gi_new`^[This time I kept the `_new`
version of the files till the end]: since there were only a few, but with a Sed
script for `chr_size` since there were many and the only action required was to
deletete them all).

```bash
$ sed 's|^\(.*\)$|/^\1/d|' < only_chr_size > delNC.sed
$ sed -ibak -f delNC.sed chr_size_new
```

And finally:

```bash
$ diff -s <(grep -v '^#' < chr_NC_gi_new | awk '{print $2}' | sort) <(grep -v '^#' < chr_size_new | awk '{print $1}' | sort)
Files /proc/self/fd/11 and /proc/self/fd/12 are identical
```

So now I can finally move the `_new` versions to their original (and final...)
names:

```bash
$ mv chr_NC_gi_new chr_NC_gi
$ mv chr_size_new chr_size
```

And finally, back in `./data`, I replace the old versions with the new ones:

```bash
$ cd ..
# Now in ./data
$ chmod +200 chr_NC_gi chr_size
$ cp chipseq_reformatting_test/chr_{NC_gi,size} .
$ chmod 400 chr_NC_gi chr_size
```

This leads us to the last step, namely the recomputation of the Perl hash that
stores everything:

```bash
../src/perl/make_chro_idx.nstorage.pl chr_NC_gi chr_size chro_idx.nstorage
# No output
```

The new version is almost ten times _smaller_ than the old one, but I suppose it
can be due to the fact that we removed material and didn't add any. Still, this
should be checked:

```bash
$ ls -l chro_idx.nstorage*
-rw-r--r-- 1 tjunier tjunier  2988223 Oct  5 12:37 chro_idx.nstorage
-r-------- 1 tjunier tjunier 19942205 Oct  5 12:37 chro_idx.nstorage.bak
```

## New Test Cases

PB sent a series of test commands along with expected outputs. The tests are in
`reformatting_tests.sh`, and the output in `reformatting_restults.txt` (sic).
All in all there are over 70 tests, so it would be convenient if we could
add them to our existing tests with at least a modicum of automation.
Fortunately, this appears to be the case: the tests file contains actual test
commands, and the results files contains outputs (which are actually often
empty). What is needed is a systematic way to construct test names.

This could be done by using the string echoed in the test file, e.g.

```bash
# reformatting_tests.sh
...
echo "sga2fps.pl            hg19.sga"        ; ../src/perl/sga2fps.pl            hg19.sga
...
```

The part before the `;` serves to identify the test in the toutput file, while
the part after the `;` is the command itself. We could try to convert the former
into a test name, e.g.;

```bash
$ grep '^echo' < reformatting_tests.sh | cut -d';' -f1 \
  | sed 's/echo //;s/\.pl\s\+/_/;s/[ .]\+/_/g;s/"//g;s/_$//' \
  > test_names
$ sort < test_names | uniq -d # no output - ok.
```

We can now extract the commands, except that we need to adapt the paths:

```bash
$ grep '^echo' < reformatting_tests.sh | cut -d';' -f2 \
  | rev | sed 's/ /§/' | rev \
  | sed 's/^ \.\.\/src/"$BIN_DIR"/;s/§/ "$DATA_DIR"\//' \
  > test_commands
```

And finally:

```bash
$ paste test_names test_commands >> ../../src/tests/test_cmds.tsv
```

Now of course we dont'have the expected output files, so none of these tests can
pass. As we said above, most of these files are actually empty, but we can
extract the non-empty ones from `reformatting_restults.txt`:

```bash
$ sed -n '/^#/d;/\.pl/h;/\.pl/!{x;s/\.pl\s\+/_/;s/[ .]\+/_/g;p}' < reformatting_restults.txt > nonempty_test_names
$ sed -n '/^#/d;/\.pl/!p' < reformatting_restults.txt > nonempty_test_exp
$ paste -d '%' nonempty_test_names nonempty_test_exp \
  | awk -F '%' '{fn="../../src/tests/exp/test_"$1".exp"; print $2 > fn}'
```

As for the empty ones, we can do a set difference from the list of all test
names:

```bash
$ sed -n '/\.pl/{s/\.pl\s\+/_/;s/[ .]\+/_/g;p}' < reformatting_restults.txt > all_test_names
$ comm -3 <(sort nonempty_test_names) <(sort all_test_names) | while read tname; do
  touch ../../src/tests/exp/test_"$tname".exp 
done
```

## Check for Rejection if RefSeq ID is invalid

From the TODO list:

>  Test the behaviour of reformatters/converters when passed invalid sequence
>  IDs/ACs (see e.g. `test_sga2fps_-s_xenTro3_xenTro3_chr_sga`, which correctly
>  rejects an invalid RefSeq ID). Ieally, the behavour should be consistent
>  across the various programs in the distribution, but that may be for the next
>  release. Also consider the case when IDs from different assemblies [...] or
>  even different species [...] are mixed.

First of all, what does a RefSeq ID look like? Well, it consists of a
[prefix](https://www.ncbi.nlm.nih.gov/books/NBK21091/table/ch18.T.refseq_accession_numbers_and_mole/?report=objectonly),
an accession number, and a version number, e.g. `NP_001068434.2`.

Now, let's make a list of formats accepted as input by our converters and check
if they make use of RefSeq IDs (`echo ../*2*(*:t) ../perl/*2*.pl(:t) | tr ' '
  '\n' | sed 's/2.*$//' | sort -u`):

Format  Uses RefSeq?
------- -------------
BED     No
Eland   No^[Judging by `./data/test_eland_1.txt`]
FPS     Yes, field 4
GFF     Yes, field 1
Partit  Yes, field 1
Rmsk    No^[Judging by the fact that `grep '\<[A-Z][A-Z]_[0-9]' < dm6_rmsk.txt` yields nothing.]
SGA     Yes, field 1
wigVS   (not found)

Note that the presence of RefSeq IDs in files of the above formats does not
necessarily mean that _only_ RefSeq is accepted by the programs in question.

Now let's see if we have versions of these formats with invalid RefSeq IDs (in
fact we know we have such wrong files for at least some formats, because I just
added tests for precisely this not longer ago than this morning):

Format  File
------- -----
FPS     `xenTro3_chr.fps`
GFF     `xenTro3_chr.gff`
Partit  No, made one from `chippart_ES_H3K27me3.sga`^[`sed 's/^NC_//;q' < chippart_ES_H3K27me3.sga > test_badRefSeq.partit.`]
SGA     `xenTro3_chr.sga`

Now let's see which converters need to be tested on the above files:

Converter       Steps Taken
----------      ------------
`fps2sga.pl`    Added test on `xenTro3_chr.fps`

### Mixed species

Made files for testing:

FPS: `mixed_bt8-hg19.fps` 
GFF: `mixed_bt8-hg19.gff` 
Partit: not sure, only have 1 file (`chippart_ES_H3K27me3.sga`)
SGA: `mixed_bt8-hg19.sga` 

### Mixed assemblies

Couldn't make files for testing: 

No species with more than one assemblies (true for all 4 formats).

**UPDATE** In fact we want to know the _behaviour_ of scripts and programs when
passed invalid RefSeq IDs -  not necessarily to add tests of it.

So to quickly check for that, I produced versions of all input files in which
the _first_ instance of a RefSeq ID was stripped of its prefix, making it
invalid. I only change the first instance so that the diffs WRT the expected
outputs are small (only one line will differ). Of course I did this on a
separate branch:

```{#lst:garble .bash}
$ git co -b wrongIDs
# in ./data
# GNU sed only!
$ sed -i '0,/[A-Z][A-Z]_[0-9]\+/s/[A-Z][A-Z]_//' *.{fps,gff,sga}
```

And now back in `./src/tests`, where I run the test system:

```bash
$ ./test_main.sh
...
81 successes
52 failures
0 errors
```

And now I can examine the diffs:

```bash
$ for out in out/*.out; do
  echo $out
  exp=${out//out/exp}
  diff $out $exp
  read _
done
```

Armed with this info I produced the following table:

Program           Behaviour when passed an invalid RefSeq ID
--------          -------------------------------------------
`chipcenter`      Ignored
`chipcor`         Ignored
`chippart`        Ignored
`compactsga`      Ignored
`countsga`        Ignored
`featreplace`     Indifferent
`filter_counts`   Indifferent
`fps2sga`         Ignored
`gff2sga`         Indifferent
`partit2bed`      ID replaced by `chr`
`partit2gff`      Ignored
`sga2bed`         Ignored
`sga2fps`         Ignored
`sga2fps`         Ignored
`sga2gff`         Ignored
`sga2wig`         Ignored
`sga2wigSmoothFS` Ignored

: Program Behaviour when passed invalid RefSeq IDs. {#tbl:invalid-RSID}


Note that the test files contained only _one_ faulty ID, so some variant
behaviours may not have been elicited.

By 'Ignored' I mean that the program doesn't emit a warning, but doesn't take
the faulty line into account, because the output is different from that produced
when the ID is correct. The outputs are then significantly different. By
'Indifferent' I mean that the program does not distinguish between correct and
incorrect IDs, so that the output differs only to the extent that the ID also
lacks the prefix.

**UPDATE**: during the 2023-10-13 TC, PB clarifed as follows: we should test the
behaviour using _two_ files, namely (i) one in which we garble the first RefSeq
ID, and (ii) another in which we remove the line that contains said ID
altogether. If removing the line has the _same_ effect as garbling the ID, then
we can assume the program ignores lines with invalid IDs; otherwise it's more
complicated. If the output differs (between the two modified input files), then
we can assume that the program accepts the invalid RefSeq ID.

Now we need a list of files for which we need to either garble the first RefSeq
Id, or remove the corresponding line altogether. I already did something similar
(see @lst:garble). But now we want to ignore files that have only one line
(since we also want to remove lines with RefSeq IDs). So I looked at the number
of lines in all SGA, FPS and GFF files that match pattern `\<NC_[0-9]\+`:

```bash
$ grep -l -m1 '\<NC_[0-9]\+' *.{sga,gff,fps} | xargs wc -l
   36056 chippart_164940_172769.sga
   36056 chippart_ES_H3K27me3.sga
  124141 genomic_hit_MA0137.3_STAT1-full.sga
  124141 genomic_hit_MA0137.3_STAT1.sga
       1 hg19.sga
  313701 STAT1_stim_small-full.sga
  313701 STAT1_stim_small.sga
      20 test_countsga.sga
      20 test_countsga-wSpc.sga
   36056 test_partit2gff_input.sga
      10 test-whitespace-mixed.sga
      10 test-whitespace-onlyTabs.sga
     100 wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga
       2 hg19.gff
       1 hg19.fps
   29595 Hs_EPDnew_006_hg19.fps
 1013611 total
```

Ok, let's try to get the _smallest_ of each format, but ignoring those files
that have only one or two lines (two-line files may contain only one data line,
the other being a header):

```bash
$ grep -l -m1 '\<NC_[0-9]\+' *.{sga,gff,fps} | xargs wc -l \
  | mlr --n2t --repifs label size,name                     \
        then filter '$name != "total" && $size > 2'        \
        then put '$name =~ "\.(...)$"; $fmt="\1";'         \
        then sort -n size then head -n 1 -g fmt then cut -f size,name
size	name
10	test-whitespace-mixed.sga
29595	Hs_EPDnew_006_hg19.fps
```

Ok, so no GFF file fits the bill -- we'll have to make one. OTOH, there are a
few other small SGA files, for example `test_countsga.sga` which is only 20
lines long (redo the above command, but take the first 3 lines for each format).

### Making a large-enough GFF

We do have a large GFF file: `hg19_TSC_v1.gff` (it has more than 70k lines, that
ought to be enough...), but it has no RefSeq IDs: its first field is `chr1`,
`chr2`, etc. So let's just take the first 10 lines (all `chr1`), replace `chr1`
with `NC_000001.10` (a valid RefSeq ID) and we'll have a short GFF with valid
IDs (which we'll proceed to garble shortly):

```bash
$ sed 's/^chr1/NC_000001.10/;10q' < hg19_TSC_v1.gff > short.gff
```

So now we have three "originals", one each for SGA, FPS and GFF. Let's produce
an "invalid Refseq" version of each:

```bash
$ for file in Hs_EPDnew_006_hg19.fps test_countsga.sga short.gff; do
    sed '0,/[A-Z][A-Z]_[0-9]\+/s/[A-Z][A-Z]_//' < "$file" > "${file/./_badId.}"
done
```

Likewise, but remove the first line:

```bash
$ for file in Hs_EPDnew_006_hg19.fps test_countsga.sga short.gff; do
    sed '0,/[A-Z][A-Z]_[0-9]\+/{/[A-Z][A-Z]_/d}' < "$file" > "${file/./_no1st.}"
done
```

Ok.

Now, let's think about what programs need to be tested. Starting from
`test_cmds.tsv`, I made a script that keeps one test per program, for all
programs that input FPS, GFF or SGA. This script is called `ck_bad_RefSeqID.sh`.
It lives in `./data`, where it is meant to be launched. It's a sort of
simplified `test_main.sh`.

Now, this script pretty much tells me which SGA, etc. files I need to produce
test versions of (i.e., ones with garbled RefSeq IDs or a missing line). Take
SGA, for example:

```bash
$ grep -Eo '[^ ]+\.sga' < ck_bad_RefSeqID.sh | sort -u
chippart_ES_H3K27me3.sga
genomic_hit_MA0137.3_STAT1.sga
hg19.sga
STAT1_stim_small.sga
test_countsga.sga
test_partit2gff_input.sga
wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga
```

That's just 7 files, it would be easy enough to produce the corresponding test
versions. But do they contain RefSeq IDs at all?

```bash
$ grep -Eo '[^ ]+\.sga' < ck_bad_RefSeqID.sh | sort -u | xargs grep -m1 '[A-Z][A-Z]_'
chippart_ES_H3K27me3.sga:NC_000067.5	H3K27me3	3659282	+	112
genomic_hit_MA0137.3_STAT1.sga:NC_000001.10	STAT1	69058	-	1
hg19.sga:NC_000001.10	F	1	+	1
STAT1_stim_small.sga:NC_000021.8	STAT1	10698193	-	1
test_countsga.sga:NC_000001.10	DNaseI-HS	10245	+	1
test_partit2gff_input.sga:NC_000067.5	H3K27me3	3659282	+	112
wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2.sga:NC_000001.10	DNaseI-HS	10245	+	1
```

Yep, they do. So let's produce garbled and one-fewer-line versions, just as I
did above:

```zsh
$ files=($(grep -Eo '[^ ]+\.sga' < ck_bad_RefSeqID.sh | sort -u))
# Zsh, GNU sed!
$ for file in $files; do
    sed '0,/[A-Z][A-Z]_[0-9]\+/s/[A-Z][A-Z]_//' < "$file" > "${file/./_badId.}"
done
$ for file in $files; do
    sed '0,/[A-Z][A-Z]_[0-9]\+/{/[A-Z][A-Z]_/d}' < "$file" > "${file/./_no1st.}"
done
```

Now of course, `hg19_no1st.sga` is actually empty, so the files made above may
turn out to be useful after all. We'll see about that later.

Now I made a script, `ck_bad_RefSeqID.sh`, that runs the test cases against both
versions of the input file, distinguished by the `_badId` or `_no1st` infix in
their names. This looks like the following:

```bash
$ ./ck_bad_RefSeqID.sh | head -25

test_fps2sga
------------
1d0
< 000001.101-	TSS	894636;	 	1	NOC2L_1

test_gff2sga_db
---------------
1d0
< 000001.10	TSC	567305	+	1	.

test_gff2sga
------------
1d0
< 000001.10	TSC	567305	+	1	.

test_bed2bed_display_ARG1
-------------------------
Files test_bed2bed_display_ARG1.badId.out and test_bed2bed_display_ARG1.no1st.out are identical
WARNING: files are empty!

test_chipcenter_STDIN
---------------------
Files test_chipcenter_STDIN.badId.out and test_chipcenter_STDIN.no1st.out are identical
WARNING: files are empty!
```

Now with the above result I revisited table @tbl:invalid-RSID.

Program           Behaviour when passed an invalid RefSeq ID
--------          -------------------------------------------
`chipextract`     Identical outputs (but with warnings)
`chipcenter`      Identical outputs
`chipcor`         **Different** outputs (header line has different content)
`chippart`        Identical outputs (but warnings about bad ID)
`chippeak`        Identical outputs
`chipscore`       Identical outputs
`compactsga`      Identical outputs
`countsga`        **Different** outputs (total tag counts, sequence lengths are different)
`featreplace`     **Different** outputs (# lines; no NC_ in 1st line on badID)
`fetchsga`        **Different** outputs (# lines)
`filter_counts`   **Different** outputs (# lines; no NC_ in 1st line on badID)
`fps2sga`         different # lines; no NC_ in 1st line on badId
`gff2sga`         different # lines; no NC_ in 1st line on badId
`partit2bed`      **Different** outputs (# lines)
`partit2gff`      **Different** outputs (fld 4 missing from no1st)
`sga2bed`         Identical outputs
`sga2fps`         **Different** outputs (empty on badId - script aborts)
`sga2gff`         **Different** outputs (empty on badId - script aborts)
`sga2wig`         Identical outputs
`sga2wigSmoothFS` **Different** outputs (# lines)

: Program Behaviour when passed invalid RefSeq IDs, second approach (PB's
two-file criterion). Programs that yield identical outputs whether a line has an
invalid RefSerq ID or the line is absent altogether are inferred to ignore lines
with invalid RefSeq IDs. {#tbl:invalid-RSID-2}

# Documenting Code Changes

After my presentation of the test system as a V-IT talk on 2023-12-01, PB asked
my to document the changes in the code. Just how wasn't specified, but it seemed
obvious that the Git logs should at least provide a starting point. So I made
TSV files with what I thought might be relevant info:

```bash
for cfile in *.[ch]; do
  git --no-pager log --pretty="tformat:%h#%ai#%cn#%s" "$cfile" \
  | sed 's/\t/ /g;s/#/\t/g;' \
  | mlr --hi --tsv label abhash,date,author,subject \
  > "$cfile.commit-log.tsv"
done
# And mutatis mutandis for Perl code in ./perl
```

I also made a general log of all changes in the repo, `general.commit-log.tsv`.

One problem is that some commits pertain to more than one file. If we are aiming
at a single change log, that's ok, but if we envision one log per file, we have
to make sure our log messages are consitent, i.e. if we decide to change a
message (say to fix a typo), the change must be present in all logs. So what I
see as a possible path if we choose that option is to copy the general log, edit
the messages therein, and then create per-log files by joining on the commit
hashes and keeping only the edited form of the message.

# `system()` command problem

Just after my V-IT talk (2023-12-01), it was noticed, I think by SM, that there
were issues with a `system()` command on Macs (but only in Docker, IIRC) - a
warning about the exit code being ignored. This did not happen under Linux,
which prompted me to investigate. I found how to persuade GCC to emit the same
warning (namely, to add an `__attribute__` to the declaration of `system()`),
but more importantly, I noticed that this call to `system()` had a number of
unexpected properties:

1. It called `rm(1)`. This works, of course, but there is a system call (and a
   glibc wrapper over it), `unlink()`, that will do the job without the extra
   hassle of launching a shell (and having to deal with the multiple ways in
   which `system()` may fail). I thus replaced `system("rm ...")` with
   `unlink("...")`.
1. The file it called `rm` on was a temporary file, but its name was hard-coded,
   so that two different instances of a given program would share that temporary
   file. On a server, having two instances of a given Chipseq-suite program
   running at the same time is far from unrealistic. Moreover, the hard-coded
   name ws _shared_ by several programs, so that any time two of these ran
   together, they would also share that same file. Since the Chipseq programs
   wwre designed to be assembled into pipelines, a collision becomes almost
   certain. I was about to fix this by having the programs use unique temp files
   (see `mktemp(3)`), but before that I stumbled upon the next problem:
1. These temp files were used as a way of cheking whether a `sort(1)` command
   was successful (to make sure the SGA file was properly ordered): the tmp file
   was actually the (redirected) standard error of the `sort` command. Now this
   works, but it does seem a rather roundabout way of making sure a shell
   command succeeds: why not check the exit code? This is accessible as the
   return value of `system(3)`.^[To be fair, it's not _quite_ as simple as `if
   (! system("my command..."))`: `system()` can fail even before the commad is
   run, for example.] So eventually I removed the temporary file altogether, and
   examined the result of `system()` instead.

Also, it turned out that the sort operation was carried out multiple times in
some programs, and that the relevant code had been pretty much duplicated
verbatim. WHat's more, the actual command turned out to be _wrong_ in all
instances (PB). This is a prime candidate for refactoring, but time constraints
dictate that the refactoring will wait for the next release. The correction,
however, is to be done now. So after making sure the correction worked for
`chippart`, I propagated it to the other programs.

The version I'm going to propagate is PB's, as contained in `chippart_test.c`
(which he sent in December 2023). His version is simpler than mine, and does not
rely on an external function, but only checks for a zero exit status while mine
does it by the book (see `man 3 system` and `man 2 waitpid`). I therfore made a
patch from the two files:

```bash
$ diff chippart.c chippart_test.c > fix.patch
# keep only the relevant hunks
$ patch chippart.c fix.patch
```

Now `chippart.c` has PB's fix rather than mine. Time for tests... ok (I had to
adjust the expected output files since PB's and my own outputs differ; I also
needed to expect an exit code larger than 0 since this is what PB's version
returns - but at the end of the day, all 136 tests pass).

Now, after finding the latest commit containing only GA's original code, I make
a GA -> PB patch:

```bash
$ git difftool -y -x 'diff' fd03d263961246 chippart.c > fix-GA2PB.patch
# edit to keep only relevant hunk
```

Note that I used `git difftool` instead of `git diff` (for which it is a
front-end) so that I could produce "normal" instead of "unified" diffs - the
hunk failure rate (see below) is much lower this way.

And now let's patch those C sources:

```bash
$ patch chipcor.c fix-GA2PB.patch
patching file chipcor.c
Hunk #1 succeeded at 339 (offset -5 lines).
Hunk #2 succeeded at 345 (offset -5 lines).
Hunk #3 succeeded at 346 (offset -5 lines).
Hunk #4 succeeded at 347 (offset -5 lines).
Hunk #5 FAILED at 355.
1 out of 5 hunks FAILED -- saving rejects to file chipcor.c.rej
```

Uh-oh - for some reason it couldn't find the last hunk, so I did it by hand. The
same happened for `chippeak.c` and `chipscore.c`. For `chipextract.c` and its
many occurrences of the patchend^[to-be-patched, as in "dividend"] code, I makde
another patch because the above one wasn't recognized at all. This I did by
extracting the relevant lines from `chippart.c` and one of the occurrences in
`chipextract.c` (as `ce.c` and `cp.c`, respectively):

```bash
$ diff ce.c cp.c > fix-chipextract.patch
```

Now, ideally I want to apply this patch to every instance of the duplicated code
I want to fic (that is, all eight of them). Problem is, I don't know how to tell
`patch` to keep patching:

```bash
$ patch --verbose chipextract.c fix-chipextract.patch 
Hmm...  Looks like a normal diff to me...
patching file chipextract.c
Using Plan A...
Hunk #1 succeeded at 482 (offset 478 lines).
Hunk #2 succeeded at 488 (offset 478 lines).
Hunk #3 succeeded at 489 (offset 478 lines).
Hunk #4 succeeded at 490 (offset 478 lines).
Hunk #5 succeeded at 493 (offset 478 lines).
done
# Carry on, please!
```

So I ended up doing `patch --verbose chipextract.c fix-chipextract.patch` until
`patch` reported an error. At each iteration I first ran with `--verbose`, and
there was never a report of a missed hunk. That's a relief. Compiled seamlessly
and all tests pass. **NOTE** however that there is only one test for
`chipextract`, and that the behaviour on unsorted files is only ever tested on
`chippart`. 

In fact, PB (2024-01-04) said that we don't need to test this as a formal test;
instead we'll check it once by manually veryifying that each program croaks when
passed `-d` on an unsorted SGA:

```zsh
$ ./chipcor -d -A "DNaseI-HS +" -B "DNaseI-HS -" -b-1000 -e1000 -w 10 ../data/STAT1_stim_small-rev.sga
Processing file ../data/STAT1_stim_small-rev.sga
 Arguments:
 Feature A (ref): DNaseI-HS +
 Feature B (tar): DNaseI-HS -
 Range : [-1000, 1000]
 Sliding Window : 10
 Cut-off : 1
 Normalization type : 0

 Ref feature : DNaseI-HS + (R)
 Tar Feature: DNaseI-HS - (T)
 Autocorrelation OFF
 xb  -5, xe 4, xc 0
 l5_p: -99  l3_p: 99
 New range: [-995, 994]

sort: ../data/STAT1_stim_small-rev.sga:2: disorder: NC_000022.10	STAT1	51234732	+	1
system command sort -s -c -k1,1 -k3,3n ../data/STAT1_stim_small-rev.sga : return code 256
sorting command command failed

$ ./chipextract -d -A "STAT1 o" -B "READS" -b -1000 -e 1000 -w 20 -c 10 ../data/STAT1_stim_small-rev.sga > /dev/null
Processing file ../data/STAT1_stim_small-rev.sga
 Arguments:
 Feature A (ref): STAT1 o
 Feature B (tar): READS
 Range : [-1000, 1000]
 Sliding Window : 20
 Cut-off : 10
 Ref feature : STAT1 o (R)
 Tar Feature: READS  (T)
 xb  -10, xc 9, xe 0
 l5_p: -49  l3_p: 49
New range: -990 - 989
sort: ../data/STAT1_stim_small-rev.sga:2: disorder: NC_000022.10	STAT1	51234732	+	1
system command sort -s -c -k1,1 -k3,3n ../data/STAT1_stim_small-rev.sga : return code 256
sorting command command failed
Total Sequence Length: 0

$ ./chippart -d -s0.06 -p-10 ../data/STAT1_stim_small-rev.sga >/dev/null
Processing file ../data/STAT1_stim_small-rev.sga
 Arguments:
 Selected Feature : (null)
 Density Threshold : 0.060000

 Transition Penalty : -10

 Count Cut-off : 1

Feature Specs: ALL -> Process all features
sort: ../data/STAT1_stim_small-rev.sga:2: disorder: NC_000022.10	STAT1	51234732	+	1
system command sort -s -c -k1,1 -k3,3n ../data/STAT1_stim_small-rev.sga : return code 256
sorting command command failed


$ ./chippeak -i ../data -f "STAT1" -t 25 -w 300 -v 300 -r ../data/STAT1_stim_small-rev.sga >/dev/null
Processing file ../data/STAT1_stim_small-rev.sga
 Arguments:
 Selected Feature : STAT1
 Integration range (Window) : 300

 Minimal distance (Vicinity) : 300

 Peak Threshold : 25

Feature Specs: Feature name : STAT1
 SIZE Hash table: NC_000001.9 (len = 12) -> 247249719 (len = 10)
 ...
 SIZE Hash table: NC_024468.1 (len = 12) -> 149627545 (len = 10)
 HASH Table for chromosome size initialized
sort: ../data/STAT1_stim_small-rev.sga:2: disorder: NC_000022.10	STAT1	51234732	+	1
system command sort -s -c -k1,1 -k3,3n ../data/STAT1_stim_small-rev.sga : return code 256
sorting command command failed

$ ./chipscore -d -A "STAT1_p" -B "STAT1" -b -150 -e 150 -t 15 -c 1 ../data/STAT1_stim_small-rev.sga
Processing file ../data/STAT1_stim_small-rev.sga
 Arguments:
 Feature A (ref): STAT1_p
 Feature B (tar): STAT1
 Range : [-150, 150]
 Cut-off : 1
 Output Threshold : 15

 Feature B tag count report : 0 (1:ON, 0:OFF)
 Ref feature : STAT1_p  (R)
 Tar Feature: STAT1  (T)
sort: ../data/STAT1_stim_small-rev.sga:2: disorder: NC_000022.10	STAT1	51234732	+	1
system command sort -s -c -k1,1 -k3,3n ../data/STAT1_stim_small-rev.sga : return code 256
sorting command command failed
```

All ok. For the record, this is commit `a11e023d1af51a8b88d1d651f2066e6cdb26748f`.

for the _preferred_ characters, I suggest adopting the same convention as BED
  does for its `chrom` field (**not** `name`, which allows whitespace), but
  perhaps without the length cap: essentially `[[:alnum:]_]+`. On top of that,
  we could _allow_ punctuation signs like `:_$§@#%&/(){}.`

Harmonizing Option Semantics
============================

**NOTE**: the changes shown here have been reviewed y PB in a separate document
(`../src/option-status_PB-TJ.txt`) which is the authoritative one.

Let's make sure that options mean the same thing across our programs. Ideally,
we want the following:
* a given behaviour (say the specification of the database path) works the same
  way and is passed by the same option
* A given option has the same meaning (that is, causes the same behaviour)
  across programs.

Of course, some behaviours are not found everywhere, so relevant options may not
be found everywhere; if possible though different behaviours should be elicited
by different options.

When I say "works in the same way", I mean this quite literally: for example
specifying the location of a file should should **NOT** depend on whether the
argument contains a trailing slash or not (see @sec:chr_replace_sga above for a
counterexample).

Consistency should also be kept across option forms: If there is a long-form
option in one program, there should be the same long form in all programs that
have the short form (which of course should be the same), etc.

Essentially, we want to follow the Principle of Least Surprise (POLS).

I made a list of all options in the C and Perl code:

```bash
$ for e in *.c perl/*.pl; do grep -E '".*\\t\\t.*-[[:alpha:]]\>.*"$' < "$e"; done \
  | sed 's/^ \+//' > options-list
```

This file was later reworked by hand into TSV-like form.

`-A`
----

Used to specify one of the features (feature A) when there are two. Consistent;
no change required IMHO (though some use `-a` instead of `-A`).


`-a` 
---

Used for assembly (`sga2wigSmooth_FS.pl`) or feature (`eland2sga.pl`, but
**not** `gff2sga.pl` -- it appears in a comment but not in the code); also min
BED score (`bed2bed_display`).

Feature is usually specified with `-f` (or `-A` and `-B` when there are two), so
we might want to change this. The thing is, `-f` is used by `eland2sga.pl` to
specify the eland file. We might use `-e`, or use a positional parameter.
Minimum might be specified with `-m`.

`-B`
----

Used to specify one of the features (feature B) when there are two. Consistent;
no change required IMHO.

`-b`
----

Used to specify beginnigs, of the chromosome or the 5'-end extension.
Semantically consistent, so no change is required IMHO.

`-c`
----

Mostly for count cutoff, but also for centering (`bed2sga`, `gff2sga`) or as a
normalisation factor (`sga2bed`). Maybe try `-C` for centering and `-n/-N` for
normalisation. Also, long option not always present.

`--color`
---------

Used in only one program, to specify the colour of the annotation track. No
change required.

`-d`
----

Among the C programs (based on `grep`ping the C source (help section)), `-d`
always seems to cause some form of debug behaviour, though in some cases this
involves checking the syntax of the input SGA file (e.g. `chipcor), while in
others it doesn't -- even though it does input SGA (e.g. `chipcenter`).
(**NOTE** Again, this is based on the help, not on the program logic!)

Only four programs (namely, `bed2bed_display`, `bed2sga`, `sga2bed`, and
`sga2wig`) seem to accept a long form (`--debug`).

Among the Perl scripts, `-d` is used for specifying the database
(`chr_replace_sga.pl`) or to specify the description of the BED header
(`partit2bed.pl`). There is no hint of any debugging behaviour, or of a
`--debug` option.

`--db`
------

Used as a long form of `-i` by all C programs except `chipcenter` and
`chippeak`. Used as apparently the sole form of specifying the path to
`chro_idx.nstorage` by `gff2sga.pl`, `partit2bed.pl`, `partit2sga.pl`,
`sga2fps.pl`, `sga2gff.pl`, and `wigVS2sga.pl`.

`-e`
----

Used mostly to specify a chromosome or region end (see also `-b`), but also to
specify extented SGA format or to specify the semantics of SGA fields. Maybe
`-E` (or `-x`, as in `fps2sga.pl`) could stand for extended format. The SGA
"feature" field, now specified to `sga2gff` by `-e`, could be specified by `-f`
since this option is unused.

`-f`
----

Mostly used to specify the feature (when there is only one, otherwise it's `-A`.
and `-B`).  Many of the programs that use `-f` in this sense, but not all,
accept the long form `--feature`. Also frequently used to specify an input file.
Other uses are: length of the 3' (= fwd) extension (`sga2bed`), normalization
factor (`sga2wig`), and fragment size (`sga2wigSmooth_FS.pl`).

I would argue that we should keep `-f` for feature. Mandatory files can be
passed as a positional argument or on stdin; optional ones (or mandatory ones as
well, if we really want) can be passed by an option derived from the format, e.g.
`-G` for GFF, `-S` for SGA, `-E` for Eland, etc.

The normalization factor could be passed by `-n`, the fragment size by `-s`.

`--feature`
----------

See `-f`.

`--format`
----------

Only used by `sga2wig`, to specify format (short form is `-o`, not `-f`).

`-h`, `--help`
--------------

Used consistently to provide help, though not all programs accept the long form.
No other option used to show help.

We may want to ensure that the long form is accepted by all programs.

`-i`
----

Among the C programs, the following accept option `-i` to specify the path to
the database: `bed2sga, `chipcenter, `chippeak, `sga2bed, and `sga2wig. Of
these, `chipcenter` and `chippeak` do not admit the long form `--db`. The
argument placeholder is inconsistently `<path>` or `<dir>`.

Among the Perl scripts, only `eland2sga.pl` accepts `-i`, seemigly for the same
purpose (though the placeholder is `db-dir`).

`--ignore0flag`
---------------

Singleton (`fps2sga.pl`).

`-l`
----

Used to specify lengths (of reads or tags). Use is consistent, nothing to do
IMHO.

`-n`
----

Use is divided roughly equally between normalisation and chromosome number.
Since `-N` is not used, we might want to introduce it.

`-o`
----

Used to select an output format or to specify that a strand is oriented. We
could introduce `-O` for one and keep `-o` for the other.

`--oformat`
-----------

Long form of `-o` for `bed2bed_display` (note that `sga2wig` uses `--format`,
not `--oformat`: we may want to harmonize).

`-p`
----

Transition penalty, used only by `chippart`.

`-q`
----

Report feature B tag counts as `feature_name=<int>`, used only by `chipscore`.

`-r`
----

This is the most polysemic of all options, and no two programs use it with the
same meaning:

Program         Meaning of `-r`
--------------- --------------------------
`bed2sga`		    Generate a 2-line[+/-] SGA file representing BED regions
`chipcenter`		New feature name (for feature replacement)
`chippeak`		  Refine Peak Positions
`chipscore`		  Reverse extraction process
`filter_counts`	Retain Mode on
`sga2bed`		    BED format without annotation track header lines

`-s`
----

Mostly assembly ("species"), but also shift, density (?), and span. We might be
able to use `-a` instead for assembly; but `-d` cannot be used for density as it
is used for "debug". Maybe `-p`, which looks like "rho", a frequent symbol for
density?.

`--span`
--------

Long form of `-s` in `sga2wig`, but not recognized by `sga2wigSmooth_FS.pl` (in
which `-s` has the same meaning).

`--species`
-----------

Long form of `-s`, but not recognized by all programs which take `-s` as
"species".

`-t`
---

For threshold or track name (2 each). I'm wondering if "threshold" is different
from "cutoff"; if not we might be able to reuse `-c`.

`-u`
----

Unoriented (referring to the generated SGA file). Consistent, no changes needed.

`-v`
----

Vicinity. Singleton, used by `chippeak`.

`-w`
----

Means "width" in `chipextract` and "window" in `chippeak`. The meanings are
similar and there is no clash with other options, so we needn't change anything
here, IMO.

`-x`
----

Means either "expand" SGA into multiple GFF or BED lines, or produce "extended"
GFF. The meanings are similar and do not conflict with others; I'd say keep as
is. Possibly change `-e` to `-x` when `-e` means "extend".

`-z`
----

Set strand to zero. Only used by `chipcenter`.

Master Help Files
=================

Branch `help`. See `./src/help`. Preliminary attempts using the C preprocessor
are encouraging, except that many of the help functions in the the C and Perl
code accept parameters, so that the generated help should be C (resp. Perl) code
that makes use of these parameters. Look for example at `chippeak.c`: its
`show_help()` function passes several params to  `fprintf`: `progname`,
`VERSION`, `Coff`, and `Thres`; these must not be hard-coded in the help master:
instead, the generated help, in its C form, must include placeholders for
`fprintf()`:  "%s", etc.


`make gunzip-data`
==================

PB reported that doing `make gunzip-data` doesn't work on his mac. I logged into
a machine running MacOS (Darwin imac-de-user-1.home 17.7.0 Darwin Kernel Version
17.7.0: Fri Oct 30 13:34:27 PDT 2020; root:xnu-4570.71.82.8~1/RELEASE_X86_64
x86_64) and after some debugging found that the variable `GUNZIPPED` in the
Makefile (l. 87) had wrong values. It's supposed to hold a list of
_uncompressed_ filenames, but it has the compressed versions. This does NOT
happen on my Linux box:

```bash
# Darwin
$ make debug
GUNZIPPED: CTCF.sga.gz ES.K4.sga.gz ES_CTCF_peaks.sga.gz Mm_EPDnew_001_mm9.sga.gz
# Linux
$ make debug
GUNZIPPED: CTCF.sga ES_CTCF_peaks.sga ES.K4.sga Mm_EPDnew_001_mm9.sga
```

The incriminated line is:

```makefile
GUNZIPPED = $(shell echo *.sga.gz | sed 's/\.gz\>//g')
```

What doesn't work seems to be the `\>` anchor: this forces the previous pattern
to match only at the end of words (similar to `$` at the end of lines). I had
intended this to ensure that we only matched the end of filenames. But the MacOS
version of Sed doesn't know about this, and accordingly fails to strip the `.gz`
bit (note that I can't use `basename` because it would also strip `.sga`). So I
chose the least common denominator and removed the anchor. Seems to work fine
now. Pushed this onto master, commit `ac19d073b901ad13...`.

Adding Descriptions of Aux Tools
================================

Started adding descriptions of the auxiliary tools to the manual. These have the
same format as for the main `chip*` suite, but they are in Section 4. **NOTE** I
wasn't able to obtain a definitive answer regarding the _order_ in which these
tools should be described (alphabetic? thematic? other?), so for now I write
descriptions in separate Markdown files named after the tools (e.g.,
`filter_counts.md`) which I then include into the Markdown using m4. If the
order is changed, I just need to change the `sinclude()` calls. This is of
course temporary: as soon as the order is fixed, I'll drop that extra stage.

The descriptions themselves are based on the help messages.

PB's Spring 2024 Comments on the UG
==================================

On 2024-04-09, PB sent two docs: (i) a marked version of the User Guide
(`ChIP-Seq_Tools-UsersGuide_PB_marked_240409.pdf`, and
(ii) a document with comments on required changes.
 (`ChIP-Seq_Tools-UsersGuide_PB_comments_240409.pdf`).

See `ChIP-Seq_Tools-UsersGuide_PB_comments_240409-TJ-log.*` for the actions I
took.

Further comments
----------------

### BED Tools Problem

In early May (03, 08 or thereabouts) PB sent further material to be integrated
into the UG, which I did (for example a `README` file with examples of
conversions, which I put under subsection 4.4 (bed2sga) due to the heavy usage
of this program. To check the examples I installed the BED tools. At some point
the compilation failed with a warning about a missing header file (`cstdint.h`),
and a suggestion for a fix, which I implemented. Things now seem to work ok.

### Running the examples

The examples already in the UG are meant to be run from `./doc` (UG, p. 6
("NOTES")). I see no reason to change this for the new set of examples, so I
will assume we're in the same place. This means changing the commands somewhat.

Also, `bamToBed` seems to have a glitch:

```bash
$ bamToBed -i GSM2199199_5-6hr_GRH_mapped_dm6.bam > /dev/null
[W::bam_hdr_read] EOF marker is absent. The input is probably truncated
[E::bgzf_read] Read block operation failed with error 4 after 0 of 4 bytes
```

This may be due to a truncated file (PB, 2024-05-17 TC).



Container Probs
============

While adding Docker examples to the UG, as well as when testing the new
incarnation of the Docker examples, I hit a snag:

```bash
$ ./test_main.sh -p '*chipcor*' -e docker    
WARNING: ./exp has writeable files
docker: Error response from daemon: invalid mount config for type "bind": stat /home/tjunier/projects/chipseq/data: permission denied.
See 'docker run --help'.
```

The actual Docker command within the above test is:

```bash
# see https://docs.docker.com/storage/bind-mounts/ for the semantics of --mount
$ docker run  --mount \
'type=bind,source=/home/tjunier/projects/chipseq/data,target=/home/chipseq/data' \
sibswiss/chipseq:latest \
chipcor -A 'CTCF +' -B 'CTCF -' -b -1000 -e 1000 -w 1 -c 1 -n 1 ./data/CTCF.sga
```

This is strange because `stat` doesn't seem to have any problems with that file:

```bash
$ stat /home/tjunier/projects/chipseq/data
  File: /home/tjunier/projects/chipseq/data
  Size: 12288     	Blocks: 24         IO Block: 4096   directory
Device: 0,65	Inode: 9593063     Links: 6
Access: (0755/drwxr-xr-x)  Uid: ( 1002/ tjunier)   Gid: ( 1002/ tjunier)
Access: 2024-04-17 15:51:40.331537982 +0200
Modify: 2024-04-26 09:06:29.485877072 +0200
Change: 2024-04-26 09:06:29.485877072 +0200
 Birth: -
```

Furthermore, all of its ancestors have the necessary permissions:

```bash
# This script just does ls -l on a file and all its ancestors.
$ ~/bin/ckpath.sh /home/tjunier/projects/chipseq/data
drwxr-xr-x /home/tjunier/projects/chipseq/data
drwxr-xr-x /home/tjunier/projects/chipseq
drwxrwxr-x /home/tjunier/projects
drwxr-xr-x /home/tjunier
drwxr-xr-x /home
```

Let's see exactly where permissions are wanting.

```bash
$ strace -Z docker run --mount type=bind,source=$CHIPSEQ_DATA_DIR,target=/home/chipseq/data sibswiss/chipseq:latest 9chipcor -A "CTCF +" -B "CTCF -" -b -1000 -e 1000 -w 1 -c 1 -n 1  ./data/CTCF.sga
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=2346005, si_uid=1002} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=2346005, si_uid=1002} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=2346005, si_uid=1002} ---
futex(0x648868ad82e0, FUTEX_WAIT_PRIVATE, 0, NULL) = -1 EAGAIN (Resource temporarily unavailable)
epoll_ctl(4, EPOLL_CTL_ADD, 3, {events=EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, data={u32=2389704705, u64=9210121652819460097}}) = -1 EPERM (Operation not permitted)
futex(0x648868ad82e0, FUTEX_WAIT_PRIVATE, 0, NULL) = -1 EAGAIN (Resource temporarily unavailable)
newfstatat(AT_FDCWD, "/usr/lib/libykcs11.so", 0xc0000c21d8, 0) = -1 ENOENT (No such file or directory)
newfstatat(AT_FDCWD, "/usr/lib/libykcs11.so.1", 0xc0000c22a8, 0) = -1 ENOENT (No such file or directory)
newfstatat(AT_FDCWD, "/usr/lib64/libykcs11.so", 0xc0000c2378, 0) = -1 ENOENT (No such file or directory)
newfstatat(AT_FDCWD, "/usr/lib64/libykcs11.so.1", 0xc0000c2448, 0) = -1 ENOENT (No such file or directory)
newfstatat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/libykcs11.so", 0xc0000c2518, 0) = -1 ENOENT (No such file or directory)
newfstatat(AT_FDCWD, "/usr/local/lib/libykcs11.so", 0xc0000c25e8, 0) = -1 ENOENT (No such file or directory)
docker: Error response from daemon: invalid mount config for type "bind": stat /home/tjunier/projects/chipseq/data: permission denied.
See 'docker run --help'.
futex(0x648868ad82e0, FUTEX_WAIT_PRIVATE, 0, NULL) = -1 EAGAIN (Resource temporarily unavailable)
+++ exited with 126 +++
```

I don't know about those unfound libraries, but for now let's focus on the
response from the daemon. This is the same message as above, and (should have
seen it) it comes from the Docker daemon, so running `strace` on the client will
not be of much help. So, let's try to attach `strace` onto the daemon process,
shall we?

```bash
$ pstree -p | grep docker
           |-dockerd(1014)-+-{dockerd}(1015)
           |               |-{dockerd}(1016)
           |               |-{dockerd}(1017)
           |               |-{dockerd}(1018)
           |               |-{dockerd}(1019)
           |               |-{dockerd}(1020)
           |               |-{dockerd}(1021)
           |               |-{dockerd}(1022)
           |               |-{dockerd}(1023)
           |               |-{dockerd}(1024)
           |               |-{dockerd}(1025)
           |               |-{dockerd}(1026)
           |               |-{dockerd}(1027)
           |               |-{dockerd}(1028)
           |               |-{dockerd}(1041)
           |               |-{dockerd}(1042)
           |               |-{dockerd}(1043)
           |               |-{dockerd}(1122)
           |               |-{dockerd}(1232)
           |               |-{dockerd}(1658755)
           |               `-{dockerd}(1658756)
```

Hm. As I feared, there are many `dockerd` processes, and it's not clear which
one we'll have to rty to latch onto. let's try the parent process (PID 1014)
first:

```bash
# as root!
strace --attach 1014 -Z 
strace: Process 1014 attached
# at this point, I run the docker command (without strace...) in another tty
^Cstrace: Process 1014 detached
```

### Volume Mounts

Let's see if using a Docker volume (instead of a bind mount) helps in any way
(volumes seemed to be the preferred way anyway). 

```bash
$ docker run --rm -v /home/tjunier/projects/chipseq/data:/data  sibswiss/chipseq:latest ls -l /
docker: Error response from daemon: error while creating mount source path '/home/tjunier/projects/chipseq/data': mkdir /home/tjunier/projects/chipseq: file exists.
```

Hm, the order must be wrong, but when I try the opposite order it also fails:

```bash
$ docker run --rm -v /data:/home/tjunier/projects/chipseq/data  sibswiss/chipseq:latest ls -l /data
ls: cannot access '/data': No such file or directory
```

The problem is that it's not clear if this is te hsame problem as with bind
mounts, or a different one.

**SOLVED** The problem is that my chipseq dir is encrypted (GoCryptFS). For some
reason, even when decrypted, Docker chokes on the permissions.

### Gunzipping Data

There is also the problem that the compressed files in
`/usr/local/share/chipseq/data` cannot be gunzipped for want of permissions.


Locale
======

From the mail PB sent on 2024-07-25:

> ChIP-seq User Guide:
> 
> We should say at some place that the sort command should be executed in the following environment settings:
> 
> export LANG=C ; sort ..
> 

Done - see Chapter 3, p. 6.

`#define`s
==========

From the same mail:

> Format of genome information files:
> 
> We should inform the user that the maximal size of the sequence name and
> sequence accession is 18 characters . This concerns chr_NC_gi and chr_size.
> 

Done - see Appendix C, scts 1,2. (`cb4a23a353dd10e` on branch `PBdocfixes`).

> I would however propose to increase the length of these fields to at least 32
> characters, see code lines:
> 
> #define CHR_NB 18
> 
> #define AC_MAX 18

Done - all tests ok (but on a new branch, `32chars` - I'll rebase it onto
`master` as soon as I get the green light from PB.

Long Names in Genome Information Files
==========================

From PB's 2014-07-31 mail:

> Following up on our discussion, I have generated test genome information files
> with long sequence names, see attachement.
> 
> The untared directory is to be used with option -i hg38_test.
> 
> This is for selected tests with SGA files that contain reference to sequences
> from hg38 only, to make sure that programs can process genome information files
> with long sequence names properly. Errors could potentially occur when the
> genome information files are read on input, or later, when information for
> specific sequences is retrieved from the hash tables. Only a few tests are
> needed.
> 
> After modification of the source code, please also run the standard tests with
> the standard genome information files, to be sure that no memory related side
> effects occur.

Ok, so I untared the archive in `./data`. Then I set out to carry out a few
tests (no need for many, see PB's mail above). This is all in branch `32chars`.

First, I replace the genome information files with their test versions:

```bash
# in ./data
$ mv hg38_test/* .
```

Then I make sure I have the latest versions of the binaries:

```bash
$ cd ../src && make clean && make -j
```

And I run the tests --- yes, all of them: it's actually _faster_ to run them all
than to decide which should be run. The results are, shall we say, suboptimal:

```bash
...
Running test_gff2sga_xenTro3_chr_gff...             ok
Running test_gff2sga_-s_xenTro3_xenTro3_chr_gff...  Chromosome GL172637 without sequence accession, line 2
failure
Running test_gff2sga_xenTro3_chr2_gff...            ok
Running test_gff2sga_-s_xenTro3_xenTro3_chr2_gff... failure

86 successes
50 failures
9 errors
```

Oh. I suppose merely `#define`ing things away wasn't _quite_ enough.

### Excessively long chromosome numbers

Part of the problem was that two lines in `chr_NC_gi` contained chromosome
numbers (field #1) that were 34 chars long, and were thus rejected (the limit
being 32).

### Unknown Chromosome

The following command (part of `test_bed2bed_display_ARG1`) fails to produce any
output, but does output plenty or errors:

```bash
# Folded STDERR for readability
../chipcenter -i ../../data -f STAT1 -s 75 ../../data/STAT1_stim_small.sga
WARNING: TAG: NC_000021.8	STAT1	10698350	-	1	 goes beyond chromosome boundaries
(pos after TAG Shift=10698275, chrom size = 0)
```

Notice `chrom size = 0`. This is because the code fails to obtain the size of
the `NC_000021.8` chromosome (hash lookup, l. 630); this in turn is caused by
the fact that the two chromosomes in the input file `STAT1_stim_small.sga`,
namely `NC_000021.8` and `NC_000022.10`, are not found in the new `chr_size`
(`grep`...), hence are not added when the `size_table` hash is constructed (l.
210). For some reason, though, we do find `NC_000022.11` and `NC_000021.9`.

```c
# chipcenter.c
627             /* Get Chromosome size */
628             ac_len = (int) strlen(seq_id) + 1;
629             //printf ("Chr ID: SeqID %s, len %d\n", seq_id, ac_len);
630             csize = hash_table_lookup(size_table, seq_id, (size_t) ac_len);
631             //printf ("Chr SIZE: csize %s\n", csize);
632             if (csize != NULL) {
633                 chr_size = (int) atoi(csize);
634             } else {
635                 chr_size = 0;
```

This means that practically _any_ `chipcenter` run with `STAT1_stim_small.sga`
as input is likely to fail, as is any other program that needs chromosome sizes
from that SGA file and looks them up in `chr_size`.

Mailed PB about this: he replied that I shoud try increasing the abovementioned
constants (as well as `SEQ_ID`) to 64, which I did thusly:

```bash
$ sed -i -E '/#define (AC_MAX|CHR_NB|SEQ_ID)/s/32/64/' *.[ch]
```

There were compilation warnings:

```bash
bed2bed_display.c: In function ‘process_bed’:
bed2bed_display.c:179:27: warning: array subscript 64 is above array bounds of ‘char[64]’ [-Warray-bounds=]
  179 |                 bed_fld[0][i] = 0;
      |                 ~~~~~~~~~~^~~
bed2bed_display.c:146:14: note: while referencing ‘bed_fld’
  146 |         char bed_fld[12][FIELD_MAX];
      |              ^~~~~~~
...
bed2sga.c: In function ‘process_bed’:
bed2sga.c:359:27: warning: array subscript 64 is above array bounds of ‘char[64]’ [-Warray-bounds=]
  359 |                 bed_fld[0][i] = 0;
      |                 ~~~~~~~~~~^~~
bed2sga.c:319:14: note: while referencing ‘bed_fld’
  319 |         char bed_fld[12][FIELD_MAX];
      |              ^~~~~~~
```

But it seems that the problem lines are precisely meant to catch fields that
exceed the max length (that GCC was able to detect this is rather impressive,
BTW). So no obvious need to fix anything.

PB further noted that supplying `-a` to `bed2sga` sometimes confuses it. Consider:
  ```
  $ ../sga2bed -i ../../data tmp.sga | ../bed2sga -i ../../data
  chrHSCHR19KIR_FH05_B_HAP_CTG3_1 chipseq 1000    +       1
  chrHSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1   chipseq 1000    +       1
  chrHSCHR19KIR_ABC08_AB_HAP_T_P_CTG3_1   chipseq 1000    +       1
  $ ../sga2bed -i ../../data tmp.sga | ../bed2sga -i ../../data -a hg38
  NT_187675.1	chipseq1	1000	+	1
```

where `tmp.sga` is:

```bash
NT_187675.1	chipseq	1000	+	1
NT_187672.1	chipseq 1000	+	1
NT_187673.1	chipseq 1000	+	1
```

Moreover, some hash keys appear truncated: 

```
$ grep NT_187672.1 < bed2sga.out
 AC Hash table: chrHSCHR (len = 9) -> NT_187672.1 (len = 12)
 AC Hash table: NT_187672.1 (len = 12) -> NT_187672.1 (len = 12)
```

where `bed2sga.out` is the combined STDERR and STDIN of the above command, but
in debug mode (`-d):

```bash
$ ./sga2bed -i ../data tmp.sga | ./bed2sga -i ../data -a hg38  -d 
```

Note that the hash is only constructed when `-a` is passed.

The probolem is that `chrHSCHR` is only a prefix of the real key, yet it is the
prefix, not the whole key, that is being added to the hash. This may or may not
be the source of the problem, but in any case it's not the intended behaviour.

### Steps taken

First, let's see what the real key is:

```bash
$ grep NT_187672.1< ../data/chr_NC_gi
HSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1	NT_187672.1
```

Note that the hash key is prefixed with `chr`, which is probably meant for
numbered chromosomes. So let's see what is actually entered in the hash.

Now the code that adds the kv pair to the hash is:

```c
                // normally this is commented out
  1             fprintf(stderr, 
278                             "adding key %s (len = %d) value %s (ac) (len = %d) to hash table\n", chrom, nb_len, ncbi_ac, ac_len);
  1             /* Store both chromosome number to NCBI identifier and NCBI identifier to NCBI identifier keys */
  2             hash_table_add(ac_table, chrom, (size_t) nb_len, ncbi_ac,
  3                            (size_t) ac_len);

```

which result sin:

```bash
...
adding key chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1 (len = 34) value NT_187671.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1 (len = 34) -> NCBI AC: NT_187671.1 (len = 12)
 AC Hash table: NCBI AC: NT_187671.1 (len = 12) -> AC: NT_187671.1 (len = 12)
adding key chrHSCHR (len = 9) value NT_187672.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR (len = 9) -> NCBI AC: NT_187672.1 (len = 12)
 AC Hash table: NCBI AC: NT_187672.1 (len = 12) -> AC: NT_187672.1 (len = 12)
adding key chrHSCHR (len = 9) value NT_187673.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR (len = 9) -> NCBI AC: NT_187673.1 (len = 12)
 AC Hash table: NCBI AC: NT_187673.1 (len = 12) -> AC: NT_187673.1 (len = 12)
...
```

So:

1. Our keys are truncated _before_ they are passed to `hash_table_add()`,
   otherwise `fprintf()` would display them intact;
1. Not _all_ keys are turncated, it would seem -
   `chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1` seems complete enough.

To facilitate debugging, I made a short version of `chr_NC_gi` which I placed in
`../data/chr_NC_gi-short.d`. It has the two known failing cases, plus two others
that work fine. I can then use `-i` to specify that file:

```bash
$ ./sga2bed -i ../data tmp.sga | ./bed2sga -i ../data/chr_NC_gi-short.d -a hg38 -d
 Arguments:
 Feature : chIP
 Species : hg38
 Centered SGA [on/off]: 0
 Unoriented SGA [on/off]: 0
 Extended SGA [on/off]: 0. BED fields:
 Use Score [on/off]: 0
 NarrowPeak format [on/off]: 0
 Use Signal Value [on/off]: 0
adding key chrhg38 (len = 8) value  (ac) (len = 1) to hash table
 AC Hash table: chrom: chrhg38 (len = 8) -> NCBI AC:  (len = 1)
 AC Hash table: NCBI AC:  (len = 1) -> AC:  (len = 1)
adding key chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1 (len = 34) value NT_187671.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1 (len = 34) -> NCBI AC: NT_187671.1 (len = 12)
 AC Hash table: NCBI AC: NT_187671.1 (len = 12) -> AC: NT_187671.1 (len = 12)
adding key chrHSCHR (len = 9) value NT_187672.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR (len = 9) -> NCBI AC: NT_187672.1 (len = 12)
 AC Hash table: NCBI AC: NT_187672.1 (len = 12) -> AC: NT_187672.1 (len = 12)
adding key chrHSCHR (len = 9) value NT_187673.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR (len = 9) -> NCBI AC: NT_187672.1 (len = 12)
 AC Hash table: NCBI AC: NT_187673.1 (len = 12) -> AC: NT_187673.1 (len = 12)
adding key chrHSCHR19KIR_FH05_A_HAP_CTG3_1 (len = 32) value NT_187674.1 (ac) (len = 12) to hash table
 AC Hash table: chrom: chrHSCHR19KIR_FH05_A_HAP_CTG3_1 (len = 32) -> NCBI AC: NT_187674.1 (len = 12)
 AC Hash table: NCBI AC: NT_187674.1 (len = 12) -> AC: NT_187674.1 (len = 12)
 HASH Table for chromosome access identifier initialized
 Processing BED file (null)
```

This is much shorter than with the whole file, and should allow for shorter
debug cycles. We note, inpassing, that a strange key-value pair consisting of
`chrhg38` as the key and empty as the value is added at the beginning. I don't
know if this is intended. The problem variable is called `chrom`, and is set in
a series of calls after line 270: 

```c
269             pmatch = match(chr_nb, "scaff"); 
270             if (pmatch == -1) { 
271                 fprintf(stderr, "\nchrom before strcpy():, l. 272 %s\n", chrom);
272                 strcpy(chrom, "chr");
273                 fprintf(stderr, "chrom after strcpy():, l. 272  %s\n", chrom);
274                 strcat(chrom, chr_nb);
275                 fprintf(stderr, "chrom after strcat():, l. 274  %s\n", chrom);
276             } else {
277                 strcpy(chrom, chr_nb);
278             }
```

This yields:

```bash
chrom before strcpy():
chrom after strcpy():  chr
chrom after strcat():  chrhg38
adding key chrhg38 (len = 8) value  (ac) (len = 1) to hash table

chrom before strcpy(): chrhg38
chrom after strcpy():  chr
chrom after strcat():  chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1
adding key chrHSCHR19KIR_ABC08_A1_HAP_CTG3_1 (len = 34) value NT_187671.1 (ac) (len = 12) to hash table

chrom before strcpy(): chrHSCHR19KIR_ABC08_A1_HAP_CTG3_HSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1
chrom after strcpy():  chr
chrom after strcat():  chrHSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1
adding key chrHSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1 (len = 38) value NT_187672.1 (ac) (len = 12) to hash table

chrom before strcpy(): chrHSCHR19KIR_ABC08_AB_HAP_C_P_CHSCHR19KIR_ABC08_AB_HAP_T_P_CTG3_1
chrom after strcpy():  chr
chrom after strcat():  chrHSCHR19KIR_ABC08_AB_HAP_T_P_CTG3_1
adding key chrHSCHR19KIR_ABC08_AB_HAP_T_P_CTG3_1 (len = 38) value NT_187673.1 (ac) (len = 12) to hash table

chrom before strcpy(): chrHSCHR19KIR_ABC08_AB_HAP_T_P_CHSCHR19KIR_FH05_A_HAP_CTG3_1
chrom after strcpy():  chr
chrom after strcat():  chrHSCHR19KIR_FH05_A_HAP_CTG3_1
adding key chrHSCHR19KIR_FH05_A_HAP_CTG3_1 (len = 32) value NT_187674.1 (ac) (len = 12) to hash table
```

Observations:
* I still don't see the use of the `chrhg38` key.
* It seems that `chrom` is not reset between consecutive lines of the
  input file `chr_NC_gi`, other than by `strcpy()`.
* The keys have changed: we now have `adding key
  chrHSCHR19KIR_ABC08_AB_HAP_C_P_CTG3_1 (len = 38) value NT_187672.1` when
  previously we had `adding key chrHSCHR (len = 9) value NT_187672.1 (ac) (len =
  12)...` --- though the only change to the code was calling `fprintf()`.

Ok, the problem was that the `CHR_NB` constant was not used everywhere it should
have been. In particular, on l. 157, we had:

```c
char chrom[18];
```

which of course wasn't long enough for 34 characters. I changed this to 

```c
char chrom[CHR_NB];
```

and the problem disappeared. The same situation occurred in `sga2bed.c` and
`sga2wig.c`, so I fixed these as well.

There is still a problem due to ACs being prefixed by `chr` (`bed2sga.c` l. 271,
I think) while the corresponding ACs in `chr_NC_gi` are not.

Duplicate specification "C" for option "c"
=========================================

When running the tests for commit `ffd02987ce2c`, some Perl programs output a
message I don't recall seeing before:

> Duplicate specification "C" for option "c"

The tests succeed alright, mind you --- it's just that I have no idea why these
messages suddenly appear, and what change has brought them about. When I
checkout an earlier commit (say, `8d2f37ca3a1be`, from early July when I'm
pretty darn sure I saw no such message), the message appears too. So the change
was likely not added by ourselves, but by an external cause, such as an update.

This (or the same with another option letter) concerns the following programs:
`gff2sga.pl`, `partit2bed.pl`, and `sga2gff.pl`. They all happen to `use
Getopt::Long;`, although they are not the only ones to do so.

Running in help mode (`-h`) is sufficient to elicit the message.

The message is output when the following line runs:

```perl
GetOptions( \%opt, @options ) );
```

where `%opt` is a hash in which the options and their values are to be stored,
and `@options` is an array of the following form, that specifies which options
are recognized:

```bash
my @options = ( "help", "h", "species=s", "a=s", "s=s", "feature=s",
  "f=s", "c", "C", "u", "x", "db=s", "i=s");
```

Unfortunately, in typical Perl fashion ("there's more than one way to do it"),
the `GetOptions` function has many call syntaxes, and I wasn't to find the one
above in Perldoc or on the web (it may be a little old). So I'm not sure why it
insists that the specification for `-c` is the same as that for `-C` (unless it
is case-insensitive?)

As it turns out, good ol' Stackoverflow pointed me to an answer
(https://stackoverflow.com/questions/5408357/perl-getoptions-case-sensitivity).

```perl
use Getopt::Long qw(:config no_ignore_case);
```

`sga2bed` Problems
================

> From a mail sent by PB on 2024-09-22:
> 
> When writing the detailed documentation for sga2bed, I noticed that the option
> -X/--extend is not working.
> 
> I used the following test sga file:
> 
> ```
> NC_000001.10 F      1      +      1      1
> NC_000001.10 G      1      0      1      Line_2
> chr1   F      1      0      1      Line_3
> 1      F      1      0      1      Line_4
> NC_000101.10 F      1      +      1      Line_5
> ```
> 
> (see attachent).
> 
> Test command:
> 
> ```bash
> $ sga2bed --extend 6:4 < sga2bed_test.sga
> ```
> Output on epdnew (currently installed old version):
> 
> ```
> track name="F" description="ChIP-Seq Custom data" visibility=1 color= ...
> chr1   0      1      1      1      +
> chr1   0      1      Line_2 1
> chr1   0      1      Line_3 1
> ```
> 
> That looks correct (in agreement with -h)
> 
> New version after git clone and compilation:
> 
> ```
> track name="F" description="ChIP-Seq Custom data" visibility=1 color= ...
> 
> Segmentation fault (core dumped)
> ```
> 
> New version on my mac after git clone and compilation:
> 
> ```
> chr1   0      1      F      1      +
> chr1   0      1      G      1
> chr1   0      1      F      1
> ```
> 
> The same output is obtained without option --extend 6:4
> 
> Could anybody or both of you look into this problem? I noticed that the latest
> version of sga2bed.c is quite different from release chip-seq.1.5.5
> 
> ```bash
> $ diff -w ../git/src/sga2bed.c ../chip-seq.1.5.5/tools/sga2bed.c | wc -l
>      432
> $ diff ../git/src/sga2bed.c ../chip-seq.1.5.5/tools/sga2bed.c | wc -l
>     1386
> ```
> 
> So, I'm wondering whether v1.5.5 was the starting point for the current modified
> version.

Downloaded the attached file as `./data/sga2bed_test.sga`. The problem could be
reproduced e.g. from `./src`:

`in``bash
$ ./sga2bed -i ../data --extend 6:4 < ../data/sga2bed_test.sga
track name="F" description="ChIP-Seq Custom data" visibility=1 color=100,100,100
zsh: segmentation fault (core dumped)  ./sga2bed -i ../data --extend 6:4 < ../data/sga2bed_test.sga
```

(the `-i` is required because the genomic information files are not in `./src`).

TODO
====

## URGENT

 
* [ ] Find a guinea pig for testing the whole install procedure and reading the UG
  (and try out the examples). Estimated time: at least 1/2 day. Best candidate
  should be familiar with NGS data and epigenomics in general, but not a
  guru in computer science / system administration.

## NORMAL

* [x] Review UG (print and read, like a MS). Also, check in recent commits from
  PBdocfixes about material that may have been lost - I distinctly remember
  writing stuff about Conda, and perhaps Apptainer/Singularity, in what is now
  sct 1.3 (Running the Examples). 

* [ ] Clean up the mess in ./doc (only keep necessary files)

* [x] Fixed the "Duplicate specification..." problem (see above). All tests ok.

* [x] new documents to integrate to UG: cf mail above, [usual
  site](https://epd.expasy.org/epd2/chipseq/userguide/), marked 'new!'
  NOTE: '-a' should be considered a regular option.

* [.] see PB's last mail, 2024-08-30 ~ 0950 AM. `gff2sga.pl` - Already changed
  'feature' (wrong) to 'type' (correct), but page layout is still wrong (TABs vs
  spaces); furthermore the correct terminology must be propagated to the UG.
  First steps taken - see `8704dda'.  **NOTE** `sga2gff.pl` still mentions
  'feature' - should this be changed to 'type' as well? => YES!

* [x] rebase PBDocfixes onto master. => Took many steps (in which I had to
  manually solve conflicts) but finally it was completed.
 
* [x] sga2gff.pl does not respond to -i (only --db). Check that (and other Perl
  scripts as well). See `f1e694b`.
 
* [x] rebase 64chars onto master: eventually succeeded, but replaced original
  chromosome info files by test versions. Restore originals. Those are:
   - data/chr_NC_gi         
   - data/chr_size
  Fixed in `5f9615a`.
 
* [x] see SM's last msg re: failing test (Teams, July 2024) => both the test
  failure and compilation warning fixed (see 69cf9b42290 and 21ccb50774746) 
 
* [x] see mail from PB on 2024-07-25 re: (i) C locale before sorting and (ii) max length
  of fields #defined to 18 (try 64; ck how it handles data that is too long)
 
* [x] check out FAIR recommendations regarding software documentation
 
* [x] try SM's new Docker container (ck Teams chat, ~ 2024-07-17) => still the
  same permission problem, but then the links in the Teams msg point to
  instructions for installing Docker (which I have already done), not to a new
  container. Did I miss something? => NO, but in the end I found the problem: it
  was down to the fact that my directory is encrypted: for some reason, even in
  decrypted state, Docker doesn't get the permissions right.
 
* [x] add SM, TJ, PB to authors of UG
 
* [x] tarball URL: use one of the Git-generated tarballs (SM knows...)
https://gitlab.sib.swiss/EPD/chipseq/-/archive/v1.6.0/chipseq-v1.6.0.tar.gz
 
* [x] Conda: PATH should not be necessary (`conda activate` takes care of that)
 
* [x] Wrote instructions for running the examples in the Conda environment, ->
  `ffcb8b3`. 
 
* [x] Wrote instructions for running the examples in the Apptainer/Singularity
  environment, but they suffer from the same permission problems as the Docker
  instrucrtions (since they actually use the Docker container) , so I couldn't
  check them. -> `e7ade3c`
 
* [x] `./partit2gff.pl` -h fails. Check if option `-a` is accepted (heed the
   conventions introduced in early 2024). -> `master`, commit `929c630`.
 
* [.] UG: Docker: provide instructions for Docker examples, in which the path to
  the local data is stored in a variable instead of hard-coded in the UG. The
  data will no longer be bundled within the container: rather, they will be
  downloaded separately (cf PB's mail about MEME). **NOTE** instructions
  written, but container has permission problems - see 'Container
  Probs' above.
  * [ ] UG: instead of duplicating each and every example for Docker,
    Apptainer/Singularity, and Conda: write an appendix/section that explains the
    differences WRT the native environments. THIS OVERRIDES THE item below!
 
* [x] Provide cross-references to examples (using section headings as labels):
  Pandoc's cross-ref should be able to handle this; if not we can fall back to
  native LaTeX (but we'd rather avoid that). => Works +-, using Lists (if this
  is not acceptable, we'll have to define a counter and evironment).
 
* [x] File formats info (on website, cf PB's mail) ... => integrated in UG.
 
* [x] Re: PB's last mail (btwn May 4 and May 8): download archive and
  try/understand commands. Note also the following:
  * the liftOver prog is no longer completely open access - requires login; must
    accept to pay but then won't be charged as we're not-for-profit; still it is
    best to download it
  * for now, ignore sga2gff (Chap. on reformatting / import-export)
  * Re: `file_formats.{md,pdf}` (web link (?)) for now, just read and comment (that is, don't
    merge into UG just yet)
 
* [x] ck that the -C/-c fix has been merged to `master` -> This is commit
  5611fb28a46...
 
* [x] merge PB's 2024-05-03 README with the User Guide: integrate the info into
  Chap. 4, and reorganize Chap 4 accordingly (remove redundancy, etc.); this
  should add more complete description of "other" (non-chip...) tools
 
* [x] Apply changes in PB's document (mail)
 
* [/]<OBSOLETE!> Docker examples: rewrite so as to use guest, not host, files (i.e., no
  need to mount); still add an example of use with a file on the host (for
  users' own data). 
 
* [x] check the `make gunzip-data` instruction in the UG: PB reports it doesn't
  work.
 
* [x] Add documentaion for tools not yet thoroughly documented in the UG,
  following the same format as used in the existing sections; in
  particular:
  * synopsis (as already exists for `chip*`)
  * mandatory params, as a table (ditto)
  * optional params, as a table (ditto)
  Add them to sct. 4 (for now, at least).
 
* [x] See about decompressing gzipped input files on the fly (preprocess with
      zcat? link to zlib?) 
 
* [ ] Handle variables in help functions (see above).
 
* [ ] `test_main.sh`: add an option to re-run all failed tests (and only those).
 
* [ ] `test_main.sh`: have 1st arg be the test list (same as ar to `-p`have 1st
  arg be the test list (same as arg to `-p`)
 
* [x] Harmonize options (see mails w/ PB)
 
* [x] The "alernative" pipeline on p. 15 of the Guide (just before Section 4)
  does not seem to work, while the original (top of p. 15) does.
 
* [x] The output of `../src/chipcenter -i ../data -f CTCF -s 40
  ../data/CTCF.sga` contains a suspicious `argv[7]: '../data/CTCF.sga'` line. --
  Fixed.
 
* [x] Add a "Install" section to the chipseq manual: it should contain
  instructions about builiding the executables (just run make...), installing
  them, and also to decompress the gzipped files (so that the manual no longer
  needs to instruct users to zcat them on the fly...)
 
* [ ] PB reports that zcat isn't found on (all) macs... in fact, do we
  absolutely need to compress those files?
 
* [ ] Clear up confusion about `./data/hg19.sga`, `./data/hg19_chr.sga`, and
  `./data/hg19_chr2.sga`: there are tests using all of them, but they differ in§
  their first fields, having "NC_000001.10", "1", and "chr1", respectively. I'm
  not sure all the test cases are correct. Follow-up: this should be examined on
  a case-by-case basis, by looking at what the code is actually trying (or
  claiming) to achieve. This concerns only some Perl scripts.
 
* [ ] Shouldn't there be an argument to `-o`? in the example command for
  `chipscore` (user guide, around l. 638 in source .md)
 
* [ ] When `chippeak` can't find `chr_NC_gi`, it aborts with the help, but says
  nothing about the actual problem. Fix that (should at least suggest to pass
  `-i`).
 
* [ ] harmonize option semantics across all chipseq programs, especially the
  "main" suite (C) and Perl suite. Auxiliary tools may be harmonized second, but
  eventually it would be best if consistency was kept everywhere.
 
* [ ] See about the #TODO test in test_cmds.tsv
 
* [ ] See about tests that leave cruft when failing (the `trap` branch was about
  that...)
 
* [x] The examples in the user guide are likely meant to be run from `./src`, or
  at least from a direct subdir of the distribution dir - otherwise locations
  like `../share` make no sense. But they refer to the binaries by their name,
  implying a search in `PATH`: this command is supposed to work anywhere, not
  just in `./src`. So either:
  * base the commands in `./src`, and call it as `./command`
  * base the commands in `./data`, and dispense with `../data` => base the
    commands in `./doc` (2024-01-12 TC); **fix the zcat commands** (`../data` is
    missing); update note at the beginning of the Examples section.
 
* [x] User guide: replace references to `./share` (which no longer stores data)
  with `./data` (which does).
 
* [x] Change the user guide to use examples that refer to the compressed version
  of the data (see w/ SM).
 
* [x] Add a `PERL_BIN_DIR` variable to the exports by `test_main.sh` to the test
  scripts. This is to uncouple the location of Perl scripts from that of C
  programs. This is relevant for Docker and Conda (discussed on 2024-01-04).
 
* [x] Manually test the behaviour of the patched programs (see below) on
  unsorted SGA; document this above in this file - no need to add a formal test
  for each program (there is already one for chippart anyway).
 
* [x] Fix the sort(1) command used to check SGA file (see Teams msg by SM,
  2023-12-14)
 
* [x] Propagate the change WRT tmp file checksort.out (see chippart.c): reliance
  on a shared tmp file is a liability, this should be fixed now. Other changes
  (such as the refactoring of the sort-checking code into a separate function)
  can wait till the next release.
 
* [ ] document the changes in the code (unclear how; mailed PB for details)
 
* [x] start preparing a presentation about the EPD project for the 2023-12-01
  V-IT group meeting.
  * start general: most ppl aren't necessarily familiar with the pros of
    systematic testing
  * mention our own constraints: various environments (native, containers,
    conda); two OSs (Linux, MacOS)
  * discuss our design choices
  * mention that the whole system is documented
 
* [ ] test handling of RefSeq by using two kinds of input files:
  * garble the first RefSeq ID
  * remove the line that contains said ID altogether
  And then compare the results. This will determine how we classify the
  program's behaviour. If removing the line has the same effect as garbling the
  ID, then we can assume the program ignores lines with invalid IDs; otherwise
  it's more complicated. If the output differs (between the two modified input
  files), then we can assume that the program accepts the invalid RefSeq ID.
  
 
* [x] check clang warnings (MacOS's C compiler), cf SM's msgs in Teams
 
* [.] Test the behaviour of reformatters/converters when passed invalid sequence
  IDs/ACs (see e.g. test_sga2fps_-s_xenTro3_xenTro3_chr_sga, which correctly
  rejects an invalid RefSeq ID). Ideally, the behavour should be consistent
  across the various programs in the distribution, but that may be for the next
  release. Also consider the case when IDs from different assemblies (e.g. hg19
  and hg38) or even different species (Amel4 and mm8, perhaps?) are mixed.
 
* [x] Redo the genomic info files. (This was discussed during the 2023-09-08 TC,
  then dropped from this list for some reason.)
    1. remake chr_NC_gi: start with the current one (Git) copy and remove all assemblies
       not in chr_size
    2. remake chr_size: do the same as for chr_NC_gi -> intersection of both.
       The two files now contain the same genomes.
    3. Check that the sequence ACs are identical (flds #2 and #1, respectively).
       The rest of the lines can be ignored.
    4. Run make_chro_idx.nstorage.pl, using the two above files as input.
    (underway: the lists of ACs were not as expected, so I sent them to PB as
    requested).
 
* [x] Integrate the tests in the test script
  ~/projects/chipseq/chipseq_reformatting_test/reformatting_tests.sh into the
  test system.
 
* [ ] There should be a single version of the User Guide, and that should be in
  the chipseq repo.
 
* [x] Tests: consider adding setup and teardown commands so that we don't have
  links to data lying around in the test dir: for example, some scripts expect
  `chr_size` etc. to be in the CWD, or else to be told where to find it
  (`-i`/`--db`). This has two undesirable consequences: (i) we accumulate links
  to the actual files, and (ii) we can't know for sure how the program behaves
  if the file is not found, since it's always there. So, the idea is to make
    these links pre-test, and remove them post-test. This could be done with
    commands stored in a text file, just like the test commands are.
 
* [x] Mail this file to PB
 
* [x] In case the chromosome information file (or other external data file) is
  not found, programs should output an informative error message, perhaps
  suggesting option -i/--db, and then output the whole help.
 
* [x] whitespace separation of SGA: most of the C code should be left untouched,
  but check the `split_str()` function in `sga2bed.c`: does it work on all
  whitespace (as it should) or just on TABs? If not, fix that. As for Perl
  scripts, change the regexp as above (Sect. 8) **NOTE** this concerns **SGA in
  input**, not other input formats or any output formats.
 
* [x] indent C code? Some of it is hard to read. I have indented the `*.[ch]`
  files I had to work on (just a simple `indent -br -brf`, nothing fancy), but
  it might be a good idea to do this systematically. The same goes for Perl,
  BTW. Ok, well I went ahead - see README.indent in ./src.
 
* [x] mail this document (PDF) to PB
 
* [x] for the test of parti2gff.pl: use data/chippart_164940_172769.{sga,gff} (!
  currently on vit-103!), mind the difference in 1st colum ('1' vs 'chr1',
  etc.). See also mail from PB, 2022-08-21.
 
* [x] check if BED is TAB- or whitespace-delimited. - See above.
 
* [x] SGA should be whitespace-delimited, not TAB (cf. above). Fix Perl code
  towards this, and revert C code to the original (which was written with
  whitespace-separated data in mind).
 
* [x] Make a list of characters that are allowed in SGA feature field
  (2023-08-18 TC). Emit a recommendation to users about preferred characters
  (e.g. alphanumeric + underscore ?)
 
* [ ] The UG's Appendix classifies the programs by type (conversion,
  reformatting, correlation, etc.). Since we now include much more than the
  original 6 programs, it might be better to structure the document along the
  same lines, e.g. a section for correlation programs (`chipcor` etc.), another
  for peak-finders, etc.
 
* [ ] Check whether (and if so, where) to describe the C programs not already
  covered in the Guide (`compactsga`, `featreplace`, etc.)
 
* [ ] Harmonize arguments in user doc with source code (all programs) - see PB's
  mail on 2023-05-30, and my reply thereto of 2023-05-31.
  * [x] C programs already in the Guide
  * [ ] Perl scripts
 
* [ ] Suggest a more terse form for some User Guide option descriptions, e.g.
  "It defines the integration range" -> "Integration range".
 
* [ ] Validate my choices - I had to come up with test inputs and command
  parameters for some of the tests, when none were available or known to me.
  These must be checked by PB. See @sec:converter-tests.
 
* [x] `test_main.sh`: consider exporting only `DATA_DIR` and building the path
  names in `test_cmds.tsv` instead. This would simplify the main script, and
  dispense us from modifying it every time a new test input file is required.
  `test_chr_replace_sga` already works in this way.
 
* [ ] Mention the "trailing slash" issue to PB and SM, also float the idea of
  specifying the _file_ rather than the containing dir (see @sec:chr_replace_sga
  above).
 
* [x] see the source to chipcenter, l. 141 or thereabouts (do loop to remove). -
  In fact, the situation was a bit different from that of bed2sga (which I had
  already done): for chipcenter (and chippeak), the loop did no harm, but it was
  redundant with a further check for '^#' about 10 lines into the while loop
  just below. So I removed the do loop anyway. All tests ok.
 
* [x] `test_main.sh` doesn't work on some Macs because their Bash is too old (it
  doesn't have the `-v` test). Make the test script work with those macs; in fact,
  make it compliant with the POSIX shell. I'm afraid that means replacing `[[`
  with `[`, but so be it. (**Update**: after discussion with SM, we agreed that
  compliance with the POSIX shell was unrealistic and that ensuring the script
  works on macs should be enough for now - see Teams, late June 2023). AFAIK PB
  hasn't formally approved this, however).
 
* [x] The user doc has an inconsistency: the sort cmd on line 105 (p. 3) works
  with _any_ whitespace, but l. 35 states that SGA is TAB-delimited. This is
  wrong, as SGA may use any whitespace as delimiter. SM thinks, like me, that
  the C code should be changed to that its input is really TAB-delimited, so as
  to reflect the doc (rather than the other way around). See my mails in late
  June 2023.
 
* [x] Ensure that there is at least one test case for each of the "translator"
  utils (i.e. those with names of the form x2y). Input files for testing are
  referenced to in PB's 2023-05-31 mail (cf. next item): they're either the
  output of other programs (such as chippart or repeatmasker), or the mail
  contains pointers to them.
 
* [x] Harmonize the behaviour of programs (both C and Perl) when passed with
  insufficient arguments. Note that some (e.g. bed2sga, compactsga) may wait for
  data on stdin, while others (sga2bed) simply quit.
 
* [x] Add `make_chro_idx.nstorage.pl` (done by SM).
 
* [x] check that the /.../local/ -> ./ change (see last change to C source) was
  propagated to ALL files that need it (C and Perl).
 
* [x] The current build structure in the chipseq repo causes build fails on some
  architectures (incl. WSL2) due to unexpected placement of `-lm` _before_
  object files rather than after them as is more usual. Compounding this is that
  the same executable may be built by two different Makefiles, namely ./Makefile
  and ./tools/Makefile (such is the case, among others, of tools/bed2sga (or
  just bed2sga, depending on the Makefile...). We decided (2023-03-30) to keep
  only one makefile and to keep only Perl scripts in a separate subdir (to be
  renamed perl).
  *NOTE* These changes have been made and committed, but somehow they
  are not in effect in master (c9d9bed276c44aa50dd2273c483367b339f0371d). 
 
* [x] Some of the tables in the .md version of the manual do not render
  correctly (or at all...) on Gitlab. Fix this, e.g. by using pipe tables instead
  (these are known to work, but it's unclear what control the author has on
  column widths).
 
* [x] Convert the user manual to markdown.
 
* [x] Implement 2nd-round corrections (weird chars in commands, missing `-i
  ../share`, missing argument in `sort -m` (p. 12; missing arg is on p. 11).
 
* [x] Carry out suggested mods to the user guide (PB will send the .doc
  original). This should be done directly in the document.
 
* [x] Check that the example commands in the user guide work. The .sga files are
  in ./data as .gz. Also check for any not-mentioned files. Annotate any
  problems in the PDF, as done previously.
 
* [x] Have a look at the Chipseq Tools User.doc and check that everything is
  clear and complete (check e.g. that all tools mentioned are also described).
 
* [x] For all Perl tools in the chipseq Github repo, check that the help text
  obtained on the command line (typically, but not exclusively, via `-h`) is
  consistent ith the code. Also, check that the list of scripts in the repo
  (https://gitlab.sib.swiss/EPD/chipseq/-/blob/master/doc/ChIP-Seq_Tools-UsersGuide.pdf)
  matches the available scripts.
 
* [x] Try installing the Docker version of MEME-suite (as seen during the
  2023-01-25 meeting).
 
* [x] Write a Readme for the whole test script and related files, including how
  it's meant to be used, how to add new tests, etc. 
 
* [x] tests: the new input files (PB, ~ 2022-12-05) result in much faster tests,
  so that having 3 modes (full, short, devel) is no longer useful. Keep only
  full.
 
* [x] tests: Docker might be made faster by keeping a single instance for the
  whole test run instead of creating a new one for each test.
 
* [x] Install conda and try running the test script under it (bt first mail SM
  about details).
 
* [x] The Makefile in the ./tools subdir of the chipseq-1.5.5 distrib from SM's
  repo (https://gitlab.sib.swiss/EPD/chipseq.git) has a bizarre build rule (l.
  45): ```Make compactsga : $(BED2BED_DISPLAY_SRC) $(OBJS) $(CC) $(LDFLAGS) -o
  compactsga $^ ``` Notice that the target and object file are called
  `compactsga`, but the _source_ file is `bed2bed_display.c` (l. 25). As it
  happens, the error message output by the Docker version of `compactsga` is
  almost exactly that of `bed2bed_display`, and very different from
  `compactsga`'s (tellingly, it refers to a BED file in input, which seems
  bizarre). So:
  * fix the Makfile and recompile
  * if it solves the problem, send a patch to SM for inclusion in the Docker
    container.
 
* [x] investigate why some tests (e.g. chipscore) fail in the docker environment
  but succeed in all the others (including  apptainer/singularity). I thought it
  was down to the version of Chipseq (the container has 1.5.5, the native
  distrib has 1.5.4), but it turns out not to be the case, because when I run
  the same command as in the script, but using
  ~/Downloads/chipseq-1.5.5/bin/chipscore instead of
  ~/projects/epd/src/chipseq/src/chipscore, (the former 1.5.5, the latter 1.5.4)
  it works just as well.
 
* [x] Send a report detailing all differnces in test results between all
  environments to PB, GA, SM.
 
* [x] Check for differences between versions 1.5.4 and 1.5.5 of ChipSeq (at the
  source code level). Also check READMEs, man pages, Makefiles etc.
 
* [x] Instead of choosing between stand-alone test scripts and commands in a TSV
  file, use _both_: generate scripts on-the-fly using the stored commands (the
  file also stores the script's name), then have the main script run _those_
  scripts (and then remove them (or not if passed some option)). Optionally just
  create the scripts but not run them at all, so the user can do it manually,
  etc.
 
* [x] (Superseded by new, shorter expected files) Test input files: some can be
  generated procedurally (e.g. H3...) - see Web site, e.g. go to Human, chipCor
  (or other), run an example and check the log (as usual) - the commands shown
  allow to recreate input files.
 
* [x] `chip*` man pages: see updated and corrected version of task (table
  above). 
 
* [x] Try separating the Docker tests into separate scripts, one test per
  script.
 
* [ ] For the new tests, also checkout the output files generated by the server.
  In fact, this can also be useful for the old tests: the examples on the server
  include input data e.g. for chippeak, etc.
 
* [x] Compare the manpages (when they exist) to the output of the corresponding
  program when it is passed `-h` (the manpages may be outdated since they're
  from 2013).
 
* [ ] Add tests of the other executables. Examples and hints of parameter values
  and input data can be found in `./tests/chipextract_examples.txt`, in
  `./tools/README_tests.txt`, as well as by passing `-h` to the programs.
 
* [x] Make a list of all programs that use `chr_NC_gi` (pass `-h`, grep in the
  source for any hard-coded references to that file). Perl programs may use
  `chro_idx.nstorage` instead (see on srv4), which is a binary version of
  `chr_NC_gi`. List those programs (most likely Perl) that can only use that
  binary datafile but not the text one.
 
* [x] Tests: add tests for `chipextract` (go to website for examples of usage),
  see results page (launch application: Chip-Extract -> Example -> Log file of
  backend commands). 
 
* [x] Explicitly require /bin/bash for Docker tests that launch a shell (such as
  all that feed input to STDIN) - some use process substitution, which is not
  supported by the POSIX shell (still less so by the Bourne shell).
 
* [x] Use a local (as opposed to in-container version of `chr_size`)
 
* [x] Add checks for status in timed tests - when the input file is not found
  the test is very fast - but that should NOT count as a success!
 
* [ ] Add tests vs expected output files where still lacking.
 
* [x] update Dockerfile in ~/projects/EPD (SM's repo) - just do git pull - then
  build a chipseq container based on that latest version; then try again to run
  chipseq binaries from the container.
* (obsolete) [x] review the discussion about 'shim tasks' and associated errors occurring
  when launching chipseq binaries with docker run (see mail exchanges May-June
  2022)
* (obsolete) [ ] Try docker installs on other machines: problem may be linked to a particular
  Linux distro, try other distros/OSs
 
* [x] Continue trying to get accurate line numbers in Valgrind - do tests with
    new kernel/libc, but don't waste time on it if it still fails. (No change
    after last update -still doesn't give line numbers, or line numbers do not
    correspond to statements where memory errors could occur).
 
* [x] Transform `/home/tjunier/projects/epd/src/chipseq/src/tests/README` into a test script (shell).
  * [x] Tests: add a mode for testing the commands running in a Docker container
  * [x] Tests: use only chromosomes 21 and 22 from the inputs - this should
    speed things up.
