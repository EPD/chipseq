---
fontsize: 12pt
classoption: onecolumn
geometry:
  - top=30mm
  - left=30mm
  - right=30mm
numbersections: true
toc: true
---

## Appendix #: Genome Information Files

Some ChIP-seq programs require, or make use of, specific information about sequences referred to in the first field of SGA files.

-   *chr_NC_gi*: text file used by C programs, associates sequences identifiers with alternative names, often chromosome names.
-   *chr_size*: text file used by C programs, informs about the length of the sequences.
-   *chro_idx.nstorage*: binary file used by Perl script, automatically generated from *chr_NC_gi* and chr_size, contains information from both files.

By default, these files are expected to reside in the current directory. The location can be changed by the command line option `-i` or `--db`.

Changing between sequence identifiers and sequence names is useful when working with public sequences referred to in different ways. For instance, the sequence of human chromosome 1 may be referred to as chr1 or by the versioned RefSeq identifier NC_000001.11. The number 11 is the version number, which for this chromosome corresponds to the human genome sequence assembly hg38. Note in this context, that NC_000001.11 identifies a unique DNA sequence whereas chr1 is ambigous if no corresponding assembly is specified. As different versions of human chromosome 1 have different lengths, the genomic coordinates of the same feature change from one assembly to another. Mixing coordinates from different assembles leads to wrong or empty output with ChIP-Seq tools. Therefore, all SGA files included as examples in this distribution, use versioned NCBI identifiers.

The possibility to switch between different types of identifiers enhances interoperability with external software and data resources. For instance, to display peaks called with ChIP-Seq tool *chippeak* in a genome browser, it may be necessary to convert unique NCBI identifiers into chromosome names. Likewise, importing genomic features available in BAM, BED or GFF3 format my require conversion of chromosomes names into unique sequence identifiers (executable examples in the main part if this document).

If you are working with your own private sequence, you will have to generate the genome information files yourself. The file *chr_NC_gi* serves no essential tasks in this case, but some programs require that it exists, and that sequence names appearing in SGA files are registered therein.

if you are working with a public genome not covered by the *chr_NC_gi* file included in this distribution, you will have to generate a new *chr_NC_gi* file or add the corresponding information to an already existing one (see format specification below).

We recommend using genomes from RefSeq or GenBank if available. Both repositories provide a file containing comprehensive assembly information. For instance, you can find a complete distribution of human genome assembly hg38 (nucleotide sequences, metadata and gene annotation) in the following directory:

> <https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/>

This directory contains a file named:

> GCF_000001405.40_GRCh38.p14_assembly_report.txt

You may download this file to your local directory to explore its content. For instance, type on the UNIX command line:

```         
grep NC_000001.11  GCF_000001405.40_GRCh38.p14_assembly_report.txt \
 | cut -f1,2,7,9,10
```

You will see:

```         
1  assembled-molecule  NC_000001.11  248956422  chr1
```

This line is extracted from a Subection of the file that contains a tab-delimited table with information on individual sequences. The column headers of the extracted fields are:

-   \(1\) Sequence-Name
-   \(2\) Sequence-Role
-   \(7\) RefSeq-Accn
-   \(9\) Sequence-Length
-   \(11\) UCSC-style-name

Note that there are three different sequence identifiers: Sequence-Name, RefSeq-Accn, UCSC-style-name. The Sequence-Name usually consists of a number, a single letter, or a short aphanumeric code (e.g. 2L for left arm of Drosophila chromosome 2). The UCSC-style-name usually consist of the NCBI Sequence-Name prefixed by "chr".

Note that the "Sequence-Role" of human chromosome 1 is "assembled-molecule". This is the "role" assigned to complete chromosomes. Genome assembly hg38 contains sequences with other roles such as "alt-scaffold". Those are not registered in the genome information files provided with this release. As a consequence, ChIP-Seq tools will ignore them on input. Note, however, that data from other sources provided in BAM, BED or GFF format may include such sequences. In such situations, the genome information files serve to restrict the output to the set of sequences registered in *chr_NC_gi*.

The text-based genome information (*chr_NC_gi* and c*hr_size*) files provided with this release contain Sequence-Names. (e.g. 1 for human chromosome 1). On output, ChIP-Seq format conversion tools will convert these names into UCSC-style names by adding the prefix "chr". On input, they accept both NCBI and UCSC-style names.

### Format of genome information files

#### 1. chr_NC_gi

Remember, this file converts sequence identifiers (ACs) from input to sequence names on output, or vice-versa. It is a sequentially organized (order of lines matters), whitespace-delimited text files with two line types:

-   Assembly lines: they start with "\#" immediately followed by the assembly name (e.g. #hg38). Only the first field is used by the programs. The following lines are assumed to belong to this assembly, unless a new assembly line is encountered.

-   Sequence lines: The first two fields are read by the program. These fields contain the sequence name" and the corresponding unique identifier. If the genome comes from RefSeq (as is the case for the *chr_NC_gi* file provided with this distribution ), the second field corresponds to the RefSeq-Accn (see example above).

Additional characteristics:

-   Both line types may contain additional fields (e.g. notes by the author), which are ignored by all programs.

-   The sequence names are invariantly alphanumeric character strings (A-Za-z0-9). Sequence identifiers contain "." (dot) and sometimes "\_" (underscore) in addition.

#### 2. chr_size

This file is used by some programs to prevent output of sequence coordinates that fall beyond the length of the sequence (which could provoke an error exit of a third party programs that is used downstream in an analysis pipelines).

The file has a similar structure as chr_NC_gi, with the following notable differences:

-   The *chr_size* file included in this distribution is *de facto* tab-delimited.

-   The second field of the sequence lines contains the length of the sequence.

#### 3. chro_idx.nstorage

This is a binary file generated rom *chr_NC_gi* and *chr_size* by the script *make_chro_idx.nstorage.pl.*

``` perl
make_chro_idx.nstorage.pl <chr_NC_gi> <chr_size> <chro_idx.nstorage>
```

The file is used by Perl scripts. It contains the combined information from *chr_NC_gi* and *chr_size* in a data structure suitable for Perl. In principle, it should be information-wise equivalent to the two text files used by the C programs, with one notable difference however: It contains UCSC style sequence names starting with "chr" instead of NCBI style names. This makes no difference from the user's perspective as C programs automatically add the prefix "chr" to sequence names. On query input, both UCSC and NCBI style names are accepted. For example, with option -s hg38 all programs map "1" and "chr1" to NC_000001.11.

Yous may explore the contents of *chro_idx.nstorage* with a few lines of Perl code, as exemplified below:

Reading the file, loading the data structure:

``` perl
use Storable qw (retrieve nstore);
my $chr2SV = retrieve('chro_idx.nstorage');
```

Given accession, find assembly:

``` perl
$assembly = $chr2SV->{'assembly'}->{'NC_000001.11'};
print "$assembly\n";
```

Given accession, find sequence name:

``` perl
$name = $chr2SV->{'NC_000001.11'};
print "$name\n";
```

Given assembly + sequence name, find accession:

``` perl
$accession = $chr2SV->{'hg38'}->{'1'};
print "$accession\n";
$accession = $chr2SV->{'hg38'}->{'chr1'};
print "$accession\n";
```

Given accession, find sequence length:

``` perl
$length = $chr2SV->{"length"}->{'NC_000001.11'};
print "$length\n";
```
