cff-version: 1.2.0
abstract: The ChIP-Seq tools and web server: a resource for analyzing ChIP-seq and other types of genomic data.
authors:
  - family-names: Ambrosini
    given-names: Giovanna
    orcid: 0000-0003-1294-6541
  - family-names: Dreos
    given-names: René
    orcid: 0000-0002-0816-7775
  - family-names: Kumar
    given-names: Sunil
    orcid: 
  - family-names: Junier
    given-names: Thomas
    orcid: 0000-0002-4015-5969
  - family-names: Moretti
    given-names: Sébastien
    orcid: 0000-0003-3947-488X
  - family-names: Bucher
    given-names: Philipp
    orcid: 0000-0003-4824-885X
title: The ChIP-Seq Tools
version: 1.6.0
identifiers:
  - type: git
    value: https://gitlab.sib.swiss/EPD/chipseq
    description: The ChIP-Seq tools
  - type: git
    value: https://gitlab.sib.swiss/EPD/chipseq-web
    description: The ChIP-Seq web interface
date-released: "2024-09-19"
keywords:
  - ChIP-seq
  - high-throughput chromatin profiling assays
  - positional correlation analysis
  - peak detection
  - genome partitioning
  - DNA-methylation
  - promoters
  - CpG
license: "GNU GPLv3"
doi: 
affiliations:
  - name: SIB Swiss Institute of Bioinformatics, Lausanne, CH
references:
  - authors:
      - family-names: Ambrosini
        given-names: Giovanna
      - family-names: Dreos
        given-names: René
      - family-names: Kumar
        given-names: Sunil
      - family-names: Bucher
        given-names: Philipp
    doi: 10.1186/s12864-016-3288-8
    journal: "BMC Genomics"
    start: 938
    title: "The ChIP-Seq tools and web server: a resource for analyzing ChIP-seq and other types of genomic data"
    type: article
    volume: 17
    year: 2016

