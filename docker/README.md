```bash
export CHIPSEQ_VERSION=1.6.0
```

# Run bash in the Docker image
```bash
docker run --rm -it chipseq:$CHIPSEQ_VERSION bash
```
### Mounting/binding a local repository (,readonly can be added to force readonly mounting)
```bash
    --mount type=bind,source=/software,target=/software
```
- CHIPSEQ_VERSION is the container version
- --name assigns a name to the running container
- --rm automatically removes the container when it exits
