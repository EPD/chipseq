Program Installation
============================================================================

For code compilation and data/code installation, a suitable makefile is provided.

- To compile the executable files in the src/ director:
```bash
make
```

- To test the executable files with the test suite:
```bash
make test
```

- To install the executables, the documentation and the sample data files (by default in /usr/local):
```bash
sudo make install
```

- To install the executable files in a specific directory:
```bash
PREFIX=<where you want>  make install
```

- To delete the executable files and all the object files from the compilation directory:
```bash
make clean
```

- To delete the executable files and all the object files from the installation directory:
```bash
make uninstall
```


The data/ subdirectory
============================================================================

This directory contains a few data sets that can be used to run some tests.
Examples on how to use the ChIP-Seq tools with these data are described in the user's guide (doc/ChIP-Seq_Tools-UsersGuide.pdf).

From release 1.5.5, the data referred to in the user's guide have been put on our FTP-Site at:

https://epd.expasy.org/ftp/chip-seq/
