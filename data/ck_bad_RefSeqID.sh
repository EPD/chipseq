#!/usr/bin/env bash

# vim: ft=bash

set -u

# fps2sga is the only script that takes FPS.  GFF is used only by gff2sga. I
# will base my tests on these two examples, since they use hg19_TSC_v1.gff, on
# which short.gff is based.  SGA is by far the most used format. Let's keep one
# test case per program, say STDIN or ARG1, which are always present.

# When the program to be tested was part of a pipeline, I removed any commands
# downstream of it.

while read tname cmd; do
	printf "\n%s\n" "$tname"
	printf "%s\n" "${tname//?/-}"
	for rsmod in badId no1st; do
		eval "$cmd" > "$tname.$rsmod.out"
	done
	diff -s "$tname.badId.out" "$tname.no1st.out" | head
	if ((${PIPESTATUS[0]} == 0)); then
		if (("$(stat -c '%s' "$tname.$rsmod.out")" == 0)); then
			printf "WARNING: files are empty!\n"
		else
			printf "Files identical and nonempty - removing.\n"
			for rsmod in badId no1st; do
				rm "$tname.$rsmod.out"
			done
		fi
	fi
done <<END
test_fps2sga	../src/perl/fps2sga.pl -f TSS -x ./Hs_EPDnew_006_hg19_\$rsmod.fps
test_gff2sga_db	../src/perl/gff2sga.pl -f TSC -c ./short_\$rsmod.gff
test_gff2sga	../src/perl/gff2sga.pl -f TSC -c ./short_\$rsmod.gff
test_chipcenter_STDIN	../src/chipcenter -i . -f "STAT1" -s 75 < STAT1_stim_small_\$rsmod.sga 2>/dev/null | head -10000
test_chipcor_ARG1	../src/chipcor -A "STAT1 +" -B "STAT1 -" -b-1000 -e1000 -w 10 STAT1_stim_small_\$rsmod.sga
test_chipextract_STDIN	../src/chipcenter -i . -f "STAT1" -s 75 -r "READS" < STAT1_stim_small_\$rsmod.sga 
test_chippart_ARG1	../src/chippart -d -f "STAT1" -s0.06 -p-10 STAT1_stim_small_\$rsmod.sga 2>/dev/null
test_chippeak_ARG1	../src/chipcenter -i . -f "STAT1" -s 75 STAT1_stim_small_\$rsmod.sga 2>/dev/null | ../src/chippeak -i . -f "STAT1" -t 25 -w 300 -v 300 -r 2>/dev/null
test_chipscore_STDIN	../src/chipcenter -i . -f "STAT1" -s 75 -r "READS" < STAT1_stim_small_\$rsmod.sga 2>/dev/null | sort -s -m -k1,1 -k3,3n -k4,4 - genomic_hit_MA0137.3_STAT1.sga | ../src/chipscore -A "STAT1" -B "READS" -o -b-150 -e150 -t 1 2>/dev/null
test_compactsga_STDIN	../src/chipcenter -i . -f "STAT1" -s 75 -z < STAT1_stim_small_\$rsmod.sga 2>/dev/null | head -10000 | ../src/compactsga
test_countsga_ARG1	../src/countsga test_countsga_\$rsmod.sga
test_featreplace_STDIN	../src/featreplace < STAT1_stim_small_\$rsmod.sga
test_filter_counts_STDIN	../src/filter_counts < STAT1_stim_small_\$rsmod.sga
test_sga2bed_ARG1	../src/sga2bed -i . STAT1_stim_small_\$rsmod.sga
test_sga2wig_ARG1	../src/sga2wig -i . STAT1_stim_small_\$rsmod.sga
test_fetch_sga	../src/perl/fetch_sga.pl STAT1_stim_small_\$rsmod.sga NC_000021.8:10698193-10698566 
test_partit2bed	../src/perl/partit2bed.pl chippart_ES_H3K27me3_\$rsmod.sga
test_partit2gff	../src/perl/partit2gff.pl test_partit2gff_input_\$rsmod.sga
test_sga2fps_ARG1	../src/perl/sga2fps.pl wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2_\$rsmod.sga
test_sga2gff_ARG1	../src/perl/sga2gff.pl wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2_\$rsmod.sga
test_sga2wigSmooth_FS-ARG1	../src/perl/sga2wigSmooth_FS.pl -a hg19 wgEncodeOpenChromDnaseUrothelUt189AlnRep2V2_\$rsmod.sga
END
