.PHONY: clean install build install-bin install-man doc install-doc \
	clean-local-data

################################################################
# Locations

# NOTE: according to the (Linux) filesystem standards
# (https://www.pathname.com/fhs/pub/fhs-2.3.html#USRLOCALSHARE1), /usr/local/man
# should be the same as /usr/local/share/man/ .
PREFIX   ?= /usr/local
BIN_DIR  ?= $(PREFIX)/bin
MAN_DIR  ?= $(PREFIX)/share/man/man1
DOC_DIR  ?= $(PREFIX)/share/chipseq/doc
DATA_DIR ?= $(PREFIX)/share/chipseq

################################################################
# Build tasks

all: build doc

build:
	$(MAKE) -C src/

doc:
	$(MAKE) -C doc/


################################################################
# Test tasks

test: build gunzip-data
	$(MAKE) -C src/ test

################################################################
# Install tasks

export DATA_DIR

MAN_PAGES  = $(wildcard ./share/man/man1/*.1.gz)
USER_DOCS  = $(wildcard ./doc/ChIP-Seq_Tools-UsersGuide.*)

install: install-bin install-man install-doc install-data

install-bin: build
	$(MAKE) -C src/ install

install-man:
	mkdir -p $(MAN_DIR)  && install -m 444 $(MAN_PAGES) $(MAN_DIR)

install-doc:
	mkdir -p $(DOC_DIR)  && install -m 444 $(USER_DOCS) $(DOC_DIR)

install-data: gunzip-data
	mkdir -p $(DATA_DIR) && $(MAKE) -C ./data install-data


################################################################
# Uninstall tasks

uninstall: uninstall-bin uninstall-man uninstall-dat uninstall-doc

uninstall-bin:
	$(MAKE) -C src/ uninstall-bin

#FIXME uninstall-man does not work
uninstall-man:
	$(RM) $(addprefix $(MAN_DIR)/, $(MAN_PAGES))

uninstall-dat:
	$(RM) -Rf $(DATA_DIR)

uninstall-doc:
	$(RM) -Rf $(DOC_DIR)


################################################################
# Unzip data (to follow the examples in the manual)
#
# NOTE: we _could_ have e.g. the 'install' target depend on this one, so that users
# don't even need to issue a separate command for decompression. OTOH it would
# also silently cause more disk space to be used up.

gunzip-data:
	$(MAKE) -C ./data gunzip-data


################################################################
# Clean tasks

clean: clean-bin clean-local-data clean-doc clean-test

clean-bin:
	$(MAKE) -C src/ clean

clean-test:
	$(MAKE) -C src/ clean_test

clean-doc:
	$(MAKE) -C doc/ clean

clean-local-data:
	$(MAKE) -C data/ clean

