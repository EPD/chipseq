.PHONY: clean install install-bin

################################################################
# Locations

PREFIX  ?= /usr/local
BIN_DIR ?= $(PREFIX)/bin

################################################################
# Build tasks

# NOTE: this makefile uses implicit make rules for C programs, so that only
# those targets for which the default rule isn't enough have an explicit rule.
# This includes anything that needs an extra object file.

# The main programs in the ChIP_Seq suite. Other programs in the suite are
# called "tools"; some are also written in C, others in Perl. The former are in
# this directory, the latter reside in ./perl.
CHIPSEQ_PROGS = chipcenter chipcor chipextract chippart chippeak chipscore
# C tools (other tools, written in Perl, are found in ./perl - see below)
C_TOOLS = bed2bed_display bed2sga compactsga countsga featreplace filter_counts sga2bed sga2wig
# Perl tools
PERL_TOOLS = $(wildcard perl/*.pl)

OBJECTS = hashtable.o

#TODO are -pedantic -fPIC usefull?
# -Wstrict-prototypes prevents empty arguments lists (a la int func() { ... }) -
# you need to explicitly say int func(void) { ... } instead. The advantage is
# that if you pass any arguments, GCC will emit a warning (there is none
# otherwise, because it isn't actually wrong).
CFLAGS = -O3 -std=gnu99 -Wall -Wextra -Wstrict-prototypes -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64

all: $(CHIPSEQ_PROGS) $(C_TOOLS)

chipcenter: chipcenter.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^

chippeak: chippeak.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^

bed2sga: bed2sga.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^

sga2bed: sga2bed.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

sga2wig: sga2wig.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^

chippart: chippart.c hashtable.o
	$(CC) $(CFLAGS) -o $@ $^


################################################################
# Test tasks

test: all
	cd tests && ./test_main.sh

clean_test:
	cd tests && ./test_main.sh -c

zgetline: zgetline.c 
	$(CC) -o $@ $< -lz

################################################################
# Install tasks

install: install-bin

install-bin: $(CHIPSEQ_PROGS) $(C_TOOLS)
	mkdir -p $(BIN_DIR) && install -s $(CHIPSEQ_PROGS) $(C_TOOLS) $(BIN_DIR) && install $(PERL_TOOLS) $(BIN_DIR)


################################################################
# Uninstall tasks

uninstall: uninstall-bin

uninstall-bin:
	# I just reuse the program sets, so I don't need to remember to add any
	# new chipseq programs to the uninstall list.
	$(RM) $(addprefix $(BIN_DIR)/, $(CHIPSEQ_PROGS) $(C_TOOLS) $(notdir $(PERL_TOOLS)))


################################################################
# Clean tasks

clean: clean_test
	$(RM) $(CHIPSEQ_PROGS) $(C_TOOLS) $(OBJECTS)

