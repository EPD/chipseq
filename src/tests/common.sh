# TODO: check if wereally need to keep all these definitions in a separate file
# - the test scripts do not seem to use them very much.
#
# Definitions common to all test scripts

declare -r RESULT_SUCCESS=0 # Test ran successfully
declare -r RESULT_FAILURE=1 # Test ran, but result not as expected
declare -r RESULT_NORUN=2   # Test could not run
declare -r RESULT_NOEXP=3   # Expected output file not found, or unreadable

red() { printf "\e[31m%s\e[0m" "$1"; }

yellow() { printf "\e[93m%s\e[0m" "$1"; }

green() { printf "\e[32m%s\e[0m" "$1"; }

cyan() { printf "\e[36m%s\e[0m" "$1"; }

die() {
	printf "%s: %s - aborting.\n"  "$(red FATAL)" "$1" >&2
	exit ${2-1}
}

warn() {
    printf "%s: %s\n"  "$(yellow WARNING)" "$1" >&2
}

info() {
	if [[ ${CHIPSEQ_TESTS_VERBOSE+x} ]]; then
		printf "INFO: %s\n" "$1"
	fi
}

debug() {
	printf "%s - %s\n" "$(cyan DEBUG)" "$1"
}
