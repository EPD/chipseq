# ChIP-Seq tools: new test examples
#
# Get input files:
#
# scp ccguser2@grbuchersrv4.epfl.ch:/local/db/genome/chr_size .
# scp ccguser2@grbuchersrv4.epfl.ch:/local/db/genome/chr_NC_gi .
# scp ccguser2@grbuchersrv4.epfl.ch:/local/db/genome/chro_idx.nstorage  .
# wget https://epd.expasy.org/mga/hg19/robertson07/STAT1_stim.sga.gz
# wget https://epd.expasy.org/mga/hg19/jaspar20p5/genomic_hit_MA0137.3_STAT1.sga.gz
# gunzip *gz
# grep '^NC_00002[12]' STAT1_stim.sga > STAT1_stim_small.sga

# Define shell variables
# BIN_DIR=?
CHR_SIZE_DIR=./
STAT1_READS=./STAT1_stim_small.sga
STAT1_MOTIFS=./genomic_hit_MA0137.3_STAT1.sga

# Commands:

# test_chipcenter_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 < "$STAT1_READS" 2>/dev/null | head -10000

# test_chipcor_STDIN
"$BIN_DIR"/chipcor -A "STAT1 +" -B "STAT1 -" -b-1000 -e1000 -w 10 < "$STAT1_READS"

# test_chippart_STDIN
"$BIN_DIR"/chippart -d -f "STAT1" -s0.06 -p-10 < "$STAT1_READS"  2>/dev/null

# test_chippart_STDIN_STDERR
"$BIN_DIR"/chippart -d -f "STAT1" -s0.06 -p-10 < "$STAT1_READS" 2>&1 >/dev/null

# test_chippeak_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 < "$STAT1_READS" 2>/dev/null | "$BIN_DIR"/chippeak -i "$CHR_SIZE_DIR" -f "STAT1" -t 25 -w 300 -v 300 -r 2>/dev/null

# test_chipscore_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 -r "READS" < "$STAT1_READS" 2>/dev/null | sort -s -m -k1,1 -k3,3n -k4,4 - "$STAT1_MOTIFS" | "$BIN_DIR"/chipscore -A "STAT1" -B "READS" -o -b-150 -e150 -t 1 2>/dev/null

# test_chipextract_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 -r "READS" < "$STAT1_READS" 2>/dev/null | sort -s -m -k1,1 -k3,3n -k4,4 - "$STAT1_MOTIFS" | "$BIN_DIR"/chipextract -A "STAT1 o" -B "READS" -b -1000 -e 1000 -w 20 -c 10 2>/dev/null

# test_compactsga_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 -z < "$STAT1_READS"  2>/dev/null | head -10000 | "$BIN_DIR"/compactsga

# test_countsga_STDIN
"$BIN_DIR"/countsga -c 10 < "$STAT1_READS"

# test_sga2bed_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 < "$STAT1_READS" 2>/dev/null | "$BIN_DIR"/chippeak -i "$CHR_SIZE_DIR" -f "STAT1" -t 25 -w 300 -v 300 -r 2>/dev/null |"$BIN_DIR"/sga2bed -i "$CHR_SIZE_DIR -l 300"

# test_bed2bed_display_STDIN
"$BIN_DIR"/chipcenter -i "$CHR_SIZE_DIR" -f "STAT1" -s 75 < "$STAT1_READS" 2>/dev/null | "$BIN_DIR"/chippeak -i "$CHR_SIZE_DIR" -f "STAT1" -t 25 -w 300 -v 300 -r 2>/dev/null | "$BIN_DIR"/sga2bed -i "$CHR_SIZE_DIR" -l 300 |"$BIN_DIR"/bed2bed_display -f STAT1_p -a 27 -b 525 --desc "Robertson 2007, HeLa S3 cells, ChIP-seq STAT1 peaks." --name "ChIP-Peak-STAT1-BED"
