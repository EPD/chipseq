#include <sys/wait.h>

#include "sysck.h"

int sysck(int system_return_value) {
    if (-1 == system_return_value)
        return SYSCK_NOCHILD;
    else
        if (WIFEXITED(system_return_value))
            return WEXITSTATUS(system_return_value);
        else
            return SYSCK_NOEXIT;

    return SYSCK_UNKNOWN;
}
