#ifndef SYSCK_H

#define SYSCK_H

#define SYSCK_NOCHILD (-1)
#define SYSCK_NOEXIT  (-2)
#define SYSCK_UNKNOWN (-3)

/* Takes the `value` returned by system(3), and returns the exit code of the
 * command passed to system(). If system() could not create a child process, returns
 * SYSCK_NOCHILD (errno will be set); if the child could not execute a shell, returns SYSCK_NOSHELL;
 *
 * */

int sysck(int value);

#endif
