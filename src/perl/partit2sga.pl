#!/usr/bin/env perl
#
# reformats partitioning output files (SGA) into bed format
# usage: partit2bed.pl [-t track_name -d desc -n chr_nb -b chr_start -e -chr_end] <ChIP-part.output>

use strict;
use Getopt::Long;
use Storable qw (retrieve nstore);         # package to store persistently variables in files [http://search.cpan.org/author/AMS/Storable-2.07/Storable.pm]

my %opt;
my @options = ("help", "h", "db=s", "i=s");

my $DB = './';

if( ! GetOptions( \%opt, @options ) ) { &Usage(); }
&Usage()  if defined($opt{'help'}) || defined($opt{'h'});
&Usage()  if $#ARGV < 0;

#define options
if (exists $opt{'i'}) {
    $DB = $opt{'i'};
}
if (exists $opt{'db'}) {
    $DB = $opt{'db'};
}

my $nstorage_path = File::Spec->catfile($DB, "chro_idx.nstorage");
my $chroac = retrieve($nstorage_path);

my $file = $ARGV[0];

# Chromosome size table
my %chr_size = ();

my @pos = ();

# Read First line of Partition File
#
# Get chrom sizes
my $assembly = "";
# Read Firstline and get assembly from chrom identifier
open(PART, "<$file") || die "can't read $file : $!";
my $firstline = <PART>;
chomp $firstline;
my @field=split(/\t/,$firstline);
if (defined($field[0])){
    $assembly = $chroac->{'assembly'}->{$field[0]};
}
if ($assembly ne "") {
    open (CHR, "$DB/$assembly/$assembly".".chrom.sizes") || die "can't open $DB/$assembly/$assembly'.'.chrom.sizes' : $!";
    while (<CHR>) {
        chomp;
        my @f=split(/\t/,$_);
        $chr_size{$f[0]} = $f[1];
    }
    close (CHR);
}
seek (PART, 0, 0);
while (<PART>){
# Partitioning algorithm reports positions of start and end of tag,
#NC_000001.9	CTCF	227595	+	1  Start Line
#NC_000001.9	CTCF	227633	-	1  End Line
#Generate centered SGA file
    my $lin=$_;
    chomp $lin;
    my @field=split(/\t/,$lin);
    if (defined($field[0])){
        if ($field[3] eq '+' || $field[3] eq '0') {
            $pos[0] = $field[2];
        } elsif ($field[3] eq '-') {
            $pos[1] = $field[2];
            my $center = int(($pos[0] + $pos[1])/2);
            next if (defined($chr_size{$field[0]}) && $center > $chr_size{$field[0]});
            print $field[0],"\t",$field[1],"\t",$center,"\t"."0"."\t$field[4]\n";
            @pos = ();
        }
    }
}
close (PART);

sub Usage {
    print STDERR <<"_USAGE_";

    partit2sga.pl [options] <ChIP-Part out file>

        where options are:
        -h|--help                Show this stuff
        -i|--db <path>           Use <path> to locate Chrom Id Storable File 'chro_idx.nstorage'

        The program converts the output of the ChIP-Seq partitioning program to centered SGA format.

_USAGE_
    exit(1);
}

1;

