#!/usr/bin/env perl

while(<STDIN>) {
    if(/^[^#]/){
        chomp;
        @f = split /\t/;
        $chr = $f[5];
        $beg = $f[6];
        $end = $f[7];
        $str = $f[9];
        $nam = "$f[11]/$f[12]/$f[10]";
        next if ($chr =~ /chr\d+_/ or $chr =~ /chrM/ or $chr =~ /chrUn/);
        print "$chr\t$beg\t$end\t$nam\t1\t$str\n";
    }
}

