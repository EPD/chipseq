#!/usr/bin/env perl

while(<STDIN>) {
    if(/^[^#]/){
        chomp;
        @f = split /\t/;
        $chr = $f[5];
        $beg = $f[6]+1;
        $end = $f[7];
        $nam = "$f[11]/$f[12]/$f[10]";
        $ftr = "RMSK";
        print "$chr\t$ftr\t$beg\t+\t1\t$nam\n";
        print "$chr\t$ftr\t$end\t-\t1\t$nam\n";
    }
}

