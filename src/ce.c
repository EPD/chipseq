
    if (options.debug && input != stdin) {
        char sort_cmd[1024] = "sort -s -c -k1,1 -k3,3n ";
        fprintf(stderr, "Check whether file %s is properly sorted\n",
                iFile);
        if (strcat(sort_cmd, iFile) == NULL) {
            fprintf(stderr, "strcat failed\n");
            return 1;
        }
        if (strcat(sort_cmd, " 2>/tmp/sortcheck.out") == NULL) {
            fprintf(stderr, "strcat failed\n");
            return 1;
        }
        fprintf(stderr, "executing : %s\n", sort_cmd);
        int sys_code = system(sort_cmd);
        /*fprintf(stderr,"system command sort : return code %d\n", sys_code); */
        if (sys_code != 0) {
            fprintf(stderr, "system command failed\n");
            return 1;
        }
        struct stat file_status;
        if (stat("/tmp/sortcheck.out", &file_status) != 0) {
            fprintf(stderr, "could not stat\n");
            return 1;
        }
        if (file_status.st_size != 0) {
            fprintf(stderr, "SGA file %s is not properly sorted\n", iFile);
            return 1;
        } else {
            system("/bin/rm /tmp/sortcheck.out");
        }
    }
